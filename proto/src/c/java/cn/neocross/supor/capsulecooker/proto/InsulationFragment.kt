package cn.neocross.supor.capsulecooker.proto

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cn.neocross.libs.neosocket.bean.InstantMessage
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neocross.supor.capsulecooker.proto.widget.ToolbarLayout
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.MessageDialogFragment
import kotlinx.android.synthetic.c.fragment_insulation.*

/**
 * 保温
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class InsulationFragment : BaseFragment() {

    companion object {

        fun get(): InsulationFragment {
            return InsulationFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_insulation, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        app.workType.type = Constant.INSULATION
        if (!app.isPassive()) {
            app.mClient?.send(InstantMessage(StatusType.TYPE_MSG, app.workType))
        }
        toolbarLayout.listener = object : ToolbarLayout.OnClickListener {
            override fun onSetting() {
                startActivity(
                        Intent(context, ContentActivity::class.java)
                                .putExtra("fragment", SettingFragment().toString()))
                (context as ContentActivity).overridePendingTransition(R.anim.setting_down_in, R.anim.setting_down_out)
            }

            override fun onCancel() {
                super.onCancel()
                val dialog = MessageDialogFragment.create("真的取消保温吗？", "是", "再想想")
                dialog.show(childFragmentManager, "")
                dialog.listener = object : MessageDialogFragment.OnClickListener {
                    override fun onPositiveListener() {
                        app.mClient?.send(InstantMessage(StatusType.TYPE_MSG, Constant.INSULATION_TEXT_CANCEL))
                        app.resetWorkType()
                        Toast.makeText(context, "已取消保温", Toast.LENGTH_SHORT).show()
                        activity.finish()
                    }
                }
            }
        }
    }
}