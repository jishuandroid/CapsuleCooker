package cn.neocross.supor.capsulecooker.proto.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

/**
 * 15等份扇形胶囊表示的状态界面
 * Created by shenhua on 2017/12/17.
 * Email shenhuanet@126.com
 */
class SectorView @JvmOverloads constructor(context: Context, attributeSet: AttributeSet? = null, defStyleAttr: Int = 0)
    : View(context, attributeSet, defStyleAttr) {

    /**
     * 存放扇形图各等份颜色
     */
    private var mArcPaint: Paint? = null
    private var colors: IntArray? = null
    private var mColorFillPaints: ArrayList<Paint> = ArrayList<Paint>()
    private var mCirclePaint: Paint? = null
    private var mRectF: RectF? = null
    private var mWidth: Int = 0
    private var mHeight: Int = 0
    /**
     * 扇形外圆半径
     */
    private var mRadius: Int = 200
    /**
     * 内圆半径占比外圆半径 0～1
     */
    private var mInsideRatio: Float = 0.4f
    var sizes: IntArray = intArrayOf(2, 4, 3)
    var listener: OnRotationListener? = null

    init {
        mArcPaint = Paint()
        mArcPaint!!.isAntiAlias = true
        mArcPaint!!.style = Paint.Style.STROKE
        mArcPaint!!.strokeWidth = 3f
        mArcPaint!!.color = Color.BLACK

        mCirclePaint = Paint(mArcPaint)
        mCirclePaint!!.color = Color.BLUE
        mCirclePaint!!.style = Paint.Style.FILL

        colors = intArrayOf(Color.parseColor("#FEFF00"),
                Color.parseColor("#9292FF"),
                Color.parseColor("#E781F2"))

        for (i in sizes.indices) {
            val paint = Paint(mCirclePaint)
            paint.color = colors!![i]
            mColorFillPaints.add(paint)
        }

        mRectF = RectF()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)

        mWidth = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> 2 * mRadius + paddingLeft + paddingRight
            MeasureSpec.UNSPECIFIED -> Math.min(2 * mRadius + paddingLeft + paddingRight, widthSize)
            else -> Math.min(2 * mRadius + paddingLeft + paddingRight, widthSize)
        }
        mHeight = when (heightMode) {
            MeasureSpec.EXACTLY -> heightSize
            MeasureSpec.AT_MOST -> mRadius * 2 + paddingTop + paddingBottom
            MeasureSpec.UNSPECIFIED -> Math.min(mRadius * 2 + paddingTop + paddingBottom, heightSize)
            else -> Math.min(mRadius * 2 + paddingTop + paddingBottom, heightSize)
        }
        setMeasuredDimension(mWidth, mHeight)
        mRadius = Math.min(mWidth - paddingLeft - paddingRight, mHeight - paddingTop - paddingBottom) / 2
        mRectF!!.set(-mRadius.toFloat(), -mRadius.toFloat(), mRadius.toFloat(), mRadius.toFloat())
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas!!.translate(mWidth / 2f, mHeight / 2f)
        canvas.rotate(mStartAngle)
        // fill
        var current = 8
        for (i in sizes.indices) {
            for (j in 0 until sizes[i]) {
                canvas.drawArc(mRectF, 24f * current, 24f, true, mColorFillPaints[i])
                current++
            }
        }
        /**
         * 15等份，每份24°，24°*4=96°,96°-90°=6°
         * 为保证最下方的扇形块处于中间，应从6°开始绘制，每次start 24°*i+6° sweep 24°
         */
        for (i in 0 until 15) {
            canvas.drawArc(mRectF, 24f * i, 24f, true, mArcPaint)
        }

//        canvas.drawCircle(0f, 0f, mInsideRatio * mRadius, mCirclePaint)
    }

    private var mStartAngle = 0f
    private var touchX = 0f
    private var touchY = 0f
    private var nowClick = 0L

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        /**
         * 假设外圆直径为R，内圆直径是外圆直径的0.4，则环形可点击区域满足：
         * (0.4R)²≤(X-R)² + (Y-R)²≤R²
         * 其中 X,Y 为点击的坐标
         */
        val mi = (Math.pow((event!!.x - mRadius).toDouble(), 2.0) + Math.pow((event.y - mRadius).toDouble(), 2.0))
//        if (mi <= Math.pow(mRadius.toDouble(), 2.0) && mi >= Math.pow((mInsideRatio * mRadius).toDouble(), 2.0)) {
            // 在指定区域内
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    touchX = event.x
                    touchY = event.y
                    nowClick = System.currentTimeMillis()
                }
                MotionEvent.ACTION_MOVE -> {
                    val moveX = event.x
                    val moveY = event.y
                    val arc = getRoundArc(touchX, touchY, moveX, moveY)
                    //重新赋值
                    touchX = moveX
                    touchY = moveY
                    mStartAngle += arc
                    invalidate()
                    notifyIndex()
                }
                MotionEvent.ACTION_UP,
                MotionEvent.ACTION_CANCEL -> {
                    // 此时为点击事件
                    if (System.currentTimeMillis() - nowClick <= 500) {
                        // 落点的角度加上偏移量基于初始的点的位置
//                    checkPosition = calInExactArea(getRoundArc(event.x, event.y) - mStartAngle)
//                    onDrawInvalidate()
//                    if (mOnWheelCheckListener != null) {
//                        mOnWheelCheckListener.onCheck(checkPosition)
//                    }
                    }
//                    notifyIndex()
//                    val point = calInExactArea(getRoundArc(event.x, event.y) - mStartAngle)
//                    println("--- point:$point")
                }
            }
            return true
//        }
//        return super.onTouchEvent(event)
    }

    private var cuAngle: Int = 0

    private fun notifyIndex() {
        val index = if (mStartAngle > 0) {
            cuAngle = mStartAngle.toInt() - (mStartAngle.toInt() / 360) * 360
            14 - cuAngle / 24
        } else {
            cuAngle = Math.abs(mStartAngle).toInt() - (Math.abs(mStartAngle).toInt() / 360) * 360
            Math.abs(cuAngle / 24)
        }
        listener?.onRotation(index, false)
    }

    /**
     * 根据落点计算角度
     * @param upX 落点x
     * @param upY 落点y
     */
    private fun getRoundArc(upX: Float, upY: Float): Float {
        // 第三点(中心)
        val mCenter = mWidth / 2f
        var arc = 0f
        // 首先计算三边的长度
        val a = Math.sqrt(Math.pow((mCenter - mCenter).toDouble(), 2.0) + Math.pow((0 - mCenter).toDouble(), 2.0)).toFloat()
        val b = Math.sqrt(Math.pow((upX - mCenter).toDouble(), 2.0) + Math.pow((upY - mCenter).toDouble(), 2.0)).toFloat()
        val c = Math.sqrt(Math.pow((upX - mCenter).toDouble(), 2.0) + Math.pow((upY - 0).toDouble(), 2.0)).toFloat()
        // 判断是否为三角形(两边之和大于第三边为三角形)
        if (a + b > c) {
            /**
             * 计算角度(反余弦)
             * acos((a2+b2-c2)/2ab)
             */
            arc = (Math.acos((Math.pow(a.toDouble(), 2.0) + Math.pow(b.toDouble(), 2.0)
                    - Math.pow(c.toDouble(), 2.0)) / (2f * a * b)) * 180 / Math.PI).toFloat()
            // 判断是大于左边还是右边，也就是180以内还是以外
            if (upX < mCenter) arc = 360 - arc
        } else {
            if (upX == mCenter) arc = if (upY < mCenter) 0f else 180f
        }
        return arc
    }

    /**
     * 根据三点的坐标计算旋转的角度
     * @param startX 起点x
     * @param startY 起点y
     * @param endX 终点x
     * @param endY 终点y
     */
    private fun getRoundArc(startX: Float, startY: Float, endX: Float, endY: Float): Float {
        // 第三点(中心)
        val mCenter = mWidth / 2f
        var arc = 0f
        // 首先计算三边的长度
        val a = Math.sqrt(Math.pow((startX - mCenter).toDouble(), 2.0) + Math.pow((startY - mCenter).toDouble(), 2.0)).toFloat()
        val b = Math.sqrt(Math.pow((endX - mCenter).toDouble(), 2.0) + Math.pow((endY - mCenter).toDouble(), 2.0)).toFloat()
        val c = Math.sqrt(Math.pow((startX - endX).toDouble(), 2.0) + Math.pow((startY - endY).toDouble(), 2.0)).toFloat()
        // 判断是否为三角形(两边之和大于第三边为三角形)
        if (a + b > c) {
            /**
             * 计算角度(反余弦)
             * acos((a2+b2-c2)/2ab)
             */
            arc = (Math.acos((Math.pow(a.toDouble(), 2.0) + Math.pow(b.toDouble(), 2.0)
                    - Math.pow(c.toDouble(), 2.0)) / (2f * a * b)) * 180 / Math.PI).toFloat()
            if (mCenter in startX..endX && startY < mCenter && endY < mCenter) {
                // 上边顺时针越界,Do Nothing
            } else if (mCenter in endX..startX && startY < mCenter && endY < mCenter) {
                // 上边逆时针越界
                arc = -arc
            } else if (mCenter in startX..endX && startY > mCenter && endY > mCenter) {
                // 下边逆时针越界
                arc = -arc
            } else if (mCenter in startX..endX && startY < mCenter && endY < mCenter) {
                // 下边顺时针越界,Do Nothing
            } else if (endX >= mCenter && startX >= mCenter) {
                // 这个时候表示在右半区
                if (startY > endY) arc = -arc
            } else if (endX < mCenter && startX < mCenter) {
                // 此时在左半区
                if (startY < endY) arc = -arc
            }
        }
        return if (Math.abs(arc) in 0.0..180.0) arc else 0f
    }

    /**
     * 根据当前旋转的mStartAngle计算当前滚动到的区域
     *
     * @param startAngle
     */
    private fun calInExactArea(startAngle: Float): Int {
        val size = 360f / 15
        // 确保rotate是正的，且在0-360度之间
        val rotate = (startAngle % 360.0f + 360.0f) % 360.0f
        for (i in 0 until 15) {
            if (i == 0) {
                if (rotate > 360 - size / 2 || rotate < size / 2) {
                    return i
                }
            } else {
                val from = size * (i - 1) + size / 2
                val to = from + size
                if (rotate > from && rotate < to) {
                    return i
                }
            }
        }
        return -1
    }

    interface OnRotationListener {

        /**
         * 旋转至某个胶囊时触发
         * @param index 某个胶囊的index
         * @param isEmptyBox 是否空胶囊
         */
        fun onRotation(index: Int, isEmptyBox: Boolean)
    }

}