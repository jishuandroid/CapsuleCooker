package cn.neocross.supor.capsulecooker.proto

import cn.neorcoss.supor.capsulecooker.library.BaseFragment

/**
 * Created by shenhua on 2018-01-11-0011.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ChoiceFragment : BaseFragment() {

    companion object {

        fun get(): ChoiceFragment {
            return ChoiceFragment()
        }
    }
}