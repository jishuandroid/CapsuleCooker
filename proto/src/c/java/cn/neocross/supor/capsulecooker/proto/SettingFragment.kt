package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.c.fragment_setting.*

/**
 * 设置
 * Created by shenhua on 2018-01-17-0017.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class SettingFragment : Fragment() {

    companion object {
        fun get(): SettingFragment {
            return SettingFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_setting, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvSettings.setOnClickListener { activity.finish() }

        tvSetting1.setOnClickListener {
            val c1 = setting1.getChildAt(1)
            updateArrow(tvSetting1, c1)
        }
        tvSetting2.setOnClickListener {
            val c1 = setting2.getChildAt(1)
            updateArrow(tvSetting2, c1)
        }
        tvSetting3.setOnClickListener {
            val c1 = setting3.getChildAt(1)
            updateArrow(tvSetting3, c1)
        }
    }

    private fun updateArrow(tv: TextView, parent: View) {
        parent.visibility = if (parent.visibility == View.GONE) View.VISIBLE else View.GONE
        tv.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                if (parent.visibility == View.GONE) R.drawable.ic_arrow_white else R.drawable.ic_arrow_down_white,
                0)
    }

    override fun toString(): String = SettingFragment::class.java.name
}