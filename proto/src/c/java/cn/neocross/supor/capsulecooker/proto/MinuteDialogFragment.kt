package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import cn.neorcoss.supor.capsulecooker.library.helper.WheelPickHelper
import kotlinx.android.synthetic.c.dialog_minute.*

/**
 *
val dialog = MinuteDialogFragment()
dialog.show(childFragmentManager, "dialog")
dialog.listener = object : MinuteDialogFragment.OnClickListener {
override fun onPositiveListener() {
Toast.makeText(context, "确定", Toast.LENGTH_SHORT).show()
}

override fun onNegativeListener() {
Toast.makeText(context, "取消", Toast.LENGTH_SHORT).show()
}
}
 * Created by shenhua on 2018-01-16-0016.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class MinuteDialogFragment : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.DialogStyle)
    }

    var listener: OnClickListener? = null
    private val times = arrayOf("10", "20", "30", "40", "50", "60", "70", "80", "90", "100")

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.dialog_minute, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog_minute.minValue = 0
        dialog_minute.maxValue = 9
        dialog_minute.displayedValues = times
        dialog_minute.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        dialog_minute.value = 5
        // 确定
        dialog_positive.setOnClickListener {
            dismiss()
            listener?.onPositiveListener()
        }
        // 取消
        dialog_negative.setOnClickListener {
            dismiss()
            listener?.onNegativeListener()
        }
        WheelPickHelper.setDividerHeight(dialog_minute, height = 1)
    }

    interface OnClickListener {

        fun onPositiveListener() {}

        fun onNegativeListener() {}
    }
}