package cn.neocross.supor.capsulecooker.proto.widget

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.os.Message
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import cn.neocross.supor.capsulecooker.proto.R
import kotlinx.android.synthetic.c.view_toolbar.view.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


/**
 * Created by shenhua on 2018-01-17-0017.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ToolbarLayout @JvmOverloads constructor(context: Context, attributeSet: AttributeSet? = null, defStyleAttr: Int = 0)
    : LinearLayout(context, attributeSet, defStyleAttr) {

    var isCooker = false
        set(value) {
            field = value
//            tvSwitchMode.visibility = View.VISIBLE
            tvCancel.visibility = View.GONE
        }
    var listener: OnClickListener? = null

    init {
        View.inflate(context, R.layout.view_toolbar, this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (isCooker) {
//            tvSwitchMode.visibility = View.VISIBLE
            tvCancel.visibility = View.GONE
        } else {
//            tvSwitchMode.visibility = View.GONE
            tvCancel.visibility = View.VISIBLE
        }
        tvCancel.setOnClickListener { listener?.onCancel() }
//        tvSwitchMode.setOnClickListener { listener?.onSwitch() }
        tvSettings.setOnClickListener { listener?.onSetting() }

        executeFixedRate()
    }

    private val scheduler = Executors.newSingleThreadScheduledExecutor()

    private fun executeFixedRate() {
        scheduler.scheduleAtFixedRate({
            val dateFormat = SimpleDateFormat("HH:mm:ss", Locale.CHINA)
            mHandler.obtainMessage(1, dateFormat.format(Date())).sendToTarget()
        }, 0, 1, TimeUnit.SECONDS)
    }

    private val mHandler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            try {
                tvSysTime.text = msg!!.obj.toString()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    interface OnClickListener {
        fun onCancel() {}
//        fun onSwitch() {}
        fun onSetting()
    }

}