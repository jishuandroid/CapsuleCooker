package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.helper.WheelPickHelper
import kotlinx.android.synthetic.c.fragment_reserve.*
import kotlinx.android.synthetic.c.view_reserve_type.*
import kotlinx.android.synthetic.c.view_wheel_minute.*
import kotlinx.android.synthetic.main.view_wheel_picker.*

/**
 * 预约
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ReserveFragment : BaseFragment() {

    companion object {

        fun get(): ReserveFragment {
            return ReserveFragment()
        }
    }

    private var currentType = cn.neorcoss.supor.capsulecooker.library.Constant.RESERVE_RICE
    private val times = arrayOf("10", "20", "30", "40", "50", "60", "70", "80", "90", "100")

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_reserve, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        val wheelHelper = WheelPickHelper()
        wheelHelper.setPicker(pickerData, pickerHour, pickerMinute)
        tvTypeRice.isSelected = true
        tvTypeRice.setOnClickListener {
            currentType = Constant.RESERVE_RICE
            tvTypeRice.isSelected = true
            tvTypeSlop.isSelected = false
            if (app.workType.mode == Constant.MODE_NONCAPSULE)
                layoutViewMinute.visibility = View.INVISIBLE
        }
        tvTypeSlop.setOnClickListener {
            currentType = Constant.RESERVE_SLOP
            tvTypeRice.isSelected = false
            tvTypeSlop.isSelected = true
            if (app.workType.mode == Constant.MODE_NONCAPSULE)
                layoutViewMinute.visibility = View.VISIBLE
        }
        btnStart.setOnClickListener {
            app.workType.time = wheelHelper.toString()
            app.workType.type = currentType
            (activity!! as ContentActivity)
                    .setFragment(ReserveDoneFragment.get(), false)
        }

        npMinte.minValue = 0
        npMinte.maxValue = 9
        npMinte.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        npMinte.displayedValues = times
        npMinte.value = 5
        WheelPickHelper.setDividerHeight(npMinte, height = 1)

        tvBack.setOnClickListener { activity.finish() }
    }
}