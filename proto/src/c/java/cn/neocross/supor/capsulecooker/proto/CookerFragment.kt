package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.supor.capsulecooker.proto.widget.CaseView
import cn.neocross.supor.capsulecooker.proto.widget.SectorView
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import kotlinx.android.synthetic.c.fragment_cooker.*
import kotlinx.android.synthetic.c.view_item_buttons_large.*

/**
 * 首页
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class CookerFragment : BaseFragment() {

    companion object {

        fun get(): CookerFragment {
            return CookerFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return if ((activity.application as App).workType.mode == Constant.MODE_CAPSULE)
            inflater!!.inflate(R.layout.fragment_cooker, container, false)
        else
            inflater!!.inflate(R.layout.fragment_cooker_no, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        val mode = app.workType.mode == Constant.MODE_CAPSULE
        tvInsulation.setOnClickListener { nav(InsulationFragment.get()) }
        tvReserve.setOnClickListener { nav(ReserveFragment.get()) }
        tvSlop.setOnClickListener {
            app.workType.type = Constant.SLOP
            if (mode) {
                nav(ProcessFragment.get())
            } else {
                val dialog = MinuteDialogFragment()
                dialog.show(childFragmentManager, "dialog")
                dialog.listener = object : MinuteDialogFragment.OnClickListener {
                    override fun onPositiveListener() {
                        nav(StartFragment.get())
                    }
                }
            }
        }
        tvRice.setOnClickListener {
            app.workType.type = Constant.RICE
            if (mode) {
                nav(ProcessFragment.get())
            } else {
                nav(StartFragment.get())
            }
        }
        tvModeSwitch.setOnClickListener { (activity as MainActivity).switchMode() }
        if (mode) {
            sectorView.listener = object : SectorView.OnRotationListener {
                override fun onRotation(index: Int, isEmptyBox: Boolean) {
                    sectorIndanView.notify(index)
                    notifyViews(index)
                }
            }
            viewChoiceSize.max = 4
            viewChoiceSize.maxListener = object : CaseView.MaxListener {
                override fun onMax(boolean: Boolean) {
                    tvSlop.isEnabled = boolean
                }
            }
        }
    }

    private fun notifyViews(index: Int) {
        if (index == 11 || index == 12 || index == 13 || index == 14) {
            tvRiceType.text = "紫米"
            viewChoiceSize.max = 4
        } else if (index == 0 || index == 1 || index == 2) {
            tvRiceType.text = "东北大米"
            viewChoiceSize.max = 3
        } else if (index == 9 || index == 10) {
            tvRiceType.text = "长白珍珠"
            viewChoiceSize.max = 2
        } else {
            tvRiceType.text = "空胶囊"
            viewChoiceSize.max = 0
        }
        tvRice.isEnabled = tvRiceType.text.toString() != "空胶囊"
        tvSlop.isEnabled = tvRiceType.text.toString() != "空胶囊"
        tvReserve.isEnabled = tvRiceType.text.toString() != "空胶囊"
    }

    private fun nav(fragment: Fragment) {
        val app = activity.application as App
        if (app.workType.mode == Constant.MODE_CAPSULE) {
            app.workType.rice = tvRiceType.text.toString()
            app.workType.size = "$viewChoiceSize 个胶囊"
        } else {
            app.workType.rice = ""
            app.workType.size = ""
        }
        (activity as MainActivity).navTo(fragment)
    }
}