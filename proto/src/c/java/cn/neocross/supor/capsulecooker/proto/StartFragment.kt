package cn.neocross.supor.capsulecooker.proto

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.libs.neosocket.bean.InstantMessage
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neocross.supor.capsulecooker.proto.widget.ToolbarLayout
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.MessageDialogFragment
import cn.neorcoss.supor.capsulecooker.library.WorkType
import kotlinx.android.synthetic.c.fragment_start.*

/**
 * 煮饭/煮粥
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class StartFragment : BaseFragment() {

    companion object {

        fun get(): StartFragment {
            return StartFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_start, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        val t = WorkType.getTypeString(app.workType.type)
        println("-- ${app.workType.type}")
        tvType.text = "正在$t"
        tvTimeSize.text = "${app.workType.size}${app.workType.rice}"
        toolbarLayout.listener = object : ToolbarLayout.OnClickListener {
            override fun onSetting() {
                startActivity(
                        Intent(context, ContentActivity::class.java)
                                .putExtra("fragment", SettingFragment().toString()))
                (context as ContentActivity).overridePendingTransition(R.anim.setting_down_in, R.anim.setting_down_out)
            }

            override fun onCancel() {
                super.onCancel()
                val dialog = MessageDialogFragment.create("真的取消${t}吗？", "是", "再想想")
                dialog.show(childFragmentManager, "")
                dialog.listener = object : MessageDialogFragment.OnClickListener {
                    override fun onPositiveListener() {
                        super.onPositiveListener()
                        app.mClient?.send(InstantMessage(StatusType.TYPE_MSG,
                                if (app.workType.type == Constant.RICE) Constant.RICE_TEXT_CANCEL
                                else Constant.SLOP_TEXT_CANCEL))
                        app.resetWorkType()
                        activity.finish()
                    }
                }
            }
        }
    }
}