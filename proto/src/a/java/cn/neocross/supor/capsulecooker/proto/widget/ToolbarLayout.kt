package cn.neocross.supor.capsulecooker.proto.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout

/**
 * Created by shenhua on 2018-01-17-0017.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ToolbarLayout @JvmOverloads constructor(context: Context, attributeSet: AttributeSet? = null, defStyleAttr: Int = 0)
    : LinearLayout(context, attributeSet, defStyleAttr) {

    /**
     * 兼容A
     */
    var isCooker = false
    var listener: OnClickListener? = null

    interface OnClickListener {
        fun onCancel() {}
        fun onSwitch() {}
        fun onSetting()
    }

}