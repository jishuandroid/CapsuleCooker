package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.WorkType
import cn.neorcoss.supor.capsulecooker.library.helper.WheelPickHelper
import kotlinx.android.synthetic.a.fragment_reserve.*
import kotlinx.android.synthetic.main.view_wheel_picker.*

/**
 * 预约
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ReserveFragment : BaseFragment() {

    companion object {
        fun get(): ReserveFragment {
            return ReserveFragment()
        }
    }

    private lateinit var wheelHelper: WheelPickHelper

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_reserve, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        btnNext.setOnClickListener {
            app.workType.time = wheelHelper.toString()
            (activity as ContentActivity).setFragment(ReserveDoneFragment.get(), false)
        }
        setViews()
        (activity as ContentActivity).setTitle("${app.workType.rice} ${app.workType.size}")
        if (app.workType.mode == Constant.MODE_NONCAPSULE) {
            (activity as ContentActivity).setTitle("${WorkType.getTypeString(app.workType.type)}")
        }
    }

    private fun setViews() {
        wheelHelper = WheelPickHelper()
        wheelHelper.setPicker(pickerData, pickerHour, pickerMinute)
    }
}