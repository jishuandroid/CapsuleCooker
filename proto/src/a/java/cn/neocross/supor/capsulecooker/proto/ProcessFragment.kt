package cn.neocross.supor.capsulecooker.proto

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.libs.neosocket.bean.InstantMessage
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import kotlinx.android.synthetic.a.fragment_process.*

/**
 * 处理进程
 */
class ProcessFragment : BaseFragment() {

    companion object {
        fun get(): ProcessFragment {
            return ProcessFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_process, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        val act = activity as ContentActivity
        act.setTitle("${app.workType.rice} ${app.workType.size}")
        if (!app.isPassive()) {
            app.mClient?.send(InstantMessage(StatusType.TYPE_MSG, app.workType))
        }
        Thread({
            try {
                for (i in 0..50) {
                    Thread.sleep(50)
                    mHandler.obtainMessage(2, i).sendToTarget()
                }
                mHandler.obtainMessage(1).sendToTarget()
                for (i in 51..100) {
                    Thread.sleep(100)
                    mHandler.obtainMessage(2, i).sendToTarget()
                }
                Thread.sleep(200)
                mHandler.obtainMessage(3).sendToTarget()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }).start()
    }

    private val mHandler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            try {
                if (activity == null) {
                    return
                }
                if (activity.isFinishing || activity.isDestroyed) {
                    return
                }
                if (msg!!.what == 1) {
                    tvTag2.text = "正在注水"
                    ivAnim.setImageResource(R.drawable.img_zhushui)
                }
                if (msg.what == 2) {
                    stepView.progress = msg.obj as Int
                    (activity as ContentActivity).hideBack()
                }
                if (msg.what == 3) {
                    (activity as ContentActivity).setFragment(StartFragment.get(), false)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}