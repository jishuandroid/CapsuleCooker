package cn.neocross.supor.capsulecooker.proto.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import cn.neocross.supor.capsulecooker.proto.R


/**
 * width = (count + 1) * interval + count * 2 * radius
 * Created by shenhua on 2018-01-03-0003.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class RadialIndicator @JvmOverloads constructor(context: Context, attr: AttributeSet? = null, defStyleAttr: Int = 0)
    : View(context, attr, defStyleAttr) {

    private val count = 15
    private var paints: ArrayList<Paint> = ArrayList<Paint>()
    private val defaultPaint: Paint
    private var interval: Float = 0f
    private val radius = 15f
    private val expandHeight = 150f
    private var rectf: RectF = RectF()
    var isExpand: Boolean = false
        set(value) {
            field = value
            invalidate()
        }
    var processes: IntArray = intArrayOf(3, 5, 2, 5)
        set(value) {
            field = value
            invalidate()
        }
    private var colors: IntArray

    init {
        val paint = Paint()
        paint.isAntiAlias = true
        paint.color = Color.GRAY
        paint.style = Paint.Style.FILL
        defaultPaint = paint
        colors = resources.getIntArray(R.array.colors)
        for (color in colors) {
            val p = Paint(paint)
            p.color = color
            paints.add(p)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width = when (MeasureSpec.getMode(widthMeasureSpec)) {
            MeasureSpec.EXACTLY -> MeasureSpec.getSize(widthMeasureSpec)
            MeasureSpec.AT_MOST -> MeasureSpec.getSize(widthMeasureSpec) - (paddingLeft + paddingRight)
            MeasureSpec.UNSPECIFIED -> MeasureSpec.getSize(widthMeasureSpec) - (paddingLeft + paddingRight)
            else -> MeasureSpec.getSize(widthMeasureSpec) - (paddingLeft + paddingRight)
        }
        val height = when (MeasureSpec.getMode(heightMeasureSpec)) {
            MeasureSpec.EXACTLY -> MeasureSpec.getSize(heightMeasureSpec) - (paddingLeft + paddingRight)
            MeasureSpec.AT_MOST -> if (isExpand) {
                expandHeight.toInt()
            } else {
                2 * radius.toInt()
            } + (paddingTop + paddingBottom)
            MeasureSpec.UNSPECIFIED -> MeasureSpec.getSize(heightMeasureSpec) - (paddingLeft + paddingRight)
            else -> MeasureSpec.getSize(heightMeasureSpec) - (paddingLeft + paddingRight)
        }

        setMeasuredDimension(width, height)
        interval = (measuredWidth - paddingLeft - paddingRight - count * 2 * radius) / (count + 1)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (isExpand) {
            drawRect(canvas)
        } else {
            drawCircle(canvas)
        }
    }

    private fun drawRect(canvas: Canvas?) {
        var sum = 0
        for (i in processes.indices) {
            for (a in 0 until processes[i]) {
                canvas!!.drawRoundRect(getRectF(sum), radius, radius, paints[i])
                sum++
            }
        }
        if (sum < count) {
            for (i in 0 until count - sum) {
                canvas!!.drawRoundRect(getRectF(sum++), radius, radius, defaultPaint)
            }
        }
    }

    private fun getRectF(i: Int): RectF {
        rectf.set(interval * (i + 1) + radius * i * 2 + paddingLeft,
                paddingTop * 1f,
                interval * (i + 1) + radius * i * 2 + 2 * radius + paddingLeft,
                paddingTop + expandHeight)
        return rectf
    }

    private fun drawCircle(canvas: Canvas?) {
        var sum = 0
        for (i in processes.indices) {
            for (a in 0 until processes[i]) {
                canvas!!.drawCircle(calculateStartX(sum), height / 2f, radius, paints[i])
                sum++
            }
        }
        if (sum < count) {
            for (i in 0 until count - sum) {
                canvas!!.drawCircle(calculateStartX(sum++), height / 2f, radius, defaultPaint)
            }
        }
    }

    private fun calculateStartX(i: Int): Float = interval * (i + 1) + radius * i * 2 + radius + paddingLeft
}