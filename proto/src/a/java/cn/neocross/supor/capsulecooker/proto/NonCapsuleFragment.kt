package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import kotlinx.android.synthetic.a.fragment_noncapsule.*

/**
 * 非胶囊烹饪
 */
class NonCapsuleFragment : BaseFragment() {

    companion object {
        fun get(): NonCapsuleFragment {
            return NonCapsuleFragment()
        }
    }

    private val data = arrayOf("煮粥", "煮饭")
    private val times = arrayOf("10", "20", "30", "40", "50", "60", "70", "80", "90", "100")

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_noncapsule, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        npType.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        npMinte.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS

        npType.displayedValues = data
        npType.minValue = 0
        npType.maxValue = 1
        npType.value = 1

        npMinte.displayedValues = times
        npMinte.minValue = 0
        npMinte.maxValue = 9
        npMinte.value = 4
        var type = Constant.RICE
        npType.setOnValueChangedListener { _, _, newVal ->
            val t = if (newVal == 0) {
                layoutMinte.visibility = View.VISIBLE
                type = Constant.SLOP
                "煮粥"
            } else {
                layoutMinte.visibility = View.INVISIBLE
                type = Constant.RICE
                "煮饭"
            }
            btnReserve.text = "预约$t"
            btnStart.text = "开始$t"
        }
        val app = activity.application as App

        btnReserve.setOnClickListener {
            app.workType.type =
                    if (type == Constant.RICE) Constant.RESERVE_RICE else Constant.RESERVE_SLOP
            nav(ReserveFragment.get())
        }
        btnStart.setOnClickListener {
            app.workType.type =
                    if (type == Constant.RICE) Constant.RICE else Constant.SLOP
            nav(StartFragment.get())
        }
    }

    private fun nav(fragment: Fragment) {
        (activity as MainActivity).navTo(fragment)
    }
}