package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.supor.capsulecooker.proto.ext.CookerAdapter
import kotlinx.android.synthetic.a.fragment_cooker.*

class CookerFragment : Fragment() {

    companion object {

        fun get(): CookerFragment {
            return CookerFragment()
        }
    }

    var position: Int = 0

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_cooker, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val fs = ArrayList<Fragment>()
        fs.add(CapsuleFragment.get())
        fs.add(NonCapsuleFragment.get())
        val adapter = CookerAdapter(childFragmentManager, fs)
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                this@CookerFragment.position = position
                updateToolbar(position)
            }
        })
        viewPager.currentItem = 0
        (activity.application as App).workType.mode = 0
    }

    private fun updateToolbar(position: Int) {
        val ac = activity as MainActivity
        ac.notifyIndex(position)
        ac.setTitle(if (position == 0) "胶囊烹饪" else "手动烹饪")
        (activity.application as App).workType.mode = position
    }
}
