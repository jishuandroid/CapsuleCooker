package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.supor.capsulecooker.proto.ext.SaveModel
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.WorkType
import kotlinx.android.synthetic.a.fragment_start.*

/**
 * 正在煮饭
 */
class StartFragment : BaseFragment() {

    companion object {
        fun get(): StartFragment {
            return StartFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_start, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        (activity as ContentActivity).setTitle("${app.workType.rice} ${app.workType.size}")
        (activity as ContentActivity).notifyBack("取消${WorkType.getTypeString(app.workType.type)}")
        if (app.workType.mode == Constant.MODE_NONCAPSULE) {
            tvNonInfo.visibility = View.VISIBLE
            (activity as ContentActivity).setTitle("${WorkType.getTypeString(app.workType.type)}")
        } else {
            if (app.workType.type == Constant.RICE) {
                SaveModel.putLastRice(context, arrayOf("煮饭", app.workType.rice!!, app.workType.size!!))
            } else {
                SaveModel.putLastSlop(context, arrayOf("煮粥", app.workType.rice!!, app.workType.size!!))
            }
        }
    }
}