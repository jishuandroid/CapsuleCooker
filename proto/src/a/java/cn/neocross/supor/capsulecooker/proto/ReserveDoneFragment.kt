package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.libs.neosocket.bean.InstantMessage
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import kotlinx.android.synthetic.a.fragment_reserve_done.*

/**
 * 预约成功
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ReserveDoneFragment : BaseFragment() {

    companion object {
        fun get(): ReserveDoneFragment {
            return ReserveDoneFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_reserve_done, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        (activity as ContentActivity).setTitle("预约 ${app.workType.rice} ${app.workType.size}")
        (activity as ContentActivity).invalidateOptionsMenu()
        (activity as ContentActivity).notifyBack("取消预约")
        circleDotView.isDot = false
        if (!app.isPassive()) {
            app.mClient?.send(InstantMessage(StatusType.TYPE_MSG, app.workType))
        }
        if (app.workType.mode == Constant.MODE_NONCAPSULE) {
            (activity as ContentActivity).setTitle("预约中")
        }
    }
}