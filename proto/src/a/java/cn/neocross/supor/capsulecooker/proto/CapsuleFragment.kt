package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.supor.capsulecooker.proto.ext.SaveModel
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import kotlinx.android.synthetic.a.fragment_capsule.*

/**
 * 胶囊烹饪
 */
class CapsuleFragment : BaseFragment() {

    companion object {
        fun get(): CapsuleFragment {
            return CapsuleFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_capsule, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        setListener()
    }

    private fun setupViews() {
        radialIndicator.processes = intArrayOf(2, 3, 1, 3)
        progress.progress = 80
        if (SaveModel.getLastRice(context)[0] != "0") {
            typeItemRice.setTitles(SaveModel.getLastRice(context))
        }
        if (SaveModel.getLastSlop(context)[0] != "0") {
            typeItemSlop.setTitles(SaveModel.getLastSlop(context))
        } else {
            typeItemSlop.setTitles(arrayOf("煮粥"))
        }
        typeItemSlop.setIcon(R.drawable.ic_slop)
    }

    private fun setListener() {
        radialIndicator.setOnClickListener { nav(WaterStatusFragment.get()) }
        progress.setOnClickListener { nav(WaterStatusFragment.get()) }
        // item
        typeItemRice.setOnClickCallback(View.OnClickListener {
            (activity.application as App).workType.type = Constant.RICE
            nav(ChoiceFragment.get())
        })
        typeItemSlop.setOnClickCallback(View.OnClickListener {
            (activity.application as App).workType.type = Constant.SLOP
            nav(ChoiceFragment.get())
        })
        // fast start
        if (typeItemRice.fastModel) {
            typeItemRice.setFastStartCallback(View.OnClickListener {
                val app = (activity.application as App)
                app.workType.type = Constant.RICE
                val str = SaveModel.getLastRice(context)
                app.workType.rice = str[1]
                app.workType.size = str[2]
                nav(ProcessFragment.get())
            })
        }
        if (typeItemSlop.fastModel) {
            typeItemSlop.setFastStartCallback(View.OnClickListener {
                val app = (activity.application as App)
                app.workType.type = Constant.SLOP
                val str = SaveModel.getLastSlop(context)
                app.workType.rice = str[1]
                app.workType.size = str[2]
                nav(ProcessFragment.get())
            })
        }
    }

    override fun onResume() {
        super.onResume()
        setupViews()
    }

    private fun nav(fragment: Fragment) {
        (activity as MainActivity).navTo(fragment)
    }
}