package cn.neocross.supor.capsulecooker.proto.widget

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import cn.neocross.supor.capsulecooker.proto.R

/**
 * Created by shenhua on 2017-12-19-0019.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class StepView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : View(context, attrs, defStyleAttr) {

    private var bgPaint: Paint = Paint()
    private var proPaint: Paint
    private var startX: Float = 0.toFloat()
    private var stopX: Float = 0.toFloat()
    private var bgCenterY: Float = 0.toFloat()
    private val lineBgWidth = 10
    private val bgColor = Color.parseColor("#cdcbcc")
    private val proColor = Color.parseColor("#029dd5")
    private val textPadding = 30
    private val textSize = 20
    var progress: Int = 30
        set(value) {
            field = value
            invalidate()
        }
    /**
     * 间隔
     */
    private var interval: Int = 0
    private var titles: Array<String> = arrayOf("切米", "注水", "煮饭")
    private var bitmap: Bitmap
    private var bitmapPaint: Paint = Paint()

    init {
        bgPaint.isAntiAlias = true
        bgPaint.style = Paint.Style.FILL
        bgPaint.color = bgColor
        bgPaint.strokeWidth = lineBgWidth.toFloat()
        bgPaint.textSize = textSize.toFloat()
        bgPaint.textAlign = Paint.Align.CENTER

        proPaint = Paint(bgPaint)
        proPaint.color = proColor

        bitmapPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        bitmapPaint.isFilterBitmap = true
        bitmapPaint.isDither = true
        bitmap = BitmapFactory.decodeResource(resources, R.drawable.ic_location)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val bgWidth = if (MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.EXACTLY) {
            MeasureSpec.getSize(widthMeasureSpec)
        } else dp2px(350)
        val bgHeight = if (MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.EXACTLY) {
            MeasureSpec.getSize(heightMeasureSpec) - paddingTop - paddingBottom
        } else paddingTop + paddingBottom + dp2px(80)
        setMeasuredDimension(bgWidth, bgHeight)
//        stopX = bgWidth * 1f
//        startX = (MeasureSpec.getSize(widthMeasureSpec) - measuredWidth) / 2f + paddingLeft + textPadding / 2
        stopX = bgWidth - paddingRight - textPadding * 1f
        startX = paddingLeft + textPadding * 1f
        bgCenterY = (bgHeight / 2).toFloat()
        interval = ((stopX - startX) / (titles.size - 1)).toInt()
    }

    override fun onDraw(canvas: Canvas) {
        drawBg(canvas)
        drawProgress(canvas)
        drawText(canvas)
    }

    private fun drawText(canvas: Canvas) {
        when {
            progress < 50 -> canvas.drawText(titles[0], startX, getTextY(), proPaint)
            progress < 100 -> {
                canvas.drawText(titles[0], startX, getTextY(), proPaint)
                canvas.drawText(titles[1], startX + interval, getTextY(), proPaint)
            }
            else -> {
                canvas.drawText(titles[0], startX, getTextY(), proPaint)
                canvas.drawText(titles[1], startX + interval, getTextY(), proPaint)
                canvas.drawText(titles[2], startX + interval * 2, getTextY(), proPaint)
            }
        }
    }

    private fun drawProgress(canvas: Canvas) {
        val p = ((stopX - startX) * progress) / 100
        canvas.drawLine(startX, bgCenterY, startX + p, bgCenterY, proPaint)
        canvas.drawBitmap(bitmap, startX + p - bitmap.width / 2, bgCenterY - bitmap.height, bitmapPaint)
    }

    private fun drawBg(canvas: Canvas) {
        canvas.drawLine(startX, bgCenterY, stopX, bgCenterY, bgPaint)
        val paint = Paint(bgPaint)
        paint.strokeWidth = 3f
        for (i in 0 until titles.size) {
            canvas.drawLine(startX + i * interval, bgCenterY, startX + i * interval, bgCenterY + 30, paint)
            canvas.drawText(titles[i], startX + i * interval, getTextY(), bgPaint)
        }
    }

    private fun getTextY(): Float = bgCenterY + textPadding * 2

    private fun dp2px(dip: Int): Int {
        val displayMetrics = context.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip.toFloat(), displayMetrics).toInt()
    }
}