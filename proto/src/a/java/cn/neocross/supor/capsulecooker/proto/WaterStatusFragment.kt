package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import kotlinx.android.synthetic.a.fragment_water.*

/**
 * 水量
 * Created by shenhua on 2018-01-11-0011.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class WaterStatusFragment : Fragment() {

    companion object {
        fun get(): WaterStatusFragment {
            return WaterStatusFragment()
        }
    }

    private var size: Int = 4
    private var waterProgress: Int = 60

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_water, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as ContentActivity).setTitle("水量")
        radialIndicator.processes = intArrayOf(2, 3, 1, 3)
        radialIndicator.isExpand = true
        progress.progress = 30
        progress.isExpand = true
        val ar = resources.obtainTypedArray(R.array.shape_circle)
        val tx = resources.getStringArray(R.array.rices2)
        for (i in 0 until size) {
            val vi = layoutInflater.inflate(R.layout.item_rice, null)
            (vi as TextView).text = tx[i]
            vi.setCompoundDrawablesWithIntrinsicBounds(ar.getResourceId(i, 0), 0, 0, 0)
            flowLayout.addView(vi)
        }
        ar.recycle()
        setInfoView()
        setWaterInfo()
    }

    private fun setWaterInfo() {
        if (waterProgress < 25) {
            tvInfo.text = "水量低于最低水位，不能进行烹饪，请加水"
        }
    }

    private fun setInfoView() {
        // 米量不足
        if (size == 1) {
            val vi = layoutInflater.inflate(R.layout.item_info, null)
            val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            (vi as TextView).text = Html.fromHtml("米量不足，请及时添加或<font color='blue'>去购买胶囊米</font>")
            flowLayout.addView(vi, params)
        }
        // 无米
        if (size == 0) {
            val vi = layoutInflater.inflate(R.layout.item_info, null)
            val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            (vi as TextView).text = Html.fromHtml("当前已无米，请及时添加胶囊米或<font color='blue'>去购买胶囊米</font>")
            flowLayout.addView(vi, params)
        }
    }

    override fun toString(): String = WaterStatusFragment::class.java.name
}