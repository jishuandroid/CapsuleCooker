package cn.neocross.supor.capsulecooker.proto.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View


/**
 * 横向多色进度条
 * Created by shenhua on 2017/12/16.
 * Email shenhuanet@126.com
 */
class ProgressView @JvmOverloads constructor(context: Context, attr: AttributeSet? = null, defStyleAttr: Int = 0)
    : View(context, attr, defStyleAttr) {

    private var mPaint: Paint? = null
    private var mWidth: Int = 0
    private var mHeight: Int = 200
    private var mProgress: IntArray? = null
    private var mPaints: ArrayList<Paint>? = null
    private var mColors: IntArray

    init {
        mPaints = ArrayList<Paint>()
        mPaint = Paint()
        mPaint!!.isAntiAlias = true
        mPaint!!.color = Color.GRAY
        mPaint!!.style = Paint.Style.FILL
        mColors = intArrayOf(0xFF9AFEC0.toInt(), 0xFFFF2A87.toInt(), 0xFF9C94FF.toInt(), 0xFF94BFFF.toInt(), 0xFFEAFF86.toInt())
    }

    fun setProgress(progress: IntArray) {
        mProgress = progress
        for (i in mProgress!!.size downTo 0) {
            val paint = Paint()
            paint.isAntiAlias = true
            paint.color = Color.GRAY
            paint.style = Paint.Style.FILL
            mPaints!!.add(paint)
        }
        postInvalidate()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mWidth = w
        mHeight = h
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas!!.drawRect(paddingLeft * 2f, paddingTop * 2f,
                mWidth - paddingLeft - paddingRight * 1f,
                mHeight - paddingTop - paddingBottom * 1f, mPaint)
        if (mProgress != null) {
            for (i in mProgress!!.size - 1 downTo 0) {
                mPaints!![i].color = mColors[i]
                canvas.drawRect(paddingLeft * 2f, paddingTop * 2f,
                        (mWidth - paddingLeft - paddingRight) * mProgress!![i] / 100f,
                        mHeight - paddingTop - paddingBottom * 1f, mPaints!![i])
            }
        }
    }

    private fun dp2px(context: Context, dip: Float): Float {
        val displayMetrics = context.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, displayMetrics)
    }
}