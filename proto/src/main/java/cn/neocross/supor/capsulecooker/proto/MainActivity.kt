package cn.neocross.supor.capsulecooker.proto

import android.app.Instrumentation
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import cn.neocross.libs.neosocket.NeoSocketServer
import cn.neocross.libs.neosocket.bean.MsgEngine
import cn.neocross.libs.neosocket.callback.NeoSocketClientCallback
import cn.neocross.libs.neosocket.callback.NeoSocketServerCallback
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neocross.supor.capsulecooker.proto.widget.ToolbarLayout
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.ViewUtils
import cn.neorcoss.supor.capsulecooker.library.WorkType
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject


class MainActivity : AppCompatActivity(), NeoSocketClientCallback {

    private lateinit var neoSocketServer: NeoSocketServer
    /**
     * 用于记录activity启动栈情况，一旦栈里包含某个activity而不继续添加改activity
     */
    private var acs = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ViewUtils.full(window)
        setContentView(R.layout.activity_main)
        setupViews()
    }

    /**
     * 页面初始化
     */
    private fun setupViews() {
        (application as App).workType.mode = currentIndex
        replace(CookerFragment.get())
        acs.add("main")
        if (BuildConfig.FLAVOR == "c") {
            findViewById<ToolbarLayout>(R.id.toolbarLayout).isCooker = true
            findViewById<ToolbarLayout>(R.id.toolbarLayout).listener = object
                : ToolbarLayout.OnClickListener {
                override fun onSetting() {
                    startActivity(
                            Intent(this@MainActivity, ContentActivity::class.java)
                                    .putExtra("fragment", SettingFragment().toString()))
                    overridePendingTransition(R.anim.setting_down_in, R.anim.setting_down_out)
                }
            }
            switchTitle(currentIndex)
            return
        }
        setSupportActionBar(toolbar)
        supportActionBar!!.title = null
        notifyIndex(currentIndex)
        setTitle(if (currentIndex == 0) "胶囊烹饪" else "手动烹饪")
        if (BuildConfig.FLAVOR == "a") {
            /**
             * 保温按钮
             */
            findViewById<TextView>(R.id.toolbar_back).setOnClickListener {
                navTo(InsulationFragment.get())
            }
        }
    }

    /**
     * fragment页面替换
     */
    fun replace(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment, fragment.toString())
                .commit()
    }

    /**
     * 跳转到contentActivity
     */
    fun navTo(fragment: Fragment) {
        if (acs.contains("content")) {
            return
        }
        startActivityForResult(Intent(this, ContentActivity::class.java)
                .putExtra("fragment", fragment.toString())
                , 0)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        acs.add("content")
    }

    /**
     * socket启动
     */
    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        neoSocketServer = NeoSocketServer(PortProvider.providePort(), object : NeoSocketServerCallback {
            override fun onServerStatusChanged(msgEngine: MsgEngine?) {
                val color: Int = when (msgEngine!!.type) {
                    StatusType.TYPE_SERVER_ERROR -> Color.RED
                    StatusType.TYPE_SERVER_STARTING -> Color.BLUE
                    StatusType.TYPE_SERVER_STARTED -> 0xFFCB8E4F.toInt()
                    StatusType.TYPE_SERVER_STOP, StatusType.TYPE_DISCONNECT -> Color.GRAY
                    StatusType.TYPE_CONNECTING -> 0xFF5E8E4F.toInt()
                    StatusType.TYPE_CONNECTED -> {
                        (application as App).mClient = neoSocketServer.createNeoSocketClient(
                                msgEngine.msg.toString(), PortProvider.providePort() + 1, this@MainActivity)
                        Color.GREEN
                    }
                    else -> Color.WHITE
                }
                tvSign.setBackgroundColor(color)
            }

            override fun onServerMsgReceived(message: Any?) {
                onMessageReserved(message)
            }
        })
        neoSocketServer.serverCallback
        tvIp.text = neoSocketServer.getSelfIp(this)
    }

    private fun onMessageReserved(message: Any?) {
        val app = application as App
        val obj = JSONObject(message.toString())
        try {
            val msg = obj.getJSONObject("message")
            val w = WorkType(
                    msg.getInt("type"),
                    msg.getInt("mode"),
                    msg.getString("rice"),
                    msg.getString("size"),
                    msg.getString("time"),
                    msg.getBoolean("passive"))
            app.workType = w
            app.workType.passive = true
            val fragment = when (w.type) {
                Constant.INSULATION -> InsulationFragment.get()
                Constant.RICE -> ProcessFragment.get()
                Constant.SLOP -> ProcessFragment.get()
                Constant.RESERVE_RICE -> ReserveDoneFragment.get()
                Constant.RESERVE_SLOP -> ReserveDoneFragment.get()
                else -> null
            }
            navTo(fragment!!)
        } catch (e: Exception) {
            val text = when (obj.getString("message")) {
                Constant.INSULATION_TEXT_CANCEL -> "已取消保温"
                Constant.RICE_TEXT_CANCEL -> "已取消煮饭"
                Constant.SLOP_TEXT_CANCEL -> "已取消煮粥"
                Constant.RESERVE_RICE_TEXT_CANCEL -> "已取消预约"
                Constant.RESERVE_SLOP_TEXT_CANCEL -> "已取消预约"
                else -> ""
            }
            app.resetWorkType()
            Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
            if (acs.contains("content")) {
                object : Thread() {
                    override fun run() {
                        try {
                            val inst = Instrumentation()
                            inst.sendKeyDownUpSync(KeyEvent.KEYCODE_BACK)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }.start()
            }
        }
    }

    override fun onClientMessageReceived(msg: Any?) {
        println("-- pad客户端onClientMessageReceived:$msg")
    }

    override fun onClientStatusChange() {
    }

    /**
     * 程序销毁
     */
    override fun onDestroy() {
        (application as App).mClient?.close()
        neoSocketServer.close()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (BuildConfig.FLAVOR == "a") {
            menuInflater.inflate(R.menu.setting, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }
        if (item.itemId == R.id.id_menu_setting) {
            startActivity(Intent(this, ContentActivity::class.java)
                    .putExtra("fragment", SettingFragment.get().toString()))
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
        if (item.itemId == R.id.id_menu_cancel) {
            // TODO 取消各项
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0) {
            acs.remove("content")
            setupViews()
        }
    }

    private var currentIndex = 0

    fun switchMode() {
        val app = application as App
        if (app.workType.mode == Constant.MODE_CAPSULE) {
            app.workType.mode = Constant.MODE_NONCAPSULE
            switchTitle(1)
            currentIndex = 1
        } else {
            app.workType.mode = Constant.MODE_CAPSULE
            switchTitle(0)
            currentIndex = 0
        }
        replace(CookerFragment.get())
    }

    private fun switchTitle(index: Int) {
        findViewById<TextView>(R.id.tvMode).text =
                if (index == 0) "胶囊烹饪模式" else "手动烹饪模式"
        findViewById<ToolbarLayout>(R.id.toolbarLayout).setBackgroundColor(
                if (index == 0) Color.parseColor("#F2F2F2")
                else Color.parseColor("#E4E4E4"))
    }

    /**
     * 修改toolbar中的指示器
     */
    fun notifyIndex(index: Int) {
        tvIndices1.isSelected = false
        tvIndices2.isSelected = false
        tvIndices1.visibility = View.VISIBLE
        tvIndices2.visibility = View.VISIBLE
        when (index) {
            0 -> tvIndices1.isSelected = true
            1 -> tvIndices2.isSelected = true
        }
        currentIndex = index
    }

    /**
     * 修改toolbar中的标题
     */
    fun setTitle(title: String) {
        if (BuildConfig.FLAVOR == "b") {
            toolbar_title.text = title
        }
    }
}
