package cn.neocross.supor.capsulecooker.proto

/**
 * Created by shenhua on 2018-01-22-0022.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
object PortProvider {

    fun providePort(): Int {
        return when (BuildConfig.FLAVOR) {
            "a" -> 5555
            "b" -> 5557
            "c" -> 5559
            else -> 8080
        }
    }
}