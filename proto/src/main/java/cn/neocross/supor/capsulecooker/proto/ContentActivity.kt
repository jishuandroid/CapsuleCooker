package cn.neocross.supor.capsulecooker.proto

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import cn.neocross.libs.neosocket.bean.InstantMessage
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.MessageDialogFragment
import cn.neorcoss.supor.capsulecooker.library.ViewUtils
import kotlinx.android.synthetic.main.activity_content.*

/**
 * Created by shenhua on 2018-01-15-0015.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ContentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ViewUtils.full(window)
        setContentView(R.layout.activity_content)
        setupViews()
        setupFragment()
    }

    /**
     * 初始化界面
     */
    private fun setupViews() {
        if (BuildConfig.FLAVOR != "c") {
            setSupportActionBar(toolbar)
            supportActionBar!!.title = null
            supportActionBar!!.setDisplayShowTitleEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        if (BuildConfig.FLAVOR == "a") {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
            // 返回,取消
            findViewById<TextView>(R.id.toolbar_back).setOnClickListener {
                val fragment = supportFragmentManager.findFragmentById(R.id.container)
                if (fragment is InsulationFragment || fragment is StartFragment || fragment is ReserveDoneFragment) {
                    val msg = when (fragment) {
                        is InsulationFragment -> "是否取消保温"
                        is StartFragment -> "是否取消做饭"
                        else -> "是否取消预约"
                    }
                    val dialog = MessageDialogFragment.create(msg, "是", "再想想")
                    dialog.show(supportFragmentManager, "")
                    dialog.listener = object : MessageDialogFragment.OnClickListener {
                        override fun onPositiveListener() {
                            val text = when (fragment) {
                                is InsulationFragment -> Constant.INSULATION_TEXT_CANCEL
                                is StartFragment -> Constant.RICE_TEXT_CANCEL
                                else -> Constant.RESERVE_RICE_TEXT_CANCEL
                            }
                            (application as App).mClient?.send(InstantMessage(StatusType.TYPE_MSG, text))
                            (application as App).resetWorkType()
                            finish()
                        }
                    }
                } else {
                    finish()
                }
            }
        }
    }

    /**
     * 自主填充Fragment
     */
    private fun setupFragment() {
        val fragment = intent.getStringExtra("fragment")
        when (fragment) {
            WaterStatusFragment.get().toString() -> setFragment(WaterStatusFragment.get())
            InsulationFragment.get().toString() -> setFragment(InsulationFragment.get())
            ChoiceFragment.get().toString() -> setFragment(ChoiceFragment.get())
            ProcessFragment.get().toString() -> setFragment(ProcessFragment.get())
            StartFragment.get().toString() -> setFragment(StartFragment.get())
            ReserveFragment.get().toString() -> setFragment(ReserveFragment.get())
            ReserveDoneFragment.get().toString() -> setFragment(ReserveDoneFragment.get())
            SettingFragment.get().toString() -> setFragment(SettingFragment.get())
        }
    }

    private fun setFragment(fragment: Fragment) {
        setFragment(fragment, false)
    }

    /**
     * 本activity设置fragment
     */
    fun setFragment(fragment: Fragment, needBackStack: Boolean) {
        if (isFinishing) {
            return
        }
        if (needBackStack) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .addToBackStack(fragment.toString())
                    .commit()
        } else {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit()
        }
        invalidateOptionsMenu()
    }

    /**
     * 设置标题
     */
    fun setTitle(title: String) {
        toolbar_title.text = title
    }

    /**
     * B方案中需要在toolbar上添加预约煮饭或是预约煮粥选择器
     */
    fun addChoiceView(): View? {
        if (BuildConfig.FLAVOR == "b") {
            val view = layoutInflater.inflate(R.layout.view_toolabr_choice, null)
            val t1 = view.findViewById<TextView>(R.id.tvChoiceRice)
            t1.isSelected = true
            val t2 = view.findViewById<TextView>(R.id.tvChoiceSlop)
            (application as App).workType.type = Constant.RESERVE_RICE
            toolbar.addView(view, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT))
            t1.setOnClickListener {
                t1.isSelected = true
                t2.isSelected = false
                (application as App).workType.type = Constant.RESERVE_RICE
            }
            t2.setOnClickListener {
                t2.isSelected = true
                t1.isSelected = false
                (application as App).workType.type = Constant.RESERVE_SLOP
            }
            return view
        }
        return null
    }

    /**
     * B方案中需要在toolbar上添加预约煮饭或是预约煮粥选择器
     */
    fun removeChoiceView() {
        if (BuildConfig.FLAVOR == "b") {
            toolbar.removeViewAt(toolbar.childCount - 1)
        }
    }

    fun hideBack() {
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
    }

    fun showBack() {
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        val s = (fragment is ReserveDoneFragment || fragment is InsulationFragment
                || fragment is StartFragment || fragment is ProcessFragment)
        if (BuildConfig.FLAVOR == "a") {
            if (s) {
                menuInflater.inflate(R.menu.setting, menu)
            }
            val tv = findViewById<TextView>(R.id.toolbar_back)
            tv.visibility = if (fragment is ProcessFragment) View.GONE else View.VISIBLE
        }
        if (BuildConfig.FLAVOR == "b") {
            menuInflater.inflate(R.menu.setting, menu)
            menu!!.findItem(R.id.id_menu_setting).isVisible = s
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }
        if (item.itemId == R.id.id_menu_setting) {
            startActivity(Intent(this, ContentActivity::class.java)
                    .putExtra("fragment", SettingFragment.get().toString()))
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        if (fragment is ReserveFragment
                || fragment is SettingFragment) {
            super.onBackPressed()
        } else if (fragment is ChoiceFragment) {
            if (BuildConfig.FLAVOR == "b") {
                removeChoiceView()
                val app = application as App
                if (app.workType.type == Constant.RICE || app.workType.type == Constant.SLOP) {
                    (application as App).resetWorkType()
                }
//                (application as App).workType.type = Constant.DEFAULT
                super.onBackPressed()
            } else {
                finish()
            }
        } else {
            finish()
        }
        if (fragment is ProcessFragment) {
            (application as App).resetWorkType()
        }
    }

    /**
     * C方案时退出切换动画
     */
    override fun finish() {
        super.finish()
        if (BuildConfig.FLAVOR == "c") {
            if (supportFragmentManager.findFragmentById(R.id.container) is SettingFragment) {
                overridePendingTransition(R.anim.setting_up_in, R.anim.setting_up_out)
            }
        } else {
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
    }

    /**
     * 修改返回按钮的文字显示
     */
    fun notifyBack(title: String) {
        if (BuildConfig.FLAVOR == "a") {
            val tv = findViewById<TextView>(R.id.toolbar_back)
            tv.text = title
        }
    }
}