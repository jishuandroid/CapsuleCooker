package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import kotlinx.android.synthetic.b.fragment_capsule.*

/**
 * 胶囊烹饪
 */
class CapsuleFragment : BaseFragment() {

    companion object {

        fun get(): CapsuleFragment {
            return CapsuleFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_capsule, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        progress1.setProgress(intArrayOf(90))
        progress2.setProgress(intArrayOf(20, 35, 45, 60, 65))
        val ac = activity as MainActivity
        tvStartSlop.setOnClickListener {
            app.workType.type = Constant.SLOP
            ac.navTo(ChoiceFragment.get())
        }
        tvStartRice.setOnClickListener {
            app.workType.type = Constant.RICE
            ac.navTo(ChoiceFragment.get())
        }
        tvReserve.setOnClickListener { ac.navTo(ReserveFragment.get()) }
        tvInsulation.setOnClickListener { ac.navTo(InsulationFragment.get()) }
    }
}