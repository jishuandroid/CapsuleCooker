package cn.neocross.supor.capsulecooker.proto.widget

import android.support.v4.view.ViewPager
import android.view.View


/**
 * Created by shenhua on 2018/1/19.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ViewPagerTransform : ViewPager.PageTransformer {

    private val mMinAlpha = 0.5f

    /**
     * Apply a property transformation to the given page.
     *
     * @param page Apply the transformation to this page
     * @param position Position of page relative to the current front-and-center
     * position of the pager. 0 is front and center. 1 is one full
     * page position to the right, and -1 is one page position to the left.
     */
    override fun transformPage(page: View?, position: Float) {
        if (position < -1) {
            page!!.alpha = mMinAlpha
        } else if (position <= 1) {
            if (position < 0) {
                val factor = mMinAlpha + (1 - mMinAlpha) * (1 + position)
                page!!.alpha = factor
            } else {
                val factor = mMinAlpha + (1 - mMinAlpha) * (1 - position)
                page!!.alpha = factor
            }
        } else {
            page!!.alpha = mMinAlpha
        }
    }
}