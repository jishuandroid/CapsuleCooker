package cn.neocross.supor.capsulecooker.proto

import android.support.v4.app.Fragment

/**
 * 水量
 * Created by shenhua on 2018-01-11-0011.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class WaterStatusFragment : Fragment() {

    companion object {
        fun get(): WaterStatusFragment {
            return WaterStatusFragment()
        }
    }

    override fun toString(): String = WaterStatusFragment::class.java.name
}