package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import android.widget.TextView
import cn.neocross.supor.capsulecooker.proto.ProcessFragment.Companion.TIMES
import cn.neocross.supor.capsulecooker.proto.adapter.CookerAdapter
import cn.neocross.supor.capsulecooker.proto.widget.ViewPagerCard
import cn.neocross.supor.capsulecooker.proto.widget.ViewPagerTransform
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.WorkType
import cn.neorcoss.supor.capsulecooker.library.bean.Rice
import kotlinx.android.synthetic.b.fragment_process_non.*
import kotlinx.android.synthetic.b.frament_choice.*
import kotlinx.android.synthetic.b.view_card_viewpager.*

/**
 * Created by shenhua on 2018/1/19.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ChoiceFragment : BaseFragment() {

    companion object {

        fun get(): ChoiceFragment {
            return ChoiceFragment()
        }
    }

    private var rootView: View? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = if ((activity.application as App).workType.mode == Constant.MODE_CAPSULE)
            inflater!!.inflate(R.layout.frament_choice, container, false)
        else
            inflater!!.inflate(R.layout.fragment_process_non, container, false)
        return rootView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        val act = activity as ContentActivity
        val type = app.workType.type
        if (app.workType.mode == Constant.MODE_CAPSULE) {
            val adapter = setViewPager()
            if (type == Constant.RICE || type == Constant.SLOP) {
                // 胶囊煮饭.煮粥
                act.setTitle(WorkType.getTypeString(app.workType.type))
                btnStart.text = "开始${WorkType.getTypeString(app.workType.type)}"
                btnStart.setOnClickListener {
                    val card = adapter.getFragment(viewPager.currentItem) as CardView
                    app.workType.rice = card.getRice()
                    app.workType.size = card.getSize()
                    act.setFragment(ProcessFragment.get(), false)
                }
            } else {
                // 胶囊预约煮饭.煮粥
                act.setTitle("")
                act.addChoiceView()
                btnStart.text = "开始预约"
                btnStart.setOnClickListener {
                    val card = adapter.getFragment(viewPager.currentItem) as CardView
                    app.workType.rice = card.getRice()
                    app.workType.size = card.getSize()
                    act.removeChoiceView()
                    act.setFragment(ReserveDoneFragment.get(), false)
                }
            }
        } else {
            if (type == Constant.RICE || type == Constant.SLOP) {
                // 非胶囊煮饭
                act.setTitle(WorkType.getTypeString(app.workType.type))
                btnStartNon.text = "开始${WorkType.getTypeString(app.workType.type)}"
                setNoViewPager(act, false)
                btnStartNon.setOnClickListener {
                    act.setFragment(StartFragment.get(), false)
                }
            } else {
                // 非胶囊预约
                act.setTitle("")
                setNoViewPager(act, true)
                btnStartNon.setOnClickListener {
                    act.removeChoiceView()
                    act.setFragment(ReserveDoneFragment.get(), false)
                }
            }
        }
    }

    /**
     * @param boolean 是否预约
     */
    private fun setNoViewPager(act: ContentActivity, boolean: Boolean) {
        val app = activity.application as App
        np.minValue = 0
        np.maxValue = TIMES.size - 1
        np.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        np.displayedValues = TIMES
        np.visibility = View.GONE
        npf.visibility = View.GONE
        tvRice.text = "预约煮饭,请手动放入需要的米"
        if (boolean) {
            btnStartNon.text = "开始预约"
            app.workType.type = Constant.RESERVE_RICE
            val choiceView = act.addChoiceView()
            val t1 = choiceView!!.findViewById<TextView>(R.id.tvChoiceRice)
            val t2 = choiceView.findViewById<TextView>(R.id.tvChoiceSlop)
            t1.setOnClickListener {
                np.visibility = View.GONE
                npf.visibility = View.GONE
                tvRice.text = "预约煮饭,请手动放入需要的米"
                t1.isSelected = true
                t2.isSelected = false
                app.workType.type = Constant.RESERVE_RICE
            }
            t2.setOnClickListener {
                np.visibility = View.VISIBLE
                npf.visibility = View.VISIBLE
                tvRice.text = "预约煮粥,请手动放入需要的米"
                t2.isSelected = true
                t1.isSelected = false
                app.workType.type = Constant.RESERVE_SLOP
            }
        } else {
            if (app.workType.type == Constant.RICE) {
                np.visibility = View.GONE
                npf.visibility = View.GONE
                tvRice.text = "预约煮饭,请手动放入需要的米"
            } else {
                np.visibility = View.VISIBLE
                npf.visibility = View.VISIBLE
                tvRice.text = "预约煮粥,请手动放入需要的米"
            }
        }
    }

    private fun setViewPager(): CookerAdapter {
        val fs = ArrayList<Fragment>()
        setupRice().forEach {
            fs.add(CardView.get(it))
        }
        val adapter = CookerAdapter(childFragmentManager, fs)
        viewPager.setPageTransformer(true, ViewPagerTransform())
        viewPager.offscreenPageLimit = 5
        viewPager.pageMargin = -230
        viewPager.adapter = adapter
        viewPager.currentItem = 0
        return adapter
    }

    private fun setupRice(): List<Rice> {
        val list = ArrayList<Rice>()
        list.add(Rice("东北五常大米", "米种描述", 2))
        list.add(Rice("紫米", "米种描述", 5))
        list.add(Rice("珍珠大米", "米种描述", 3))
        list.add(Rice("泰国香米", "米种描述", 2))
        return list
    }

    class CardView : Fragment() {

        companion object {
            fun get(rice: Rice): CardView {
                val fragment = CardView()
                val bundle = Bundle()
                bundle.putSerializable("rice", rice)
                fragment.arguments = bundle
                return fragment
            }
        }

        var rootView: ViewPagerCard? = null

        override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            rootView = ViewPagerCard(context)
            return rootView
        }

        override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            val rice = arguments.getSerializable("rice") as Rice
            rootView!!.setMax(rice.size)
            tvRiceName.text = rice.name
            tvRiceDesc.text = rice.desc
            tvRiceCount.text = "${rice.size}个胶囊"
        }

        fun getRice(): String {
            return (arguments.getSerializable("rice") as Rice).name
        }

        fun getSize(): String {
            return rootView!!.size
        }
    }
}