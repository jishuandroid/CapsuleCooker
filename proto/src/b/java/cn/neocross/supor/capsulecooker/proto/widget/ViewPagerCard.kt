package cn.neocross.supor.capsulecooker.proto.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import cn.neocross.supor.capsulecooker.proto.R
import kotlinx.android.synthetic.b.view_card_viewpager.view.*

/**
 * Created by shenhua on 2018/1/19.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ViewPagerCard @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : LinearLayout(context, attrs, defStyleAttr) {

    var size: String = "2个胶囊"

    init {
        View.inflate(context, R.layout.view_card_viewpager, this)
        size = caseView.toString()
        caseView.sizeListener = object : CaseView.SizeListener {
            override fun onSizeChanged(size: String) {
                this@ViewPagerCard.size = size
            }
        }
    }

    fun setMax(max: Int) {
        caseView.max = max
    }
}