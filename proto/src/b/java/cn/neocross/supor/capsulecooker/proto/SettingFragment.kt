package cn.neocross.supor.capsulecooker.proto

import android.support.v4.app.Fragment

/**
 * Created by shenhua on 2018/1/19.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class SettingFragment : Fragment() {
    companion object {
        fun get(): SettingFragment {
            return SettingFragment()
        }
    }
}