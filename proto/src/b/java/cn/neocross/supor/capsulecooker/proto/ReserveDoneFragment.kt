package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cn.neocross.libs.neosocket.bean.InstantMessage
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.MessageDialogFragment
import cn.neorcoss.supor.capsulecooker.library.WorkType
import cn.neorcoss.supor.capsulecooker.library.helper.WheelPickHelper
import cn.neorcoss.supor.capsulecooker.library.listener.WheelPickListener
import kotlinx.android.synthetic.b.fragment_reserve_done.*

/**
 * 预约成功
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ReserveDoneFragment : BaseFragment() {

    companion object {

        fun get(): ReserveDoneFragment {
            return ReserveDoneFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_reserve_done, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        val act = activity as ContentActivity
        act.setTitle("预约中")
        act.hideBack()
        act.invalidateOptionsMenu()
        tvTimeDate.text = WorkType.getTimeSplit(app.workType.time!!)[0]
        tvTimeTime.text = WorkType.getTimeSplit(app.workType.time!!)[1]
        tvRiceMode.text = WorkType.getTypeString(app.workType.type)
        if (app.workType.mode == Constant.MODE_CAPSULE) {
            tvRiceName.text = app.workType.rice
            tvRiceCount.text = app.workType.size
        } else {
            view4.visibility = View.GONE
            tvReserveT.text = "手动烹饪"
            tvRiceName.text = "不要忘记放米哟~~~~~"
        }

        if (!app.isPassive()) {
            app.mClient?.send(InstantMessage(StatusType.TYPE_MSG, app.workType))
        }

        btnReset.setOnClickListener {
            val builder = AlertDialog.Builder(context)
            val dv = layoutInflater.inflate(R.layout.view_wheel_picker, null)
            val helper = WheelPickHelper()
            helper.setPicker(dv.findViewById(R.id.pickerData),
                    dv.findViewById(R.id.pickerHour),
                    dv.findViewById(R.id.pickerMinute))
            builder.setView(dv)
            val dialog = builder.create()
            helper.forDialog(dialog, dv.findViewById(R.id.tvCancel), dv.findViewById(R.id.tvDone), object : WheelPickListener {
                override fun onSelect(value: String) {
                    app.workType.time = value
                    tvTimeDate.text = WorkType.getTimeSplit(app.workType.time!!)[0]
                    tvTimeTime.text = WorkType.getTimeSplit(app.workType.time!!)[1]
                }
            })
            dialog.show()
        }
        btnCancel.setOnClickListener {
            val dialog = MessageDialogFragment.create("确定要取消预约吗", "是", "再想想")
            dialog.show(childFragmentManager, "")
            dialog.listener = object : MessageDialogFragment.OnClickListener {
                override fun onPositiveListener() {
                    app.mClient?.send(InstantMessage(StatusType.TYPE_MSG,
                            if (app.workType.type == Constant.RICE) Constant.RESERVE_RICE_TEXT_CANCEL
                            else Constant.RESERVE_SLOP_TEXT_CANCEL))
                    app.resetWorkType()
                    Toast.makeText(context, "已经取消本次预约", Toast.LENGTH_SHORT).show()
                    activity.finish()
                }
            }
        }
    }
}