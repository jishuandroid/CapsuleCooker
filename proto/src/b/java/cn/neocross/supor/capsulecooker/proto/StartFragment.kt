package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cn.neocross.libs.neosocket.bean.InstantMessage
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neocross.supor.capsulecooker.proto.widget.CountDownTimeView
import cn.neocross.supor.capsulecooker.proto.widget.ProgressDownView
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.MessageDialogFragment
import cn.neorcoss.supor.capsulecooker.library.WorkType
import kotlinx.android.synthetic.b.fragment_start.*

/**
 * 正在煮饭
 */
class StartFragment : BaseFragment() {

    companion object {

        fun get(): StartFragment {
            return StartFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_start, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        val act = activity as ContentActivity
        val t = WorkType.getTypeString(app.workType.type)
        tvStarting.text = "${t}中，剩余计时"
        btnCancel.text = "取消${t}"
        act.setTitle("${t}中")
        act.hideBack()
        act.invalidateOptionsMenu()
        tvRice.text = if (app.workType.mode == Constant.MODE_CAPSULE) "${app.workType.rice} ${app.workType.size}" else "散米模式"
        progress.setOnStartCountDownListener(object : ProgressDownView.OnStartCountDownListener {
            override fun onStart() {
                countDownTimeView.initTime(0, 3, 50)
                countDownTimeView.start()
                countDownTimeView.setOnTimeCompleteListener(object : CountDownTimeView.OnTimeCompleteListener {
                    override fun onProgress(p: Int) {
                        progress.updateProgress(p)
                    }

                    override fun onComplete() {
                        Toast.makeText(context, "倒计时完成!", Toast.LENGTH_SHORT).show()
                    }
                })
            }
        })
        btnCancel.setOnClickListener {
            val dialog = MessageDialogFragment.create("确定要取消${t}吗", "是", "再想想")
            dialog.show(childFragmentManager, "")
            dialog.listener = object : MessageDialogFragment.OnClickListener {
                override fun onPositiveListener() {
                    app.mClient?.send(InstantMessage(StatusType.TYPE_MSG,
                            if (app.workType.type == Constant.RICE) Constant.RICE_TEXT_CANCEL
                            else Constant.SLOP_TEXT_CANCEL))
                    app.resetWorkType()
                    Toast.makeText(context, "已取消${t}", Toast.LENGTH_SHORT).show()
                    act.finish()
                }
            }
        }
    }
}