package cn.neocross.supor.capsulecooker.proto.adapter

import android.content.Context
import android.widget.Switch
import cn.neocross.supor.capsulecooker.proto.R
import cn.neorcoss.supor.capsulecooker.library.BaseRecyclerAdapter
import cn.neorcoss.supor.capsulecooker.library.bean.Reserve
import java.util.*

/**
 * 预约时间列表
 * Created by shenhua on 2017-12-27-0027.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ReserveTimeAdapter(mContext: Context, datas: ArrayList<Reserve>?) : BaseRecyclerAdapter<Reserve>(mContext, datas) {

    override fun getItemViewId(viewType: Int): Int = R.layout.item_add_time

    private var mLastSelected: Int = -1
    var onSelectedListener: OnSelectedListener? = null

    override fun bindData(holder: BaseRecyclerAdapter.BaseRecyclerViewHolder, position: Int, item: Reserve) {
        holder.setText(R.id.tvTime, item.time)
        val sw = holder.getView(R.id.switchUsually) as Switch
        sw.isChecked = item.selected
        sw.setOnClickListener { onSwitch(holder.adapterPosition) }
        holder.getView(R.id.btnDelete).setOnClickListener {
            val pos = holder.adapterPosition
            datas!!.removeAt(pos)
            notifyItemRemoved(pos)
        }
    }

    fun add(item: Reserve) {
        if (mLastSelected != -1) {
            datas!![mLastSelected].selected = false
            notifyItemChanged(mLastSelected)
        }
        addItem(0, item)
        mLastSelected = 0
    }

    private fun onSwitch(position: Int) {
        if (mLastSelected == position) {
            return
        }
        if (mLastSelected != -1) {
            datas!![mLastSelected].selected = false
            notifyItemChanged(mLastSelected)
        }
        mLastSelected = position
        onSelectedListener?.onSelect(position)
    }

    interface OnSelectedListener {
        fun onSelect(position: Int)
    }

    fun getOften(): String = datas!!.firstOrNull { it.selected }?.time ?: ""
}