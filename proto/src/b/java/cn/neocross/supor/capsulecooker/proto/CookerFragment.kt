package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.supor.capsulecooker.proto.adapter.CookerAdapter
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import kotlinx.android.synthetic.b.fragment_cooker.*

/**
 * 分配胶囊模式和非胶囊模式
 */
class CookerFragment : BaseFragment() {

    companion object {

        fun get(): CookerFragment {
            return CookerFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_cooker, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val fs = ArrayList<Fragment>()
        fs.add(CapsuleFragment.get())
        fs.add(NonCapsuleFragment.get())
        val adapter = CookerAdapter(childFragmentManager, fs)
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                updateToolbar(position)
            }
        })
        viewPager.currentItem = (activity.application as App).workType.mode
    }

    private fun updateToolbar(position: Int) {
        val ac = activity as MainActivity
        ac.notifyIndex(position)
        ac.setTitle(if (position == 0) "胶囊烹饪" else "手动烹饪")
        (activity.application as App).workType.mode = position
    }
}
