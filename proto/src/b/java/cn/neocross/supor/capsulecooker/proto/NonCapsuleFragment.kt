package cn.neocross.supor.capsulecooker.proto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import kotlinx.android.synthetic.b.fragment_noncapsule.*

/**
 * 非胶囊烹饪
 */
class NonCapsuleFragment : BaseFragment() {

    companion object {

        fun get(): NonCapsuleFragment {
            return NonCapsuleFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_noncapsule, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val ac = activity as MainActivity
        tvStartRice.setOnClickListener {
            (activity.application as App).workType.type = Constant.RICE
            ac.navTo(ChoiceFragment.get())
        }
        tvStartSlop.setOnClickListener {
            (activity.application as App).workType.type = Constant.SLOP
            ac.navTo(ChoiceFragment.get())
        }
        tvReserve.setOnClickListener { ac.navTo(ReserveFragment.get()) }
        tvInsulation.setOnClickListener { ac.navTo(InsulationFragment.get()) }
    }
}