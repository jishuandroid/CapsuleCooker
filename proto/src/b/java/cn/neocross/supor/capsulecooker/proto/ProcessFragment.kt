package cn.neocross.supor.capsulecooker.proto

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.libs.neosocket.bean.InstantMessage
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.WorkType
import kotlinx.android.synthetic.b.fragment_process.*

/**
 * 非胶囊烹饪
 */
class ProcessFragment : BaseFragment() {

    companion object {

        fun get(): ProcessFragment {
            return ProcessFragment()
        }

        val TIMES = arrayOf("30", "60", "90", "120", "150")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_process, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        val ac = activity as ContentActivity
        val t = WorkType.getTypeString(app.workType.type)
        ac.setTitle(t)
        ac.hideBack()
        ac.setTitle("${t}切米")
        stepView.titles = arrayOf("切米", "注水", "$t")
        tvRice.text = "${app.workType.rice} ${app.workType.size}"
        if (!app.isPassive()) {
            app.mClient?.send(InstantMessage(StatusType.TYPE_MSG, app.workType))
        }

        Thread({
            try {
                for (i in 0..50) {
                    Thread.sleep(50)
                    mHandler.obtainMessage(2, i).sendToTarget()
                }
                mHandler.obtainMessage(1).sendToTarget()
                for (i in 51..100) {
                    Thread.sleep(100)
                    mHandler.obtainMessage(2, i).sendToTarget()
                }
                Thread.sleep(1000)
                mHandler.obtainMessage(3).sendToTarget()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }).start()
    }

    private val mHandler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            try {
                if (activity.isFinishing || activity.isDestroyed) {
                    return
                }
                if (msg!!.what == 1) {
                    (activity as ContentActivity)
                            .setTitle("${WorkType.getTypeString((activity.application as App).workType.type)}注水")
                    tvStatus.text = "正在注水中"
                }
                if (msg.what == 2) {
                    tvProgress.text = "${msg.obj as Int}%"
                    stepView.progress = msg.obj as Int
                }
                if (msg.what == 3) {
                    (activity as ContentActivity)
                            .setFragment(StartFragment.get(), false)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}