package cn.neocross.supor.capsulecooker.ui.widget

import android.content.Context
import android.graphics.*
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.view.animation.Animation
import android.view.animation.Transformation

/**
 * 倒计时的圆环进度条
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ProgressDownView @JvmOverloads constructor(context: Context, attributeSet: AttributeSet? = null, defStyleAttr: Int = 0)
    : View(context, attributeSet, defStyleAttr) {

    private var mDefaultPaint: Paint? = null
    private var mPaint: Paint? = null
    private var mTextPaint: Paint? = null
    private var mRadius: Int = 200
    private var mStrokeWidth: Float = 20f
    private var mRectF: RectF? = null
    private var mSweepAngle: Float = 0f
    private var mMaxAngle: Float = 0f

    var progress: Int = 100
    var text = ""
    private var textRect: Rect? = null
    private var listener: OnStartCountDownListener? = null

    init {
        mDefaultPaint = Paint()
        mDefaultPaint!!.isAntiAlias = true
        mDefaultPaint!!.color = Color.GRAY
        mDefaultPaint!!.style = Paint.Style.STROKE
        mDefaultPaint!!.strokeWidth = mStrokeWidth / 2
        mPaint = Paint(mDefaultPaint)
        mPaint!!.color = 0xFF6BCCF7.toInt()
        mPaint!!.strokeWidth = mStrokeWidth
        mPaint!!.strokeCap = Paint.Cap.ROUND
        mRectF = RectF()
        mMaxAngle = (360 * progress / 100).toFloat()

        textRect = Rect()
        mTextPaint = Paint()
        mTextPaint!!.textAlign = Paint.Align.CENTER
        mTextPaint!!.textSize = 40f
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        val anim = Anim()
        anim.duration = 4000
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationStart(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                listener?.onStart()
            }
        })
        startAnimation(anim)
    }

    fun setOnStartCountDownListener(l: OnStartCountDownListener) {
        listener = l
    }

    fun updateProgress(progress: Int) {
        mSweepAngle = (360 * progress / 100).toFloat()
        postInvalidate()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width = getDefaultSize(suggestedMinimumWidth, widthMeasureSpec)
        val height = getDefaultSize(suggestedMinimumHeight, heightMeasureSpec)
        val min = Math.min(width, height)
        setMeasuredDimension(min, min)
        mRadius = (min - mStrokeWidth).toInt()
        mRectF!!.set(mStrokeWidth, mStrokeWidth, mRadius.toFloat(), mRadius.toFloat())
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas!!.drawArc(mRectF, 0f, 360f, false, mDefaultPaint)
        canvas.drawArc(mRectF, 0f, mSweepAngle, false, mPaint)
        if (!TextUtils.isEmpty(text)) {
            mTextPaint!!.getTextBounds(text, 0, text.length, textRect)
            canvas.drawText(text, measuredWidth / 2f,
                    measuredHeight / 2f + textRect!!.height() / 2f, mTextPaint)
        }
    }

    inner class Anim : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
            super.applyTransformation(interpolatedTime, t)
            mSweepAngle = if (interpolatedTime < 1.0f) {
                interpolatedTime * mMaxAngle
            } else {
                mMaxAngle
            }
            postInvalidate()
        }
    }

    interface OnStartCountDownListener {
        fun onStart()
    }
}