package cn.neocross.supor.capsulecooker.ui.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import cn.neocross.supor.capsulecooker.CookerFragment
import cn.neocross.supor.capsulecooker.R
import cn.neocross.supor.capsulecooker.ui.activity.MainActivity

/**
 * 用户通过WIFI绑定电饭煲的界面
 * Created by shenhua on 2017-12-15-0015.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class BindingStepFragment : Fragment() {

    private var activity: MainActivity? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        activity = context as MainActivity
    }

    companion object {
        /**
         * 步骤,0 1 2
         */
        fun get(step: Int): BindingStepFragment {
            val fragment = BindingStepFragment()
            val bundle = Bundle()
            bundle.putInt("step", step)
            fragment.arguments = bundle
            return fragment
        }
    }

    init {

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return when (arguments.getInt("step", 0)) {
            0 -> inflater!!.inflate(R.layout.fragment_binding_step1, container, false)
            1 -> inflater!!.inflate(R.layout.fragment_binding_step2, container, false)
            2 -> inflater!!.inflate(R.layout.fragment_binding_step_done, container, false)
            else -> super.onCreateView(inflater, container, savedInstanceState)
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val step = arguments.getInt("step", 0)
        when (step) {
            0, 1 ->
                view!!.findViewById<Button>(R.id.btnNext).setOnClickListener { activity!!.add(BindingStepFragment.get(step + 1)) }
            2 -> {
                view!!.findViewById<Button>(R.id.btnNext).setOnClickListener {
//                    activity!!.nav(CookerFragment.getInstance((activity!!.application as App).workType))
                    activity!!.nav(CookerFragment.get())
                }
            }
        }
    }

}