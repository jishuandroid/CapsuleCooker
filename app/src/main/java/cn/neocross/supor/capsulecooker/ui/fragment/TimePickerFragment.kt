package cn.neocross.supor.capsulecooker.ui.fragment

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.supor.capsulecooker.R
import cn.neorcoss.supor.capsulecooker.library.helper.WheelPickHelper
import cn.neorcoss.supor.capsulecooker.library.listener.WheelPickListener
import kotlinx.android.synthetic.main.view_wheel_picker.*

/**
 * 重置时间时对话框
 * Created by shenhua on 2017-12-20-0020.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class TimePickerFragment : BottomSheetDialogFragment(), WheelPickListener {

    override fun onSelect(value: String) {
        listener?.onSelect(value)
    }

    companion object {
        fun pick(): TimePickerFragment {
            return TimePickerFragment()
        }
    }

    private var listener: WheelPickListener? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.view_wheel_picker, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val pickHelper = WheelPickHelper()
        pickHelper.forDialogFragment(this, tvCancel, tvDone, this)
        pickHelper.setPicker(pickerData, pickerHour, pickerMinute)
    }

    fun setWheelPickListener(listener: WheelPickListener?) {
        this.listener = listener
    }

}