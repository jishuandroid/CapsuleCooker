package cn.neocross.supor.capsulecooker.ui.adapter

import android.view.View

/**
 * 卡片米饭选择适配器
 */
interface CardRiceAdapter {

    /**
     * 页卡总数
     */
    val count: Int

    /**
     * 页卡布局
     */
    fun getView(position: Int): View
}
