package cn.neocross.supor.capsulecooker.ui.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.graphics.Path
import android.graphics.Region
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View


/**
 * Created by shenhua on 2017-12-19-0019.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class CircleWaveView @JvmOverloads constructor(context: Context, attr: AttributeSet? = null, defStyleAttr: Int = 0)
    : View(context, attr, defStyleAttr) {

    private var mPaint: Paint? = null
    private val mContainerPath = Path()
    private var mWidth: Int = 0
    private var mHeight: Int = 200
    private var mProgress: Int = 0
    /**
     * X轴偏移量，默认为0，从左边系0点开始绘制
     */
    private var mOffsetX: Int = 0
    /**
     * 步进
     */
    var mStep: Int = 5
        set(value) {
            field = dp2px(context, value * 1f).toInt()
        }

    private val VALUE_A: Int = 20
    private var VALUE_W: Double = 0.0
    private val VALUE_H: Int = 0

    init {
        mPaint = Paint(ANTI_ALIAS_FLAG)
        mPaint!!.isAntiAlias = true
        mPaint!!.color = -0x66643119
        mPaint!!.style = Paint.Style.FILL_AND_STROKE
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mWidth = w
        mHeight = h
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        clipContainer(canvas!!, mWidth, mHeight)
        for (i in 0 until mWidth) {
            canvas.drawLine(i.toFloat(), mHeight.toFloat() - getLineY(i) - mProgress * mHeight / 100,
                    i.toFloat(), mHeight.toFloat(), mPaint)
            canvas.drawLine(i.toFloat(), mHeight.toFloat() - getLineY2(i) - mProgress * mHeight / 100,
                    i.toFloat(), mHeight.toFloat(), mPaint)
        }
        mOffsetX += mStep
        if (mOffsetX >= mWidth) {
            mOffsetX = 0
        }

        postInvalidate()
    }

    private fun clipContainer(canvas: Canvas, width: Int, height: Int) {
        mContainerPath.reset()
        canvas.clipPath(mContainerPath)
        mContainerPath.addCircle(width / 2f, height / 2f, width / 2f, Path.Direction.CCW)
        canvas.clipPath(mContainerPath, Region.Op.REPLACE)
    }

    fun setProgress(progress: Int) {
        this.mProgress = progress
    }

    fun getProgress(): Int {
        return mProgress
    }

    private fun getLineY(i: Int): Float {
        VALUE_W = (2 * Math.PI / mWidth)
        return VALUE_A * Math.sin(VALUE_W * (i - mOffsetX)).toFloat() + VALUE_H
    }

    private fun getLineY2(i: Int): Float {
        VALUE_W = (2 * Math.PI / mWidth)
        return VALUE_A * Math.sin(VALUE_W * (i - mOffsetX * 2)).toFloat() + VALUE_H
    }

    private fun dp2px(context: Context, dip: Float): Float {
        val displayMetrics = context.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, displayMetrics)
    }
}