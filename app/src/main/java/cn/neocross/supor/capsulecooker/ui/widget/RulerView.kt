package cn.neocross.supor.capsulecooker.ui.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View


/**
 * 标尺
 * Created by shenhua on 2017-12-27-0027.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class RulerView @JvmOverloads constructor(context: Context, attr: AttributeSet? = null, defStyleAttr: Int = 0)
    : View(context, attr, defStyleAttr) {

    /**
     * 红笔
     */
    private var mRedPaint: Paint
    /**
     * 文字笔
     */
    private var mTxtPaint: Paint
    /**
     * 刻度线笔
     */
    private var mScalePaint: Paint = Paint()
    private var mTextBounds: Rect
    private var mWidth: Int = 0
    private var mHeight: Int = 0
    private val mStep: Float = 20f
    var progress: Int = 30
        get() = field
        set(value) {
            field = value
        }

    init {
        mScalePaint.isAntiAlias = true
        mScalePaint.color = Color.GRAY
        mScalePaint.style = Paint.Style.FILL
        mTxtPaint = Paint(mScalePaint)
        mTxtPaint.color = 0xFF333333.toInt()
        mTxtPaint.textSize = 20f
        mTxtPaint.textAlign = Paint.Align.LEFT
        mRedPaint = Paint(mTxtPaint)
        mRedPaint.color = Color.RED
        mTextBounds = Rect()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        mWidth = measuredWidth / 4
        mHeight = measuredHeight
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        // 画刻度
        for (i in 0..10) {
            canvas!!.drawLine(0f, calculateStartY(i), calculateEndX(i), calculateStartY(i), getPaint(i))
        }
        // 画最低文字
        canvas!!.drawText("最低水位线", mStep + 10, calculateStartY(10) + 5, mRedPaint)
        var i = calculateI()
        if (i == 0) i += 1
        // 画刻度文字
        mTxtPaint.getTextBounds(getText(i), 0, getText(i).length, mTextBounds)
        val fontMetrics = mTxtPaint.fontMetricsInt
        if (i >= 10) {
            canvas.drawText(getText(i), calculateEndX(i - 1) + mStep,
                    calculateStartY(i - 1) - (fontMetrics.bottom + fontMetrics.top) / 2, mTxtPaint)
        } else {
            canvas.drawText(getText(i), calculateEndX(i) + mStep,
                    calculateStartY(i) - (fontMetrics.bottom + fontMetrics.top) / 2, mTxtPaint)
        }
    }

    private fun getText(int: Int): String {
        return when (int) {
            10 -> "2碗饭，不可煮粥"
            9 -> "2碗饭，不可煮粥"
            8 -> "3碗饭，1碗粥"
            7 -> "4碗饭，2碗粥"
            6 -> "5碗饭，3碗粥"
            5 -> "6碗饭，2碗粥"
            4 -> "7碗饭，3碗粥"
            3 -> "8碗饭，3碗粥"
            2 -> "9碗饭，4碗粥"
            1 -> "10碗饭，4碗粥"
            else -> ""
        }
    }

    private fun calculateI(): Int = 10 - (progress / 10)

    private fun getPaint(int: Int) = if (int == 10) mRedPaint else mScalePaint

    private fun calculateEndX(int: Int): Float = if (int % 2 == 1) mStep * 2 else mStep

    private fun calculateStartY(int: Int): Float = mHeight * int / 10 - mStep

}