package cn.neocross.supor.capsulecooker.ui.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View

/**
 * Created by shenhua on 2017-12-19-0019.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class HorizontalStepView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : View(context, attrs, defStyleAttr) {

    private var bgPaint: Paint? = null
    private var proPaint: Paint? = null
    private val bgRadius = 10f
    private val proRadius = 8f
    private var startX: Float = 0.toFloat()
    private var stopX: Float = 0.toFloat()
    private var bgCenterY: Float = 0.toFloat()
    private val lineBgWidth = 3
    private val bgColor = Color.parseColor("#cdcbcc")
    private val lineProWidth = 2
    private val proColor = Color.parseColor("#029dd5")
    private val textPadding = 20
    private val timePadding = 30
    private val textSize = 20
    var progress: Int = 0
        set(value) {
            field = value
            invalidate()
        }
    /**
     * 间隔
     */
    private var interval: Int = 0
    private var titles: Array<String>? = arrayOf("切米", "注水", "煮饭")
//    private var times: Array<String>? = arrayOf("00:01:02", "00:00:02", "")
//    private var volues: Array<String>? = arrayOf("2个胶囊", "30ml", "")
    private var map: Map<String, String>? = null

    init {
        bgPaint = Paint()
        bgPaint!!.isAntiAlias = true
        bgPaint!!.style = Paint.Style.FILL
        bgPaint!!.color = bgColor
        bgPaint!!.strokeWidth = lineBgWidth.toFloat()
        bgPaint!!.textSize = textSize.toFloat()
        bgPaint!!.textAlign = Paint.Align.CENTER

        proPaint = Paint(bgPaint)
        proPaint!!.color = proColor
        proPaint!!.strokeWidth = lineProWidth.toFloat()
    }

    fun setTitles(titles: Array<String>, times: Array<String>, volues: Array<String>) {
        this.titles = titles
//        this.times = times
//        this.volues = volues
        invalidate()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val widthMode = View.MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = View.MeasureSpec.getMode(heightMeasureSpec)
        val bgWidth = if (widthMode == View.MeasureSpec.EXACTLY) {
            View.MeasureSpec.getSize(widthMeasureSpec) - paddingLeft - paddingRight
        } else
            dp2px(context, 300f).toInt()
        val bgHeight = if (heightMode == View.MeasureSpec.EXACTLY) {
            View.MeasureSpec.getSize(heightMeasureSpec) - paddingTop - paddingBottom
        } else
            dp2px(context, 49f).toInt()
        stopX = bgWidth - bgRadius
        startX = paddingLeft + bgRadius
        bgCenterY = (bgHeight / 2).toFloat()
    }

    override fun onDraw(canvas: Canvas) {
        interval = ((stopX - startX) / (titles!!.size - 1)).toInt()
        drawBg(canvas)
        drawProgress(canvas)
//        drawText(canvas)
    }

    private fun drawProgress(canvas: Canvas) {
        val p = ((stopX - startX) * progress) / 100
        canvas.drawLine(startX, bgCenterY, startX + p, bgCenterY, proPaint!!)
        for (i in 0..getStep()) {
            canvas.drawCircle(startX + i * interval, bgCenterY, proRadius, proPaint!!)
            canvas.drawText(titles!![i], startX + i * interval, bgCenterY - textPadding, proPaint!!)
//            canvas.drawText(times!![i], startX + i * interval, bgCenterY + timePadding, proPaint!!)
        }
    }

    private fun drawText(canvas: Canvas) {
        for (i in 0 until titles!!.size) {
            if (i < getStep()) {
                setPaintColor(i)
//                if (null != volues && i < volues!!.size) {
//                    canvas.drawText(volues!![i], startX + i * interval, bgCenterY + timePadding * 2, proPaint!!)
//                }
            }
        }
    }

    private fun getStep(): Int {
        val p = ((stopX - startX) * progress) / 100
        return (p / interval).toInt()
    }

    private fun setPaintColor(i: Int) {
        if (titles == null || map == null) return
        val title = titles!![i]
        for ((key, value) in map!!) {
            if (title.contains(key)) {
                proPaint!!.color = Color.parseColor(value)
                return
            } else {
                proPaint!!.color = proColor
            }
        }
    }

    private fun drawBg(canvas: Canvas) {
        canvas.drawLine(startX, bgCenterY, stopX, bgCenterY, bgPaint!!)
        for (i in 0 until titles!!.size) {
            canvas.drawCircle(startX + i * interval, bgCenterY, bgRadius, bgPaint!!)
        }
    }

    private fun dp2px(context: Context, dip: Float): Float {
        val displayMetrics = context.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, displayMetrics)
    }

}
