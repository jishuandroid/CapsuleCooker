package cn.neocross.supor.capsulecooker.ui.fragment

import android.support.v4.app.Fragment

/**
 * 我的
 * Created by shenhua on 2017-12-20-0020.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class UserFragment : Fragment() {

    companion object {
        fun get(): UserFragment {
            return UserFragment()
        }
    }

    override fun toString(): String = UserFragment::class.java.name

}