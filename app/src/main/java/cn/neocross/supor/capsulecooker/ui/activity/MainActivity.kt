package cn.neocross.supor.capsulecooker.ui.activity

import android.app.Instrumentation
import android.content.Intent
import android.os.Bundle
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import cn.neocross.libs.neosocket.NeoSocketClient
import cn.neocross.libs.neosocket.NeoSocketServer
import cn.neocross.libs.neosocket.bean.MsgEngine
import cn.neocross.libs.neosocket.callback.NeoSocketClientCallback
import cn.neocross.libs.neosocket.callback.NeoSocketServerCallback
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neocross.supor.capsulecooker.*
import cn.neocross.supor.capsulecooker.ui.fragment.MallFragment
import cn.neocross.supor.capsulecooker.ui.fragment.UserFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.MessageDialogFragment
import cn.neorcoss.supor.capsulecooker.library.ViewUtils
import cn.neorcoss.supor.capsulecooker.library.WorkType
import cn.neorcoss.supor.capsulecooker.library.helper.InputLocalHelper
import cn.neorcoss.supor.capsulecooker.library.helper.UserHelper
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), NeoSocketServerCallback {

    private lateinit var neoSocketServer: NeoSocketServer

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                start()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_mall -> {
                replace(MallFragment.get())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_user -> {
                replace(UserFragment.get())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    /**
     * 是否开机，默认 开
     */
    private var powerOn: Boolean = true
    private var acs = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ViewUtils.full(window)
        setContentView(R.layout.activity_main)
        if (BuildConfig.FLAVOR != "c") {
            setSupportActionBar(toolbar)
            supportActionBar!!.title = null
            navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        }
        acs.add("main")
    }

    /**
     * 更新角标
     */
    private fun setBadge(count: Int) {
        val menuView = navigation.getChildAt(0) as BottomNavigationMenuView
        val tab = menuView.getChildAt(0) as BottomNavigationItemView
        if (count > 0) {
            val badge = LayoutInflater.from(this).inflate(R.layout.view_badge, menuView, false)
            val tv = badge.findViewById<TextView>(R.id.tvBadgeCount)
            tv.text = "$count"
            tv.visibility = View.VISIBLE
            tab.addView(badge)
        } else {
            if (tab.getChildAt(2) != null) {
                tab.removeViewAt(2)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        start()
    }

    /**
     * 开始
     */
    fun start() {
        // B方案无关机功能
        replace(if (BuildConfig.FLAVOR != "b" && !powerOn)
            PowerSwitchFragment.get()
        else
            CookerFragment.get()
        )
    }

    /**
     * fragment页面替换
     */
    fun replace(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment, fragment.toString())
                .commit()
    }

    fun navTo(fragment: Fragment) {
        if (acs.contains("content")) {
            return
        }
        startActivityForResult(Intent(this, ContentActivity::class.java)
                .putExtra("fragment", fragment.toString()), 0)
        acs.add("content")
    }

    /**
     * 添加到回退栈
     */
    fun add(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .addToBackStack(fragment.toString())
                .replace(R.id.container, fragment)
                .commit()
    }

    /**
     * 切换到某个页面,不添加到回退栈
     */
    fun nav(fragment: Fragment?) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit()
    }

    private fun showBack(show: Boolean) {
        supportActionBar!!.setDisplayHomeAsUpEnabled(show)
        supportActionBar!!.setDisplayShowHomeEnabled(show)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (BuildConfig.FLAVOR == "a") {
            menuInflater.inflate(R.menu.share_switch, menu)
        }
        if (BuildConfig.FLAVOR == "b") {
            menuInflater.inflate(R.menu.share_scan, menu)
        }
        if (BuildConfig.FLAVOR == "a" || BuildConfig.FLAVOR == "b") {
            menu!!.findItem(R.id.id_menu_share).isVisible = false
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            if (supportFragmentManager.backStackEntryCount > 0)
                supportFragmentManager.popBackStack()
        }
        if (BuildConfig.FLAVOR == "a") {
            if (item.itemId == R.id.id_menu_switch) {
                // 已开机
                if (powerOn) {
                    // 工作中
                    if ((application as App).workType.type != Constant.DEFAULT) {
                        val dialog = MessageDialogFragment.create("饭煲正在工作\n如需关机请先取消", "知道了", "")
                        dialog.show(supportFragmentManager, "")
                    } else {
                        supportFragmentManager.beginTransaction()
                                .add(R.id.container, PowerSwitchFragment.get())
                                .commit()
                        powerOn = false
                    }
                } else {
                    // 关机中
                    val fragment = supportFragmentManager.findFragmentById(R.id.container)
                    supportFragmentManager.beginTransaction().remove(fragment).commit()
                    powerOn = true
                }
            }
        } else {
            if (item.itemId == R.id.id_menu_scan) {
                Toast.makeText(this, "暂时无效", Toast.LENGTH_SHORT).show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        if (!UserHelper.isBinding(this)) {
            showConnectDialog()
        } else {
            try {
                val ip = InputLocalHelper.readIp(this)
                Thread({
                    val client = NeoSocketClient().connect(ip, PortProvider.providePort() - 1).get(3000, TimeUnit.SECONDS)
                    client.addClientListener(object : NeoSocketClientCallback {
                        override fun onClientMessageReceived(msg: Any?) {
                            println("-- 手机客户端 onClientMessageReceived:$msg")
                        }

                        override fun onClientStatusChange() {
                            println("-- 手机客户端掉线")
                        }
                    })
                    (application as App).mClient = client
                }).start()
            } catch (e: Exception) {
                e.printStackTrace()
                Toast.makeText(this, "连接失败,请确定对方机器是否准备就绪!", Toast.LENGTH_SHORT).show()
                InputLocalHelper.cleanIp(this)
                showConnectDialog()
            }
        }
        neoSocketServer = NeoSocketServer(PortProvider.providePort(), this)
    }

    private fun showConnectDialog() {
        val builder = AlertDialog.Builder(this)
        val view = layoutInflater.inflate(R.layout.view_dialog_ip, null)
        builder.setTitle("请连接机器")
        builder.setView(view)
        val dialog = builder.create()
        view.findViewById<Button>(R.id.btnOk).setOnClickListener {
            val ip = view.findViewById<EditText>(R.id.etIp).text.toString()
            if (!InputLocalHelper.isIp(ip)) {
                Toast.makeText(this, "IP地址有误,请重输!", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, ip, Toast.LENGTH_SHORT).show()
                try {
                    val client = NeoSocketClient().connect(ip, PortProvider.providePort() - 1).get(5000, TimeUnit.SECONDS)
                    (application as App).mClient = client
                    InputLocalHelper.saveIp(this, ip)
                    dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(this, "连接失败,请确定对方机器是否准备就绪!", Toast.LENGTH_SHORT).show()
                    InputLocalHelper.cleanIp(this)
                }
            }
        }
        dialog.show()
    }

    override fun onServerStatusChanged(msgEngine: MsgEngine?) {
        if (msgEngine!!.type == StatusType.TYPE_CONNECTED) {
            Toast.makeText(this, "已建立双向联动会话", Toast.LENGTH_SHORT).show()
        }
        if (msgEngine.type == StatusType.TYPE_DISCONNECT) {
            finish()
        }
    }

    override fun onServerMsgReceived(message: Any?) {
        val app = application as App
        val obj = JSONObject(message.toString())
        try {
            val msg = obj.getJSONObject("message")
            val w = WorkType(
                    msg.getInt("type"),
                    msg.getInt("mode"),
                    msg.getString("rice"),
                    msg.getString("size"),
                    msg.getString("time"),
                    msg.getBoolean("passive"))
            app.workType = w
            app.workType.passive = true
            val fragment = when (w.type) {
                Constant.INSULATION -> InsulationFragment.get()
                Constant.RICE -> ProcessFragment.get()
                Constant.SLOP -> ProcessFragment.get()
                Constant.RESERVE_RICE -> ReserveDoneFragment.get()
                Constant.RESERVE_SLOP -> ReserveDoneFragment.get()
                else -> null
            }
            if (BuildConfig.FLAVOR == "c") {
                replace(fragment!!)
                return
            }
            navTo(fragment!!)
        } catch (e: Exception) {
            val text = when (obj.getString("message")) {
                Constant.INSULATION_TEXT_CANCEL -> "已取消保温"
                Constant.RICE_TEXT_CANCEL -> "已取消煮饭"
                Constant.SLOP_TEXT_CANCEL -> "已取消煮粥"
                Constant.RESERVE_RICE_TEXT_CANCEL -> "已取消预约"
                Constant.RESERVE_SLOP_TEXT_CANCEL -> "已取消预约"
                else -> ""
            }
            app.resetWorkType()
            Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
            if (acs.contains("content")) {
                object : Thread() {
                    override fun run() {
                        try {
                            val inst = Instrumentation()
                            inst.sendKeyDownUpSync(KeyEvent.KEYCODE_BACK)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }.start()
            } else {
                start()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0) {
            acs.remove("content")
        }
    }

    override fun onDestroy() {
        (application as App).mClient?.close()
        neoSocketServer.close()
        super.onDestroy()
    }

    override fun onBackPressed() {
        // TODO(2018/1/18 onBackPressed)
        if (BuildConfig.FLAVOR == "c") {
            if (supportFragmentManager.findFragmentById(R.id.container) !is CookerFragment) {
                start()
            } else {
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }
}
