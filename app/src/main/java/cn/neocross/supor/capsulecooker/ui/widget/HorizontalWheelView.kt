package cn.neocross.supor.capsulecooker.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.HorizontalScrollView
import android.widget.LinearLayout
import android.widget.TextView
import cn.neocross.supor.capsulecooker.R
import java.util.*

/**
 * Created by shenhua on 2017-12-19-0019.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class HorizontalWheelView @JvmOverloads constructor(context: Context, attr: AttributeSet? = null, defStyleAttr: Int = 0) :
        HorizontalScrollView(context, attr, defStyleAttr) {

    private var container: LinearLayout? = null
    private var items: MutableList<String>? = null
    var offset = 1
    private var onWheelViewListener: OnWheelViewListener? = null
    private var itemCount: Int = 0
    private var selectedIndex = 1
    private var initialX: Int = 0
    private var scrollerTask: Runnable
    private var duration = 50
    private var itemWidth = 0
    private var viewHeight: Int = 0
    val seletedItem: String
        get() = items!![selectedIndex]

    val seletedIndex: Int
        get() = selectedIndex - offset

    interface OnWheelViewListener {
        fun onSelected(selectedIndex: Int, item: String)
    }

    fun setItems(list: List<String>) {
        if (null == items) {
            items = ArrayList()
        }
        items!!.clear()
        items!!.addAll(list)
        // 前面和后面补全
        for (i in 0 until offset) {
            items!!.add(0, "")
            items!!.add("")
        }
        itemCount = offset * 2 + 1
        for (item in items!!) {
            container!!.addView(createView(item))
        }
        refreshItemView(0)
    }

    init {
        isHorizontalScrollBarEnabled = false
        container = LinearLayout(context)
        container!!.orientation = LinearLayout.HORIZONTAL
        this.addView(container)
        scrollerTask = MyRunnable()
    }

    private inner class MyRunnable : Runnable {
        override fun run() {
            val newX = scrollX
            if (initialX - newX == 0) {
                val remainder = initialX % itemWidth
                val divided = initialX / itemWidth
                if (remainder == 0) {
                    selectedIndex = divided + offset
                    onSelectedCallBack()
                } else {
                    if (remainder > itemWidth / 2) {
                        this@HorizontalWheelView.post {
                            this@HorizontalWheelView.smoothScrollTo(initialX - remainder + itemWidth, 0)
                            selectedIndex = divided + offset + 1
                            onSelectedCallBack()
                        }
                    } else {
                        this@HorizontalWheelView.post {
                            this@HorizontalWheelView.smoothScrollTo(initialX - remainder, 0)
                            selectedIndex = divided + offset
                            onSelectedCallBack()
                        }
                    }
                }
            } else {
                initialX = scrollX
                this@HorizontalWheelView.postDelayed(scrollerTask, duration.toLong())
            }
        }
    }

    private fun startScrollerTask() {
        initialX = scrollX
        this.postDelayed(scrollerTask, duration.toLong())
    }

    private fun createView(item: String): TextView {
        val tv = LayoutInflater.from(context).inflate(R.layout.view_wheel_item, container, false) as TextView
        tv.text = item
        if (0 == itemWidth) {
            itemWidth = getViewMeasuredWidth(tv)
            container!!.layoutParams = FrameLayout.LayoutParams(itemWidth * itemCount, ViewGroup.LayoutParams.MATCH_PARENT)
            val lp = this.layoutParams as LinearLayout.LayoutParams
            this.layoutParams = LinearLayout.LayoutParams(itemWidth * itemCount, lp.height)
        }
        return tv
    }

    override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
        super.onScrollChanged(l, t, oldl, oldt)
        refreshItemView(l)
    }

    /**
     * 刷新item
     */
    private fun refreshItemView(y: Int) {
        var position = y / itemWidth + offset
        val remainder = y % itemWidth
        val divided = y / itemWidth

        if (remainder == 0) {
            position = divided + offset
        } else {
            if (remainder > itemWidth / 2) {
                position = divided + offset + 1
            }
        }

        val childSize = container!!.childCount
        for (i in 0 until childSize) {
            val itemView = container!!.getChildAt(i) as TextView ?: return
            if (position == i) {
                itemView.setTextColor(0xff0288ce.toInt())
                itemView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
                itemView.isSelected = true
            } else {
                itemView.setTextColor(0xffbbbbbb.toInt())
                itemView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
                itemView.isSelected = false
            }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        viewHeight = h
    }

    /**
     * 选中回调
     */
    private fun onSelectedCallBack() {
        if (null != onWheelViewListener) {
            onWheelViewListener!!.onSelected(selectedIndex, items!![selectedIndex])
        }
    }

    /**
     * 设置当前选中
     */
    fun setSelection(position: Int) {
        selectedIndex = position + offset
        this.post { this@HorizontalWheelView.smoothScrollTo(position * itemWidth, 0) }
    }

    override fun fling(velocityY: Int) {
        super.fling(velocityY / 3)
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        if (ev.action == MotionEvent.ACTION_UP) {
            startScrollerTask()
        }
        return super.onTouchEvent(ev)
    }

    fun setOnWheelListener(onWheelViewListener: OnWheelViewListener) {
        this.onWheelViewListener = onWheelViewListener
    }

    private fun getViewMeasuredWidth(view: View): Int {
        val height = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        val expandSpec = View.MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE shr 2, View.MeasureSpec.AT_MOST)
        view.measure(height, expandSpec)
        return view.measuredWidth
    }

    private fun dp2px(context: Context, dip: Float): Float {
        val displayMetrics = context.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, displayMetrics)
    }
}
