package cn.neocross.supor.capsulecooker.ui.fragment

import android.support.v4.app.Fragment

/**
 * 商城
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class MallFragment : Fragment() {

    companion object {
        fun get(): MallFragment {
            return MallFragment()
        }
    }

    override fun toString(): String = MallFragment::class.java.name
}