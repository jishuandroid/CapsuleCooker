package cn.neocross.supor.capsulecooker

import android.app.Application
import cn.neocross.libs.neosocket.NeoSocketClient
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.WorkType
import com.tencent.bugly.crashreport.CrashReport

/**
 * Created by shenhua on 2017-12-20-0020.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class App : Application() {

    var mClient: NeoSocketClient? = null

    lateinit var workType: WorkType

    override fun onCreate() {
        super.onCreate()
        CrashReport.initCrashReport(applicationContext, "4059b63590", false)
        resetWorkType()
    }

    fun resetWorkType() {
        workType = WorkType(Constant.DEFAULT, Constant.MODE_CAPSULE,
                "紫米", "2个胶囊", "今天,12:00")
    }

    fun isWorking(): Boolean = workType.type != Constant.DEFAULT

    /**
     * 是否被动发起,false为主动发起.为true时是被动发起
     */
    fun isPassive(): Boolean = workType.passive
}