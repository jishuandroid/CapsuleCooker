package cn.neocross.supor.capsulecooker.ui.widget

import android.content.Context
import android.graphics.*
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.animation.Animation
import android.view.animation.Transformation

/**
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ProgressView @JvmOverloads constructor(context: Context, attributeSet: AttributeSet? = null, defStyleAttr: Int = 0)
    : View(context, attributeSet, defStyleAttr) {

    private var mDefaultPaint: Paint? = null
    private var mPaint: Paint? = null
    private var mTextPaint: Paint? = null
    private var mRadius: Int = 200
    private var mStrokeWidth: Float = 20f
    private var mRectF: RectF? = null
    private var mSweepAngle: Float = 0f
    private var mEndAngle: Float = 0f
    private var colors: IntArray? = null

    var progress: Int = 80
    private var progresses: IntArray? = null
    var maxAngles: FloatArray? = null
    private var sweepAngles: FloatArray? = null
    var text = ""
    private var textRect: Rect? = null

    init {
        mDefaultPaint = Paint()
        mDefaultPaint!!.isAntiAlias = true
        mDefaultPaint!!.color = Color.GRAY
        mDefaultPaint!!.style = Paint.Style.STROKE
        mDefaultPaint!!.strokeWidth = mStrokeWidth / 2
        mPaint = Paint(mDefaultPaint)
        mPaint!!.color = Color.BLUE
        mPaint!!.strokeWidth = mStrokeWidth
        mPaint!!.strokeCap = Paint.Cap.ROUND
        mRectF = RectF()

        textRect = Rect()
        mTextPaint = Paint()
        mTextPaint!!.textAlign = Paint.Align.CENTER
        mTextPaint!!.textSize = dp2px(context, 15f)

        if (progresses == null) {
            progresses = intArrayOf(progress)
        }

        if (maxAngles == null) {
            maxAngles = kotlin.FloatArray(5)
            sweepAngles = kotlin.FloatArray(5)
        }
        colors = if (progresses!!.size == 1) {
            intArrayOf(0xFF6BCCF7.toInt())
        } else {
            intArrayOf(0xFFEAFF86.toInt(), 0xFFFF2A87.toInt(), 0xFF9C94FF.toInt(), 0xFF94BFFF.toInt(), 0xFF9AFEC0.toInt())
        }
        for (i in 0 until progresses!!.size) {
            maxAngles!![i] = (360 * progresses!![i] / 100).toFloat()
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        val anim = Anim()
        anim.duration = 4000
        startAnimation(anim)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width = getDefaultSize(suggestedMinimumWidth, widthMeasureSpec)
        val height = getDefaultSize(suggestedMinimumHeight, heightMeasureSpec)
        val min = Math.min(width, height)
        setMeasuredDimension(min, min)
        mRadius = (min - mStrokeWidth).toInt()
        mRectF!!.set(mStrokeWidth, mStrokeWidth, mRadius.toFloat(), mRadius.toFloat())
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas!!.drawArc(mRectF, 0f, 360f, false, mDefaultPaint)
        for (i in 0 until progresses!!.size) {
            mPaint!!.color = colors!![i]
            val startAngle = if (i == 0) 0f else maxAngles!![i - 1]
            canvas.drawArc(mRectF, startAngle, sweepAngles!![i], false, mPaint)
            mEndAngle += sweepAngles!![i]
        }
        if (!TextUtils.isEmpty(text)) {
            mTextPaint!!.getTextBounds(text, 0, text.length, textRect)
            canvas.drawText(text, measuredWidth / 2f,
                    measuredHeight / 2f + textRect!!.height() / 2f, mTextPaint)
        }
    }

    inner class Anim : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
            super.applyTransformation(interpolatedTime, t)
            for (i in 0 until maxAngles!!.size) {
                sweepAngles!![i] = if (interpolatedTime < 1.0f) {
                    interpolatedTime * maxAngles!![i]
                } else {
                    maxAngles!![i]
                }
                postInvalidate()
            }
        }
    }

    fun setProgress(progresses: IntArray) {
        this.progresses = progresses
        colors = if (progresses.size == 1) {
            intArrayOf(0xFF6BCCF7.toInt())
        } else {
            intArrayOf(0xFFEAFF86.toInt(), 0xFFFF2A87.toInt(), 0xFF9C94FF.toInt(), 0xFF94BFFF.toInt(), 0xFF9AFEC0.toInt())
        }
        for (i in 0 until progresses.size) {
            maxAngles!![i] = (360 * progresses[i] / 100).toFloat()
        }
        invalidate()
    }

    private fun dp2px(context: Context, dip: Float): Float {
        val displayMetrics = context.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, displayMetrics)
    }
}