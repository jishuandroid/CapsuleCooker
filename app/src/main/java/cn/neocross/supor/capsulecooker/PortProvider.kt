package cn.neocross.supor.capsulecooker

/**
 * Created by shenhua on 2018-01-22-0022.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
object PortProvider {

    fun providePort(): Int {
        return when (BuildConfig.FLAVOR) {
            "a" -> 5556
            "b" -> 5558
            "c" -> 5560
            else -> 8081
        }
    }
}