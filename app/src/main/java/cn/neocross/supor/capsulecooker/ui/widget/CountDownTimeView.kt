package cn.neocross.supor.capsulecooker.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.Chronometer
import java.text.SimpleDateFormat
import java.util.*

/**
 * 时分秒倒计时view
 * 使用流程: initTime()->start()->stop()
 * 拓展方法: OnTimeCompleteListener 计时完成回调
 * 拓展方法: reStart() 重新开始
 * Created by shenhua on 2017-12-20-0020.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class CountDownTimeView @JvmOverloads constructor(context: Context, attr: AttributeSet? = null, defStyleAttr: Int = 0)
    : Chronometer(context, attr, defStyleAttr), Chronometer.OnChronometerTickListener {

    private var mTime: Long = 0
    private var mNextTime: Long = 0
    private var mTimeFormat: SimpleDateFormat? = null
    private var listener: OnTimeCompleteListener? = null

    init {
        mTimeFormat = SimpleDateFormat("hh:mm:ss", Locale.CHINA)
        onChronometerTickListener = this
        text = "00:00:00"
    }

    /**
     * 初始化时间(秒)
     */
    fun initTime(miss: Long) {
        if (miss <= 0) {
            return
        }
        mNextTime = miss
        mTime = mNextTime
        updateTimeText()
    }

    /**
     * 初始化时间（分秒）
     */
    fun initTime(hour: Long, minu: Long, miss: Long) {
        initTime(hour * 3600 + minu * 60 + miss)
    }

    /**
     * 重新开始
     */
    fun reStart(miss: Long) {
        if (miss.toInt() == -1) {
            mNextTime = mTime
        } else {
            mNextTime = miss
            mTime = mNextTime
        }
        this.start()
    }

    /**
     * 重新开始
     */
    fun reStart() {
        reStart(-1)
    }

    override fun onChronometerTick(chronometer: Chronometer?) {
        if (mNextTime <= 0) {
            if (mNextTime.toInt() == 0) {
                this.stop()
                listener?.onComplete()
            }
            mNextTime = 0
            updateTimeText()
            return;
        }
        mNextTime--
        listener?.onProgress((mNextTime * 100 / mTime).toInt())
        updateTimeText()
    }

    private fun updateTimeText() {
        text = formatMiss(mNextTime)
    }

    private fun formatMiss(miss: Long): String {
        val hh = if (miss / 3600 > 9) (miss / 3600).toString() + "" else "0" + miss / 3600
        val mm = if (miss % 3600 / 60 > 9) (miss % 3600 / 60).toString() + "" else "0" + miss % 3600 / 60
        val ss = if (miss % 3600 % 60 > 9) (miss % 3600 % 60).toString() + "" else "0" + miss % 3600 % 60
        return "$hh:$mm:$ss"
    }

    fun setOnTimeCompleteListener(listener: OnTimeCompleteListener) {
        this.listener = listener
    }

    interface OnTimeCompleteListener {

        fun onProgress(p: Int)

        fun onComplete()
    }
}