package cn.neocross.supor.capsulecooker.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import cn.neocross.supor.capsulecooker.R
import kotlinx.android.synthetic.c.view_choice_size.view.*

/**
 * Created by shenhua on 2018-01-16-0016.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class CaseView @JvmOverloads constructor(context: Context, attributeSet: AttributeSet? = null, defStyleAttr: Int = 0)
    : LinearLayout(context, attributeSet, defStyleAttr) {

    var max: Int = 3
        set(value) {
            field = value
            updateViews()
        }
    var maxListener: MaxListener? = null

    init {
        View.inflate(context, R.layout.view_choice_size, this)
    }

    private fun updateViews() {
        if (max == 0) {
            tvCase1.visibility = View.INVISIBLE
            tvCase2.text = "0"
            tvCaseInfo.text = "当前胶囊已无剩余"
            tvCase3.visibility = View.INVISIBLE
            return
        }
        tvCase1.visibility = View.VISIBLE
        tvCase2.visibility = View.VISIBLE
        tvCase3.visibility = View.VISIBLE
        tvCase1.visibility = if (max < 2) View.INVISIBLE else View.VISIBLE
        tvCase3.visibility = if (max < 3) View.INVISIBLE else View.VISIBLE
        tvCaseInfo.text = "约2碗米饭或4碗粥"

        tvCase1.text = "1"
        tvCase2.text = "2"
        tvCase3.text = "3"
        if (max == 1) {
            tvCase1.text = "1"
            tvCase2.text = "1"
            tvCase3.text = "1"
        }
        add.setOnClickListener {
            var a: Int = tvCase1.text.toString().toInt()
            if (++a <= max - 1) {
                tvCase1.text = "$a"
                tvCase2.text = "${a + 1}"
                tvCase3.text = "${a + 2}"
                tvCase3.visibility = if (tvCase3.text.toString().toInt() > max) View.INVISIBLE else View.VISIBLE
                tvCase1.visibility = if (a == 0) View.INVISIBLE else View.VISIBLE
                tvCaseInfo.text = "约${a + 1}碗米饭或${a + 3}碗粥 ${if (a + 1 < 3) "" else "(煮粥最多可选2个胶囊)"}"
                maxListener?.onMax(a + 1 < 3)
            }
        }
        decrease.setOnClickListener {
            var a: Int = tvCase1.text.toString().toInt()
            if (--a >= 0) {
                tvCase1.text = "$a"
                tvCase2.text = "${a + 1}"
                tvCase3.text = "${a + 2}"
                tvCase3.visibility = if (tvCase3.text.toString().toInt() > max) View.INVISIBLE else View.VISIBLE
                tvCase1.visibility = if (a == 0) View.INVISIBLE else View.VISIBLE
                tvCaseInfo.text = "约${a + 1}碗米饭或${a + 3}碗粥 ${if (a + 1 < 3) "" else "(煮粥最多可选2个胶囊)"}"
                maxListener?.onMax(a + 1 < 3)
            }
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        updateViews()
    }

    override fun toString(): String = tvCase2.text.toString()

    interface MaxListener {
        fun onMax(boolean: Boolean)
    }
}