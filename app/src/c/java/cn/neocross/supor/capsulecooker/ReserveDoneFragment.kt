package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cn.neocross.libs.neosocket.bean.InstantMessage
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neocross.supor.capsulecooker.ui.activity.MainActivity
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.MessageDialogFragment
import cn.neorcoss.supor.capsulecooker.library.WorkType
import kotlinx.android.synthetic.c.fragment_reserve_done.*

/**
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ReserveDoneFragment : BaseFragment() {

    companion object {

        fun get(): ReserveDoneFragment {
            return ReserveDoneFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_reserve_done, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        tvType.text = "已预约${WorkType.getTypeString(app.workType.type)}"
        tvTimeDay.text = "将于${WorkType.getTimeSplit(app.workType.time!!)[0]}"
        tvTimeTime.text = WorkType.getTimeSplit(app.workType.time!!)[1]
        tvTimeSize.text = "煮好${app.workType.size}${app.workType.rice}"
        if (!app.isPassive()) {
            app.mClient?.send(InstantMessage(StatusType.TYPE_MSG, app.workType))
        }
        btnCancel.setOnClickListener {
            val dialog = MessageDialogFragment.create("是否确定取消预约？", "是", "再想想")
            dialog.show(childFragmentManager, "")
            dialog.listener = object : MessageDialogFragment.OnClickListener {
                override fun onPositiveListener() {
                    app.mClient?.send(InstantMessage(StatusType.TYPE_MSG,
                            if (app.workType.type == Constant.RICE) Constant.RESERVE_RICE_TEXT_CANCEL
                            else Constant.RESERVE_SLOP_TEXT_CANCEL))
                    app.resetWorkType()
                    Toast.makeText(context, "已取消预约", Toast.LENGTH_SHORT).show()
                    (activity as MainActivity).start()
                }
            }
        }
    }
}