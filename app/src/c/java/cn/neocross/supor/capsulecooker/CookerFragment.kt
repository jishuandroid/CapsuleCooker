package cn.neocross.supor.capsulecooker

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import cn.neocross.supor.capsulecooker.ui.activity.MainActivity
import cn.neocross.supor.capsulecooker.widget.CaseView
import cn.neocross.supor.capsulecooker.widget.SectorView
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import kotlinx.android.synthetic.c.fragment_cooker.*
import kotlinx.android.synthetic.c.view_item_buttons.*

/**
 * 首页
 * Created by shenhua on 2017-12-15-0015.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class CookerFragment : BaseFragment() {

    companion object {

        fun get(): CookerFragment {
            return CookerFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_cooker, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        setListener()
    }

    private fun setupViews() {
        viewChoiceSize.max = 4
        viewChoiceSize.maxListener = object : CaseView.MaxListener {
            override fun onMax(boolean: Boolean) {
                tvSlop.isEnabled = boolean
            }
        }
    }

    private fun setListener() {
        val app = activity.application as App
        sectorView.listener = object : SectorView.OnRotationListener {
            override fun onRotation(index: Int, isEmptyBox: Boolean) {
                sectorIndanView.notify(index)
                notifyViews(index)
            }
        }
        // 煮饭
        tvRice.setOnClickListener {
            app.workType.type = Constant.RICE
            app.workType.rice = tvRiceType.text.toString()
            app.workType.size = "$viewChoiceSize 个胶囊"
            nav(ProcessFragment.get())
        }
        // 煮粥
        tvSlop.setOnClickListener {
            app.workType.type = Constant.SLOP
            app.workType.rice = tvRiceType.text.toString()
            app.workType.size = "$viewChoiceSize 个胶囊"
            nav(ProcessFragment.get())
        }
        // 预约
        tvReserve.setOnClickListener {
            val rice = tvRiceType.text.toString()
            val size = "$viewChoiceSize 个胶囊"
            ReserveDialogFragment.get(rice, size).show(childFragmentManager, "dialog")
        }
        // 保温
        tvInsulation.setOnClickListener {
            nav(InsulationFragment.get())
        }
        // 开关
        btnPower.setOnClickListener {
            nav(PowerSwitchFragment.get())
        }
        // 饭煲设备
        view1.setOnClickListener { showDevices() }
    }

    private fun nav(fragment: Fragment) {
        (activity as MainActivity).replace(fragment)
    }

    private fun notifyViews(index: Int) {
        // TODO(2018/1/18) 米种的定位
        if (index == 0 || index == 1 || index == 14 || index == 13) {
            tvRiceType.text = "紫米"
            viewChoiceSize.max = 4
        } else if (index == 2 || index == 3 || index == 4) {
            tvRiceType.text = "东北大米"
            viewChoiceSize.max = 3
        } else if (index == 11 || index == 12) {
            tvRiceType.text = "长白珍珠"
            viewChoiceSize.max = 2
        } else {
            tvRiceType.text = "空胶囊"
            viewChoiceSize.max = 0
        }
        tvRice.isEnabled = tvRiceType.text.toString() != "空胶囊"
        tvSlop.isEnabled = tvRiceType.text.toString() != "空胶囊"
        tvReserve.isEnabled = tvRiceType.text.toString() != "空胶囊"
    }

    private fun showDevices() {
        val popupWindow = PopupWindow(context)
        popupWindow.width = 360
        popupWindow.height = ViewGroup.LayoutParams.WRAP_CONTENT
        popupWindow.isFocusable = true
        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = true
        popupWindow.setBackgroundDrawable(ColorDrawable(0))
        val contentView = LayoutInflater.from(context).inflate(R.layout.pop_device, null)
        popupWindow.contentView = contentView
        contentView.setOnClickListener {
            popupWindow.dismiss()
        }
        popupWindow.showAsDropDown(view1, -60, 20)
    }
}