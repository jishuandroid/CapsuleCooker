package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.supor.capsulecooker.ui.activity.MainActivity
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.WorkType
import cn.neorcoss.supor.capsulecooker.library.helper.WheelPickHelper
import kotlinx.android.synthetic.c.dialog_view_reserve.*
import kotlinx.android.synthetic.main.view_wheel_picker.*

/**
 * Created by shenhua on 2018-01-16-0016.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ReserveDialogFragment : DialogFragment() {

    private var currentType = cn.neorcoss.supor.capsulecooker.library.Constant.RESERVE_RICE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogStyle)
    }

    companion object {
        fun get(rice: String, size: String): ReserveDialogFragment {
            val fragment = ReserveDialogFragment()
            val bundle = Bundle()
            bundle.putString("rice", rice)
            bundle.putString("size", size)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.dialog_view_reserve, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rice = arguments.getString("rice")
        val size = arguments.getString("size")
        tyChoice.text = "当前选择：${rice}；数量：${size}"
        tvTypeRice.isSelected = true
        tvTypeRice.setOnClickListener {
            currentType = Constant.RESERVE_RICE
            tvTypeRice.isSelected = true
            tvTypeSlop.isSelected = false
        }
        tvTypeSlop.setOnClickListener {
            currentType = Constant.RESERVE_SLOP
            tvTypeRice.isSelected = false
            tvTypeSlop.isSelected = true
        }
        val wheelHelper = WheelPickHelper()
        wheelHelper.setPicker(pickerData, pickerHour, pickerMinute)
        btnStart.setOnClickListener {
            dismiss()
            (activity.application as App).workType = WorkType(currentType,
                    Constant.MODE_CAPSULE,
                    rice, size, wheelHelper.toString())
            (activity as MainActivity).replace(ReserveDoneFragment.get())
        }
//        btnClose.setOnClickListener { dismiss() }
    }
}