package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cn.neocross.libs.neosocket.bean.InstantMessage
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neocross.supor.capsulecooker.ui.activity.MainActivity
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.WorkType
import kotlinx.android.synthetic.c.fragment_process.*

/**
 * 进程处理
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ProcessFragment : BaseFragment() {

    companion object {

        fun get(): ProcessFragment {
            return ProcessFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_process, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        tvType.text = "正在准备开始${WorkType.getTypeString(app.workType.type)}"
        tvSize.text = "${app.workType.size} ${app.workType.rice}"
        if (!app.isPassive()) {
            app.mClient?.send(InstantMessage(StatusType.TYPE_MSG, app.workType))
        }
        btnCancel.setOnClickListener {
            Toast.makeText(context, "已取消", Toast.LENGTH_SHORT).show()
            (activity as MainActivity).start()
        }
        object : CountDownTimer(4000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                if (!isDestroy) {
                    val time = millisUntilFinished / 1000
                    tvTimer.text = "$time 秒"
                }
            }

            override fun onFinish() {
                if (!isDestroy) {
                    btnCancel.visibility = View.GONE
                    tvType.visibility = View.GONE
                    viewPrepare.visibility = View.GONE
                    viewJiami.visibility = View.VISIBLE
                    starting()
                }
            }
        }.start()
    }

    private fun starting() {
        object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                if (!isDestroy) {
                    val fragment = StartFragment.get()
                    (activity as MainActivity).nav(fragment)
                }
            }
        }.start()
    }
}