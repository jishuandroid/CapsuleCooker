package cn.neocross.supor.capsulecooker.widget

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View

/**
 * 15等份扇形胶囊表示的状态界面
 * Created by shenhua on 2017/12/17.
 * Email shenhuanet@126.com
 */
class SectorIndanView @JvmOverloads constructor(context: Context, attributeSet: AttributeSet? = null, defStyleAttr: Int = 0)
    : View(context, attributeSet, defStyleAttr) {

    /**
     * 存放扇形图各等份颜色
     */
    private var mArcStrokePaint: Paint = Paint()
    private var mArcPaint: Paint = Paint()
    private var mCirclePaint: Paint
    private var mWaterPaint: Paint
    private var mTextPaint: Paint
    private var colors: IntArray? = null
    private var mRectF: RectF? = null
    private var mWidth: Int = 0
    private var mHeight: Int = 0
    /**
     * 扇形外圆半径
     */
    private var mRadius: Int = 200
    /**
     * 内圆半径占比外圆半径 0～1
     */
    private var mInsideRatio: Float = 0.4f
    var sizes: IntArray = intArrayOf(2, 4, 3)
    var progress: Int = 50
    private var textBound: Rect? = null
    private var textProgress: String = "50%"

    init {
        colors = intArrayOf(Color.parseColor("#FEFF00"),
                Color.parseColor("#9292FF"),
                Color.parseColor("#E781F2"))

        mArcStrokePaint.isAntiAlias = true
        mArcStrokePaint.style = Paint.Style.STROKE
        mArcStrokePaint.strokeWidth = 3f
        mArcStrokePaint.color = Color.BLACK

        mRectF = RectF()
        mArcPaint.isAntiAlias = true
        mArcPaint.style = Paint.Style.FILL
        mArcPaint.color = colors!![1]

        mCirclePaint = Paint(mArcPaint)
        mCirclePaint.color = Color.WHITE
        mWaterPaint = Paint(mArcPaint)
        mWaterPaint.color = Color.parseColor("#D6EEFA")

        mTextPaint = Paint(mArcPaint)
        mTextPaint.textSize = 50f
        mTextPaint.color = Color.BLACK
        textBound = Rect()
        mTextPaint.getTextBounds(textProgress, 0, textProgress.length, textBound)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)

        mWidth = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> 2 * mRadius + paddingLeft + paddingRight
            MeasureSpec.UNSPECIFIED -> Math.min(2 * mRadius + paddingLeft + paddingRight, widthSize)
            else -> Math.min(2 * mRadius + paddingLeft + paddingRight, widthSize)
        }
        mHeight = when (heightMode) {
            MeasureSpec.EXACTLY -> heightSize
            MeasureSpec.AT_MOST -> mRadius * 2 + paddingTop + paddingBottom
            MeasureSpec.UNSPECIFIED -> Math.min(mRadius * 2 + paddingTop + paddingBottom, heightSize)
            else -> Math.min(mRadius * 2 + paddingTop + paddingBottom, heightSize)
        }
        setMeasuredDimension(mWidth, mHeight)
        mRadius = Math.min(mWidth - paddingLeft - paddingRight, mHeight - paddingTop - paddingBottom) / 2
        mRectF!!.set(-mRadius.toFloat(), -mRadius.toFloat(), mRadius.toFloat(), mRadius.toFloat())
    }

    private val radius: Float = mInsideRatio * mRadius

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas!!.translate(mWidth / 2f, mHeight / 2f)
        canvas.drawArc(mRectF, 24f * 3 + 6, 24f, true, mArcStrokePaint)
        canvas.drawArc(mRectF, 24f * 3 + 6, 24f, true, mArcPaint)
        // 水波纹
        canvas.drawCircle(0f, 0f, radius, mCirclePaint)
        for (i in (-radius.toInt()) until radius.toInt()) {
            canvas.drawLine(i.toFloat(), getStartY(i), i.toFloat(), getStopY(i), mWaterPaint)
        }
        canvas.drawText(textProgress, -textBound!!.width() / 2f, textBound!!.height() / 4f, mTextPaint)
    }

    private fun getStopY(i: Int): Float {
        return Math.sqrt(Math.pow(radius.toDouble(), 2.0) - Math.pow(i.toDouble(), 2.0)).toFloat()
    }

    private fun getStartY(i: Int): Float {
        return (5 * Math.sin((2 * Math.PI / radius) * i)).toFloat()
    }

    fun notify(index: Int) {
        println("index: $index")
        if (index == 0 || index == 1 || index == 14 || index == 13) {
            mArcPaint.color = colors!![1]
        } else if (index == 2 || index == 3 || index == 4) {
            mArcPaint.color = colors!![2]
        } else if (index == 11 || index == 12) {
            mArcPaint.color = colors!![0]
        } else {
            mArcPaint.color = Color.WHITE
        }
        invalidate()
    }
}