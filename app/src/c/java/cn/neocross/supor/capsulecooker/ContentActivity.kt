package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * 内容界面
 * Created by shenhua on 2017/12/16.
 * Email shenhuanet@126.com
 */
class ContentActivity : AppCompatActivity() {

    /**
     * 兼容B方案
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)
    }
}