package cn.neocross.supor.capsulecooker

import android.support.v4.app.Fragment

/**
 * Created by shenhua on 2018/1/19.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class PowerSwitchFragment : Fragment() {

    /**
     * 兼容B方案（B方案本无开关）
     */
    companion object {
        fun get(): PowerSwitchFragment {
            return PowerSwitchFragment()
        }
    }
}