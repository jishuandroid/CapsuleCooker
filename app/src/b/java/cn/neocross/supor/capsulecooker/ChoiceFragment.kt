package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import cn.neocross.supor.capsulecooker.ui.widget.CardRiceView
import cn.neocross.supor.capsulecooker.ui.widget.HorizontalWheelView
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.WorkType
import kotlinx.android.synthetic.b.fragment_choice.*

/**
 * 选择米种(预约选择/正常选择)
 * Created by shenhua on 2017/12/18.
 * Email shenhuanet@126.com
 */
class ChoiceFragment : BaseFragment() {

    companion object {

        fun get(): ChoiceFragment {
            return ChoiceFragment()
        }
    }

    val rices = arrayOf("泰国香稻米", "珍珠雪原米", "东北五常大米")
    val sizes = arrayOf("5个胶囊", "6个胶囊", "3个胶囊")

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_choice, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        setupCardView()
    }

    /**
     * 设置卡片
     */
    private fun setupCardView() {
        card.setAdapter(object : BaseAdapter() {

            override fun getCount(): Int {
                return rices.size
            }

            override fun getItem(position: Int): Any {
                return position
            }

            override fun getItemId(position: Int): Long {
                return position.toLong()
            }

            override fun getView(position: Int, child: View?, parent: ViewGroup): View {
                var child = child
                if (child == null) {
                    child = LayoutInflater.from(parent.context).inflate(R.layout.item_card_rice, parent, false)
                }

                (child!!.findViewById<View>(R.id.tvRiceName) as TextView).text = rices[position]
                (child.findViewById<View>(R.id.tvRiceCount) as TextView).text = sizes[position]
                setupWheelView(child, position)
                return child
            }

            /**
             * 设置横向选择
             */
            private fun setupWheelView(child: View, position: Int) {
                val items = ArrayList<String>()
                val count = when (position) {
                    0 -> 5
                    1 -> 6
                    2 -> 3
                    else -> 5
                }
                (1 until count + 1).mapTo(items) { "$it" }
                val wheel = child.findViewById<HorizontalWheelView>(R.id.wheelView)
                wheel.offset = 5
                wheel.setItems(items)
                wheel.setSelection(2)
                wheel.setOnWheelListener(object : HorizontalWheelView.OnWheelViewListener {

                    override fun onSelected(selectedIndex: Int, item: String) {
                        child.findViewById<TextView>(R.id.tvWan).text = "约${item}碗饭，${item.toInt() - 1}-${item}人份"
                        (activity.application as App).workType.size = "${item}个胶囊"
                    }
                })
            }
        })
        card.setOnTurnListener(object : CardRiceView.OnTurnListener {
            override fun onTurned(position: Int) {
                try {
                    (activity.application as App).workType.rice = rices[position]
                    // TODO(卡片滑回去的时候，被选择的米量未更新)
                } catch (e: Exception) {
                    e.printStackTrace()
                    // activity 空指针
                }
            }
        })
    }

    private fun setupViews() {
        val app = activity.application as App
        app.workType.rice = rices[0]
        app.workType.size = "3个胶囊"
        if (app.workType.type == Constant.DEFAULT) {
            // 预约
            app.workType.type = Constant.RESERVE_RICE
            (activity as ContentActivity).setTitle("米种选择")
            btnStart.text = "开始预约"
            tabLayout.visibility = View.VISIBLE
            tabLayout.removeAllTabs()
            tabLayout.addTab(tabLayout.newTab().setText("预约煮饭"))
            tabLayout.addTab(tabLayout.newTab().setText("预约煮粥"))
            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabReselected(tab: TabLayout.Tab?) {}

                override fun onTabUnselected(tab: TabLayout.Tab?) {}

                override fun onTabSelected(tab: TabLayout.Tab?) {
                    app.workType.type = if (tab!!.position == 0) Constant.RESERVE_RICE else Constant.RESERVE_SLOP
                }
            })
            btnStart.setOnClickListener {
                (activity as ContentActivity).setFragment(ReserveDoneFragment.get(), true)
            }
        } else {
            // 正常
            tabLayout.visibility = View.GONE
            (activity as ContentActivity).setTitle(WorkType.getTypeString(app.workType.type))
            btnStart.text = "开始${WorkType.getTypeString(app.workType.type)}"
            btnStart.setOnClickListener {
                (activity as ContentActivity).setFragment(ProcessFragment.get(), false)
            }
        }
    }
}