package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.supor.capsulecooker.adapter.HistoryAdapter
import cn.neocross.supor.capsulecooker.ui.widget.HorizontalWheelView
import kotlinx.android.synthetic.b.fragment_history.*

/**
 * 历史记录
 * Created by shenhua on 2017/12/18.
 * Email shenhuanet@126.com
 */
class HistoryFragment : Fragment() {

    companion object {
        fun get(): HistoryFragment {
            return HistoryFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as ContentActivity).setTitle("")
        setupWheelView()
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = HistoryAdapter(context, (0..20).mapTo(java.util.ArrayList<String>()) { it.toString() })
    }

    /**
     * 设置横向选择
     */
    private fun setupWheelView() {
        val items = ArrayList<String>()
        (1 until 13).mapTo(items) { "${it}月" }
        wheelView.offset = 5
        wheelView.setItems(items)
        wheelView.setSelection(4)
        wheelView.setOnWheelListener(object : HorizontalWheelView.OnWheelViewListener {

            override fun onSelected(selectedIndex: Int, item: String) {

            }
        })
    }

    override fun toString(): String = HistoryFragment::class.java.name
}