package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.Spannable
import android.text.SpannableString
import android.text.style.AbsoluteSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cn.neocross.libs.neosocket.bean.InstantMessage
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neocross.supor.capsulecooker.ui.widget.CountDownTimeView
import cn.neocross.supor.capsulecooker.ui.widget.ProgressDownView
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.MessageDialogFragment
import cn.neorcoss.supor.capsulecooker.library.WorkType
import kotlinx.android.synthetic.b.fragment_start.*

/**
 * 煮饭/煮粥 进行中……
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class StartFragment : BaseFragment() {

    companion object {

        fun get(): StartFragment {
            return StartFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_start, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = (activity.application as App)
        val type = WorkType.getTypeString(app.workType.type)
        (activity as ContentActivity).setTitle("${type}中")
        btnCancel.text = "取消$type"
        tvStarting.text = "完成${type}剩余计时"
        tvStart.text = "正在${type}中"
        tvRice.text = app.workType.rice
        val sp = SpannableString(app.workType.size)
        sp.setSpan(AbsoluteSizeSpan(30, true), 0, 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        tvRiceSize.text = sp
        (activity as ContentActivity).invalidateOptionsMenu()
        btnCancel.setOnClickListener {
            val dialog = MessageDialogFragment.create("米已经加了\n确定取消吗？", "是", "再想想")
            dialog.show(childFragmentManager, "")
            dialog.listener = object : MessageDialogFragment.OnClickListener {
                override fun onPositiveListener() {
                    app.mClient?.send(InstantMessage(StatusType.TYPE_MSG,
                            if (app.workType.type == Constant.RICE) Constant.RICE_TEXT_CANCEL
                            else Constant.SLOP_TEXT_CANCEL))
                    Toast.makeText(context, "已取消$type", Toast.LENGTH_SHORT).show()
                    app.resetWorkType()
                    (activity as ContentActivity).onBackPressed()
                }
            }
        }
        progress.setOnStartCountDownListener(object : ProgressDownView.OnStartCountDownListener {
            override fun onStart() {
                countDownTimeView.initTime(0, 3, 50)
                countDownTimeView.start()
                countDownTimeView.setOnTimeCompleteListener(object : CountDownTimeView.OnTimeCompleteListener {
                    override fun onProgress(p: Int) {
                        progress.updateProgress(p)
                    }

                    override fun onComplete() {
                        Toast.makeText(context, "倒计时完成!", Toast.LENGTH_SHORT).show()
                    }
                })
            }
        })
        ivRedPack.setOnClickListener {
            val dialog = AlertDialog.Builder(context)
            dialog.setView(R.layout.dialog_winning)
            dialog.setTitle("红包")
            dialog.setPositiveButton("分享", null)
            dialog.show()
        }
    }
}