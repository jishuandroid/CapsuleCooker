package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Spannable
import android.text.SpannableString
import android.text.style.AbsoluteSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.b.frag_water_status.*
import java.util.*

/**
 * 水量状态
 * Created by shenhua on 2017/12/16.
 * Email shenhuanet@126.com
 */
class WaterStatusFragment : Fragment() {

    companion object {
        fun get(): WaterStatusFragment {
            return WaterStatusFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.frag_water_status, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as ContentActivity).setTitle("水量")
        val progress = Random().nextInt(100)
        rulerView.progress = progress
        waveView.setProgress(progress)
        val sp = SpannableString(getWater(progress))
        sp.setSpan(AbsoluteSizeSpan(60, true), 0, 3, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        sp.setSpan(AbsoluteSizeSpan(20, true), 3, 5, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        tvVolume.text = sp
        if (progress < 20) {
            tvInfo.visibility = View.VISIBLE
        }
        tvDesc.text = "当前水量${sp}，占${progress}%，上次换水4天前"
    }

    private fun getWater(progress: Int): String {
        val p = 600 * progress / 100
        return when {
            p.toString().length == 1 -> "00${p.toString()}ml"
            p.toString().length == 2 -> "0${p.toString()}ml"
            else -> "${p.toString()}ml"
        }
    }

    override fun toString(): String = WaterStatusFragment::class.java.name
}