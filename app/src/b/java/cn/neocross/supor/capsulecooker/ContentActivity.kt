package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import cn.neorcoss.supor.capsulecooker.library.ViewUtils
import kotlinx.android.synthetic.b.activity_content.*

/**
 * 内容界面
 * Created by shenhua on 2017/12/16.
 * Email shenhuanet@126.com
 */
class ContentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ViewUtils.full(window)
        setContentView(R.layout.activity_content)
        setupViews()
        setupFragment()
    }

    /**
     * 初始化界面
     */
    private fun setupViews() {
        setSupportActionBar(toolbar)
        supportActionBar!!.title = null
//        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
//        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbar_back.setOnClickListener {
            if (supportFragmentManager.findFragmentById(R.id.container) is ChoiceFragment) {
                (application as App).resetWorkType()
                if (supportFragmentManager.backStackEntryCount > 0) supportFragmentManager.popBackStack()
                else finish()
            } else if (supportFragmentManager.findFragmentById(R.id.container) is ProcessFragment) {
                (application as App).resetWorkType()
                finish()
            } else {
                finish()
            }
        }
    }

    /**
     * 自主填充Fragment
     */
    private fun setupFragment() {
        val fragment = intent.getStringExtra("fragment")
        when (fragment) {
            WaterStatusFragment.get().toString() -> setFragment(WaterStatusFragment.get())
            RiceStatusFragment.get().toString() -> setFragment(RiceStatusFragment.get())
            RiceDetailFragment.get().toString() -> setFragment(RiceDetailFragment.get())
            InsulationFragment.get().toString() -> setFragment(InsulationFragment.get())
            ChoiceFragment.get().toString() -> setFragment(ChoiceFragment.get())
            ProcessFragment.get().toString() -> setFragment(ProcessFragment.get())
            StartFragment.get().toString() -> setFragment(StartFragment.get())
            ReserveFragment.get().toString() -> setFragment(ReserveFragment.get())
            ReserveDoneFragment.get().toString() -> setFragment(ReserveDoneFragment.get())
        }
    }

    private fun setFragment(fragment: Fragment) {
        setFragment(fragment, false)
    }

    /**
     * 本activity设置fragment
     */
    fun setFragment(fragment: Fragment, needBackStack: Boolean) {
        if (isFinishing) {
            return
        }
        if (needBackStack) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .addToBackStack(fragment.toString())
                    .commit()
        } else {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit()
        }
    }

    /**
     * 更新标题
     */
    fun setTitle(title: String) {
        toolbar_title.text = title
    }

    /**
     * 更新返回按钮文字
     */
    fun setBackTitle(title: String) {
        toolbar_back.text = title
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        if (fragment is ReserveDoneFragment || fragment is StartFragment) {
            menuInflater.inflate(R.menu.share_scan, menu)
            menu!!.findItem(R.id.id_menu_scan).isVisible = false
        }
        if (fragment is InsulationFragment || fragment is ProcessFragment
                || fragment is StartFragment || fragment is ReserveDoneFragment) {
            setBackTitle("首页")
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == R.id.id_menu_share) {
            val builder = AlertDialog.Builder(this)
            builder.setView(layoutInflater.inflate(R.layout.dialog_share, null))
            builder.setPositiveButton("查看记录", null)
            builder.create().show()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (isFinishing) {
            return
        }
        if (supportFragmentManager.findFragmentById(R.id.container) is ReserveDoneFragment) {
            finish()
            return
        }

        if (supportFragmentManager.backStackEntryCount > 0)
            supportFragmentManager.popBackStack()
        else finish()
    }
}