package cn.neocross.supor.capsulecooker

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.libs.neosocket.bean.InstantMessage
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.WorkType
import kotlinx.android.synthetic.b.fragment_process.*

/**
 * 煮饭/煮粥 过程
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ProcessFragment : BaseFragment() {

    companion object {

        fun get(): ProcessFragment {
            return ProcessFragment()
        }
    }

    private lateinit var titles: Array<String>
    private var times: Array<String> = arrayOf("00:01:02", "00:00:02", "")
    private var volues: Array<String> = arrayOf("2个胶囊", "30ml", "")

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_process, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        Thread(Runnable {
            for (i in 0..50) {
                Thread.sleep(50)
                mHandler.obtainMessage(1, i).sendToTarget()
            }
            mHandler.obtainMessage(2).sendToTarget()
            for (i in 51..100) {
                Thread.sleep(100)
                mHandler.obtainMessage(2, i).sendToTarget()
            }
            Thread.sleep(1000)
            mHandler.obtainMessage(3).sendToTarget()
        }).start()
    }

    /**
     * 注水动画及文字适配
     */
    private fun setupViews() {
        val app = activity.application as App
        (activity as ContentActivity).invalidateOptionsMenu()
        if (!app.isPassive()) {
            app.mClient?.send(InstantMessage(StatusType.TYPE_MSG, app.workType))
        }
        tvRice.text = "${app.workType.rice} ${app.workType.size}"
        titles = when (app.workType.type) {
            Constant.RICE -> arrayOf("切米", "注水", "煮饭")
            Constant.SLOP -> arrayOf("切米", "注水", "煮粥")
            else -> arrayOf("切米", "注水", "煮饭")
        }
        stepView.setTitles(titles, times, volues)
    }

    private val mHandler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            try {
                if (msg.what == 1) {
                    (activity as ContentActivity).setTitle(
                            "${WorkType.getTypeString((activity.application as App).workType.type)}切米")
                    tvStatus.text = "正在切米中"
                    progress.setProgress(msg.obj as Int)
                    stepView.progress = msg.obj as Int
                }
                if (msg.what == 2) {
                    (activity as ContentActivity).setTitle(
                            "${WorkType.getTypeString((activity.application as App).workType.type)}注水")
                    tvStatus.text = "正在注水中"
                    progress.setProgress(msg.obj as Int)
                    stepView.progress = msg.obj as Int
                }
                if (msg.what == 3) {
                    (activity as ContentActivity)
                            .setFragment(StartFragment.get(), false)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}