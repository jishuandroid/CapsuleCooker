package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.os.SystemClock
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.supor.capsulecooker.ui.activity.MainActivity
import cn.neocross.supor.capsulecooker.ui.widget.CountDownTimeView
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import kotlinx.android.synthetic.b.fragment_cooker.*

/**
 * 首页
 * Created by shenhua on 2017-12-15-0015.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class CookerFragment : BaseFragment() {

    companion object {

        fun get(): CookerFragment {
            return CookerFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_cooker, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity.application as App
        if (app.workType.type != Constant.DEFAULT) {
            clWorkStatus.visibility = View.VISIBLE
            clFunction.visibility = View.GONE
        } else {
            clWorkStatus.visibility = View.GONE
            clFunction.visibility = View.VISIBLE
        }

        progress1.text = "水量"
        progress2.text = "米量"
        progress1.setProgress(intArrayOf(90))
        progress2.setProgress(intArrayOf(5, 20, 30, 30, 40))

        when (app.workType.type) {
            Constant.RICE -> tvStatus.text = "完成煮饭剩余时间"
            Constant.SLOP -> tvStatus.text = "完成煮粥剩余时间"
            Constant.RESERVE_RICE, Constant.RESERVE_SLOP -> tvStatus.text = "预约剩余时间"
            Constant.INSULATION -> tvStatus.text = "已保温时长"
        }

        when (app.workType.type) {
            Constant.RICE, Constant.SLOP, Constant.RESERVE_RICE, Constant.RESERVE_SLOP -> {
                tvTime.visibility = View.VISIBLE
                tvTime.initTime(0, 35, 0)
                tvTime.start()
                tvTime.setOnTimeCompleteListener(object : CountDownTimeView.OnTimeCompleteListener {
                    override fun onProgress(p: Int) {}
                    override fun onComplete() {}
                })
            }
            Constant.INSULATION -> {
                tvTime1.visibility = View.VISIBLE
                tvTime1.base = SystemClock.elapsedRealtime()
                val hour = ((SystemClock.elapsedRealtime() - tvTime1.base) / 1000 / 60).toInt()
                tvTime1.format = "0" + hour.toString() + ":%s"
                tvTime1.start()
            }
        }
        clWorkStatus.setOnClickListener { navToWork(app) }
        progress1.setOnClickListener { navActivity(WaterStatusFragment.get()) }
        progress2.setOnClickListener { navActivity(RiceStatusFragment.get()) }
        slInsulation.setOnClickListener { navActivity(InsulationFragment.get()) }
        slReserve.setOnClickListener { navActivity(ReserveFragment.get()) }
        slStartRice.setOnClickListener {
            app.workType.type = Constant.RICE
            navActivity(ChoiceFragment.get())
        }
        slStartSlop.setOnClickListener {
            app.workType.type = Constant.SLOP
            navActivity(ChoiceFragment.get())
        }
    }

    private fun navToWork(app: App) {
        when (app.workType.type) {
            Constant.RICE, Constant.SLOP -> {
                navActivity(StartFragment.get())
            }
            Constant.RESERVE_RICE, Constant.RESERVE_SLOP -> {
                navActivity(ReserveDoneFragment.get())
            }
            Constant.INSULATION -> {
                navActivity(InsulationFragment.get())
            }
        }
    }

    private fun navActivity(fragment: Fragment) {
        (activity as MainActivity).navTo(fragment)
    }
}