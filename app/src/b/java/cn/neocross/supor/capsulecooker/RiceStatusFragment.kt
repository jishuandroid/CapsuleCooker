package cn.neocross.supor.capsulecooker

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.supor.capsulecooker.adapter.Rice
import cn.neocross.supor.capsulecooker.adapter.RiceStatusAdapter
import cn.neorcoss.supor.capsulecooker.library.BaseRecyclerAdapter
import kotlinx.android.synthetic.b.frag_rice_status.*
import java.util.*

/**
 * 米量状态
 * Created by shenhua on 2017/12/16.
 * Email shenhuanet@126.com
 */
class RiceStatusFragment : Fragment() {

    companion object {
        fun get(): RiceStatusFragment {
            return RiceStatusFragment()
        }
    }

    private var adapter: RiceStatusAdapter? = null
    private val rices = arrayOf("泰国香稻米", "珍珠雪原米", "东北五常大米", "大米", "好米")
    private val sizes = arrayOf("5个胶囊", "3个胶囊", "2个胶囊", "3个胶囊", "2个胶囊")
    private val descs = "细腻绵软，东北最好的土地产出的最好的大米"

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.frag_rice_status, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as ContentActivity).setTitle("米量")
        recyclerView.layoutManager = LinearLayoutManager(context)
        adapter = RiceStatusAdapter(context, getDatas())
        recyclerView.adapter = adapter
        adapter!!.setOnItemClickListener(object : BaseRecyclerAdapter.OnItemClickListener<Rice> {

            override fun onItemClick(view: View, position: Int, data: Rice) {
                startActivity(Intent(context, ContentActivity::class.java)
                        .putExtra("fragment", RiceDetailFragment().toString()))
            }
        })
    }

    private fun getDatas(): ArrayList<Rice>? {
        return (0..4).mapTo(ArrayList<Rice>()) { Rice(rices[it], descs, sizes[it]) }
    }

    override fun toString(): String = RiceStatusFragment::class.java.name
}