package cn.neocross.supor.capsulecooker.ui.widget

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.content.Context
import android.database.DataSetObserver
import android.graphics.Rect
import android.support.v4.view.ViewCompat
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import android.view.animation.LinearInterpolator
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.ListAdapter
import cn.neocross.supor.capsulecooker.R
import java.util.*


class CardRiceView @JvmOverloads constructor(context: Context, attr: AttributeSet? = null, defStyleAttr: Int = 0)
    : FrameLayout(context, attr, defStyleAttr) {

    private var mTouchSlop: Int = 0
    private var mPosition = 0
    private var offsetsX = intArrayOf(-dp2px(30), -dp2px(27), -dp2px(13), dp2px(0), dp2px(5))
    private var offsetsY = intArrayOf(-dp2px(40), -dp2px(34), -dp2px(17), dp2px(0), 0)
    private var mHeight = 1f
    private var mWidth = 1f
    private var mRecycler: Queue<View> = ArrayDeque(MAX_CHILD_COUNT)
    var mAdapter: ListAdapter? = null
    private var mStartX: Float = 0f
    private var mStartY: Float = 0f
    private var mRatio: Float = 0f
    private var mIsTurning: Boolean = false
    private var mWheelRect: Rect
    private var mIsAnimating: Boolean = false
    private var mListener: OnTurnListener? = null
    private var mDataSetObserver: DataSetObserver = object : DataSetObserver() {

        override fun onChanged() {
            turnAll(true)
        }
    }

    companion object {
        internal val MAX_CHILD_COUNT = 5
        internal val MAX_VISIBLE_COUNT = 3
        internal val NEWER_OFFSET = -1
    }

    init {
        clipToPadding = false
        isClickable = true
        mTouchSlop = ViewConfiguration.get(context).scaledTouchSlop
        mTouchSlop *= mTouchSlop
        setPadding(dp2px(15), dp2px(50), dp2px(15), dp2px(15))
        mWheelRect = Rect()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        offsetsY[4] = measuredHeight - paddingTop
        mHeight = (measuredHeight - paddingTop - paddingBottom).toFloat()
        mWidth = (measuredWidth - paddingLeft - paddingRight).toFloat()
        if (!mIsAnimating) {
            reset()
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (mIsAnimating || mAdapter == null || mAdapter!!.count == 0) {
            return super.dispatchTouchEvent(ev)
        }
        when (ev!!.action) {
            MotionEvent.ACTION_DOWN -> {
                mStartX = ev.x
                mStartY = ev.y
                if (needDispatchToChild()) {
                    return super.dispatchTouchEvent(ev)
                }
                mIsTurning = false
                mRatio = 0f
            }
            MotionEvent.ACTION_MOVE -> {
                if (needDispatchToChild()) {
                    return super.dispatchTouchEvent(ev)
                }
                val dx = (ev.x - mStartX).toInt()
                val dy = (ev.y - mStartY).toInt()
                if (dx * dx + dy * dy > mTouchSlop) {
                    mRatio = dy * 1f / measuredHeight
                    turning(mRatio)
                    mIsTurning = true
                    return true
                }
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> if (mIsTurning) {
                mIsTurning = false
                val threshold = 0.1f
                if (mRatio > threshold && mPosition < mAdapter!!.count - 1) { // older
                    TurnAnimator().turn(true, mRatio)
                } else if (-mRatio > threshold && mPosition > 0) { // newer
                    TurnAnimator().turn(true, mRatio)
                } else {
                    TurnAnimator().turn(false, mRatio)
                }
                return true
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    fun setAdapter(adapter: ListAdapter) {
        if (mAdapter != null) {
            mAdapter!!.unregisterDataSetObserver(mDataSetObserver)
        }
        mAdapter = adapter
        if (mAdapter != null) {
            mAdapter!!.registerDataSetObserver(mDataSetObserver)
        }
        mPosition = 0
        turnAll(true)
    }

    fun setOnTurnListener(listener: OnTurnListener) {
        mListener = listener
    }

    private fun turnAll(isDown: Boolean) {
        while (childCount > 0) {
            recycle(0)
        }
        val initial = if (isDown) 0 else 4
        val count = Math.min(mAdapter!!.count - mPosition, 4)
        val animators = ArrayList<Animator>()
        for (i in -1 until count) {
            if (mPosition + i < 0) {
                continue
            }
            val child = mAdapter!!.getView(mPosition + i, mRecycler.poll(), this)
            if (child.layoutParams == null) {
                child.layoutParams = generateDefaultLayoutParams()
            }
            addViewInLayout(child, 0, child.layoutParams, true)
            if (i == -1) {
                resetChild(child, 4, mWidth)
            } else if (i == 3) {
                resetChild(child, 0, mWidth)
            } else {
                resetChild(child, initial, mWidth)
                val animator = TurnOneAnimator(child, initial, 3 - i)
                if (isDown) {
                    animators.add(animator)
                } else {
                    animators.add(0, animator)
                }
            }
        }
        val set = AnimatorSet()
        set.playSequentially(animators)
        set.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {
                mIsAnimating = true
            }

            override fun onAnimationEnd(animation: Animator) {
                mIsAnimating = false
                reset()
                if (mListener != null) {
                    mListener!!.onTurned(mPosition)
                }
            }
        })
        set.start()
    }

    private fun turnAll(isDown: Boolean, clean: Boolean, position: Int) {
        var childCount = childCount
        if (childCount == 5) {
            recycle(childCount - 1)
            recycle(0)
        } else if (childCount > 0 && position != 0) {
            recycle(childCount - 1)
        } else if (childCount > 0 && position + MAX_VISIBLE_COUNT < mAdapter!!.count - 1) {
            recycle(0)
        }
        childCount = getChildCount()
        val initial = if (isDown) 4 else 0
        val animators = ArrayList<Animator>()
        for (i in 0 until childCount) {

            val child = getChildAt(i)
            val animator = TurnOneAnimator(child, 3 - childCount + i, initial)
            if (isDown) {
                animators.add(0, animator)
            } else {
                animators.add(animator)
            }
        }
        val set = AnimatorSet()
        set.playSequentially(animators)
        set.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {
                mIsAnimating = true
            }

            override fun onAnimationEnd(animation: Animator) {
                mIsAnimating = false
                while (getChildCount() > 0) {
                    recycle(0)
                }
                if (clean) {
                    turnAll(isDown)
                }
            }
        })
        set.start()
    }

    private fun needDispatchToChild(): Boolean {
        val root = findViewById<CardView>(R.id.cardRoot)
        val wheelFather = root.findViewById<LinearLayout>(R.id.llWheel)
        val wheel = root.findViewById<HorizontalWheelView>(R.id.wheelView)
        mWheelRect.set(90,
                wheel.top + root.top + wheelFather.top,
                985,
                wheel.top + wheel.height + root.top + wheelFather.top)
        return mWheelRect.contains(mStartX.toInt(), mStartY.toInt())
    }

    private fun recycle(i: Int) {
        val child = getChildAt(i)
        removeViewInLayout(child)
        mRecycler.offer(child)
    }

    private fun scaleX(from: Int, to: Int, ratio: Float, width: Float): Float {
        val delta = offsetsX[from] + (offsetsX[to] - offsetsX[from]) * ratio
        return (width + delta) / width
    }

    private fun transY(from: Int, to: Int, ratio: Float): Float {
        return offsetsY[from] + (offsetsY[to] - offsetsY[from]) * ratio
    }

    private fun resetChild(child: View, n: Int, width: Float) {
        val scale = (width + offsetsX[n]) / width
        child.pivotY = 0f
        child.pivotX = width / 2
        child.scaleY = scale
        child.scaleX = scale
        child.translationY = offsetsY[n].toFloat()
        child.alpha = (if (n == 0) 0 else 1).toFloat()
        ViewCompat.setTranslationZ(child, (n - MAX_CHILD_COUNT).toFloat())
    }

    private fun startIndex(count: Int): Int {
        return MAX_CHILD_COUNT - count - if (mPosition == 0 && count < MAX_CHILD_COUNT) 1 else 0
    }

    private fun updateChild(child: View, from: Int, to: Int, ratio: Float) {
        val scale = scaleX(from, to, ratio, mWidth)
        child.translationY = transY(from, to, ratio)
        child.scaleX = scale
        child.scaleY = scale
        child.alpha = if (to == 0) 1 - ratio else 1f
    }

    private fun older() {
        mPosition++
        if (mPosition != 1) {
            recycle(childCount - 1)
        }
        val olderPosition = mPosition + MAX_VISIBLE_COUNT
        if (olderPosition < mAdapter!!.count) {
            addView(mAdapter!!.getView(olderPosition, mRecycler.poll(), this), 0)
        }
        if (mListener != null) {
            mListener!!.onTurned(mPosition)
        }
    }

    private fun newer() {
        mPosition--
        if (childCount == MAX_CHILD_COUNT) {
            recycle(0)
        }
        val newerPosition = mPosition + NEWER_OFFSET
        if (newerPosition >= 0) {
            addView(mAdapter!!.getView(newerPosition, mRecycler.poll(), this))
        }
        if (mListener != null) {
            mListener!!.onTurned(mPosition)
        }
    }

    internal fun reset() {
        val count = childCount
        val start = startIndex(count)
        for (i in 0 until count) {
            resetChild(getChildAt(i), start + i, mWidth)
        }
    }

    internal fun turning(ratio: Float) {
        var ratio = ratio
        if (mPosition == 0 && ratio <= 0) {
            return
        }
        val count = childCount
        val start = startIndex(count)
        val offset = if (ratio > 0) 1 else -1

        ratio = Math.min(1f, Math.abs(ratio))

        for (i in 0 until count) {
            val n = start + i
            if (n == 0 && offset <= 0) {
                continue
            }
            if (n == MAX_CHILD_COUNT - 1 && offset >= 0) {
                continue
            }
            val child = getChildAt(i)
            val scale = scaleX(n, n + offset, ratio, mWidth)
            child.translationY = transY(n, n + offset, ratio)
            child.scaleX = scale
            child.scaleY = scale
            if (n == 0 && offset >= 0) {
                child.alpha = ratio * 0.7f
            } else if (n == 1 && offset <= 0) {
                child.alpha = 1 - ratio * 0.9f
            }
        }
    }

    internal inner class TurnAnimator : ValueAnimator(), ValueAnimator.AnimatorUpdateListener, Animator.AnimatorListener {
        var mIsTurn: Boolean = false
        var mRatio: Float = 0.toFloat()
        var mOffset: Int = 0

        init {
            addUpdateListener(this)
            addListener(this)
        }

        fun turn(isTurn: Boolean, ratio: Float) {
            mIsTurn = isTurn
            mRatio = ratio
            mOffset = if (ratio > 0) 1 else -1
            setFloatValues(ratio, if (isTurn) if (ratio > 0) 1f else -1f else 0f)
            start()
        }

        private fun turnBy(offset: Int) {
            if (offset == 0) {
                return
            }
            mIsTurn = true
            mRatio = 0f
            mOffset = offset
            setFloatValues(0f, if (mOffset > 0) 1f else -1f)
            start()
        }

        override fun onAnimationUpdate(animation: ValueAnimator) {
            turning(animation.animatedValue as Float)
        }

        override fun onAnimationStart(animation: Animator) {
            mIsAnimating = true
        }

        override fun onAnimationEnd(animation: Animator) {
            mIsAnimating = false
            if (mIsTurn) {
                if (mOffset > 0) {
                    older()
                    reset()
                    turnBy(mOffset - 1)
                } else {
                    newer()
                    reset()
                    turnBy(mOffset + 1)
                }
            }
        }

        override fun onAnimationCancel(animation: Animator) {}

        override fun onAnimationRepeat(animation: Animator) {}
    }

    internal inner class TurnOneAnimator(var child: View, var from: Int, var to: Int) : ValueAnimator(), ValueAnimator.AnimatorUpdateListener {
        init {
            setFloatValues(0f, 1f)
            interpolator = LinearInterpolator()
            addUpdateListener(this)
        }

        override fun onAnimationUpdate(animation: ValueAnimator) {
            val ratio = animation.animatedValue as Float
            updateChild(child, from, to, ratio)
        }
    }

    private fun dp2px(dip: Int): Int = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip.toFloat(), context.resources.displayMetrics).toInt()


    interface OnTurnListener {
        fun onTurned(position: Int)
    }
}