package cn.neocross.supor.capsulecooker.adapter

import android.content.Context
import android.widget.Toast
import cn.neocross.supor.capsulecooker.R
import cn.neorcoss.supor.capsulecooker.library.BaseRecyclerAdapter
import java.util.*

/**
 * 米量状态列表适配器
 * Created by shenhua on 2017-12-13-0013.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class RiceStatusAdapter(mContext: Context, datas: ArrayList<Rice>?) : BaseRecyclerAdapter<Rice>(mContext, datas) {

    override fun getItemViewId(viewType: Int): Int {
        return R.layout.item_rice_status
    }

    override fun bindData(holder: BaseRecyclerViewHolder, position: Int, item: Rice) {
        holder.setText(R.id.tvRiceName, item.name)
        holder.setText(R.id.tvRiceDesc, item.desc)
        holder.setText(R.id.tvRiceCount, item.size)
        holder.getView(R.id.tvShare).setOnClickListener {
            Toast.makeText(mContext, "分享", Toast.LENGTH_SHORT).show()
        }
    }
}