package cn.neocross.supor.capsulecooker.adapter

import android.content.Context
import cn.neocross.supor.capsulecooker.R
import cn.neorcoss.supor.capsulecooker.library.BaseRecyclerAdapter
import java.util.*

/**
 * Created by shenhua on 2017-12-20-0020.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class HistoryAdapter(mContext: Context, datas: ArrayList<String>?) : BaseRecyclerAdapter<String>(mContext, datas) {

    override fun getItemViewId(viewType: Int): Int {
        return R.layout.item_history
    }

    override fun bindData(holder: BaseRecyclerViewHolder, position: Int, item: String) {
    }

}