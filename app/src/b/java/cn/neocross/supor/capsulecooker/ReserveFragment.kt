package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cn.neocross.supor.capsulecooker.adapter.ReserveTimeAdapter
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.SwipeItemLayout
import cn.neorcoss.supor.capsulecooker.library.bean.Reserve
import cn.neorcoss.supor.capsulecooker.library.helper.WheelPickHelper
import kotlinx.android.synthetic.b.fragment_reserve.*
import kotlinx.android.synthetic.main.view_wheel_picker.*

/**
 * 预约
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ReserveFragment : BaseFragment() {

    companion object {

        fun get(): ReserveFragment {
            return ReserveFragment()
        }
    }

    private lateinit var wheelHelper: WheelPickHelper
    private var adapter: ReserveTimeAdapter? = null
    private var mTimesData: ArrayList<Reserve>? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_reserve, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViews()
        setRecycler()
        setDatas()
    }

    private fun setDatas() {
        mTimesData = getData()
        adapter!!.datas = mTimesData
    }

    private fun setViews() {
        (activity as ContentActivity).setTitle("预约时间")
        wheelHelper = WheelPickHelper()
        wheelHelper.setPicker(pickerData, pickerHour, pickerMinute)
        btnSaveTime.setOnClickListener {
            val ad = Reserve("0", wheelHelper.toString(), true)
            var isSame = false
            mTimesData!!.forEach {
                if (it == ad) {
                    isSame = true
                    return@forEach
                }
            }
            if (isSame) {
                Toast.makeText(context, "请勿重复添加", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            adapter!!.add(ad)
            recyclerView.scrollToPosition(0)
        }
        btnNext.setOnClickListener {
            var time = adapter!!.getOften()
            if (TextUtils.isEmpty(time)) {
                time = wheelHelper.toString()
            }
            nav(time)
        }
    }

    private fun setRecycler() {
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addOnItemTouchListener(SwipeItemLayout.OnSwipeItemTouchListener(context))
        adapter = ReserveTimeAdapter(context, mTimesData)
        recyclerView.adapter = adapter
        adapter!!.onSelectedListener = object : ReserveTimeAdapter.OnSelectedListener {
            override fun onSelect(position: Int) {
                wheelHelper.jumpTo(mTimesData!![position].time)
            }
        }
    }

    private fun getData(): ArrayList<Reserve>? {
        val datas = ArrayList<Reserve>()
        datas.add(Reserve("0", "今天,09:00", false))
        datas.add(Reserve("0", "今天,18:00", false))
        datas.add(Reserve("0", "明天,09:00", false))
        datas.add(Reserve("0", "明天,20:15", false))
        return datas
    }

    private fun nav(time: String) {
        val app = activity.application as App
        app.workType.type = Constant.DEFAULT
        app.workType.time = time
        (activity!! as ContentActivity)
                .setFragment(ChoiceFragment.get(), true)
    }
}