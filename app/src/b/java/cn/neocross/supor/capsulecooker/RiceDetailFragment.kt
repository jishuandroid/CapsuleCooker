package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * 米详情
 * Created by shenhua on 2017-12-19-0019.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class RiceDetailFragment : Fragment() {

    companion object {
        fun get(): RiceDetailFragment {
            return RiceDetailFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.frag_rice_detail, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        (activity as ContentActivity).setTitle("")
    }

    override fun toString(): String = RiceDetailFragment::class.java.name
}