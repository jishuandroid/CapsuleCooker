package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by shenhua on 2018/1/14.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class PowerSwitchFragment : Fragment() {

    companion object {

        fun get(): PowerSwitchFragment {
            return PowerSwitchFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_switch, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view!!.setOnTouchListener { v, event -> true }
    }
}