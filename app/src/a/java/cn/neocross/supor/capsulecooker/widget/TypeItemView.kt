package cn.neocross.supor.capsulecooker.widget

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import cn.neocross.supor.capsulecooker.R
import kotlinx.android.synthetic.a.item_cooker_type.view.*

/**
 * Created by shenhua on 2018-01-11-0011.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class TypeItemView @JvmOverloads constructor(context: Context, attr: AttributeSet? = null, defStyleAttr: Int = 0)
    : ConstraintLayout(context, attr, defStyleAttr) {

    private var rootLayoutParams: ViewGroup.LayoutParams? = null
    private lateinit var root: View
    var fastModel: Boolean = false
        set(value) {
            field = value
            root.findViewById<LinearLayout>(R.id.riceRight).visibility = if (field) View.VISIBLE else View.GONE
            root.findViewById<View>(R.id.line).visibility = if (field) View.VISIBLE else View.GONE
        }

    init {
        View.inflate(context, R.layout.item_cooker_type, this)
        setPadding(10, 10, 10, 10)
        isClickable = true
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        root = getChildAt(0)
        rootLayoutParams = root.layoutParams
        root.findViewById<LinearLayout>(R.id.riceRight).visibility = if (fastModel) View.VISIBLE else View.GONE
        root.findViewById<View>(R.id.line).visibility = if (fastModel) View.VISIBLE else View.GONE
    }

    fun setHeight(height: Int) {
        rootLayoutParams!!.height = dp2px(height)
    }

    fun setTitles(titles: Array<String>) {
        root.findViewById<TextView>(R.id.riceTag).text = titles[0]
        if (titles.size > 1) {
            root.findViewById<TextView>(R.id.riceType).text = titles[1]
            root.findViewById<TextView>(R.id.riceVolume).text = titles[2]
            fastModel = true
        }
    }

    fun setFastStartCallback(listener: OnClickListener) {
        riceRight.setOnClickListener(listener)
    }

    fun setOnClickCallback(listener: OnClickListener) {
        root.setOnClickListener(listener)
    }

    private fun dp2px(dip: Int): Int {
        val displayMetrics = context.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip.toFloat(), displayMetrics).toInt()
    }

}