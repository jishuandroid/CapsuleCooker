package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.a.view_red_package.*

/**
 * Created by shenhua on 2018-01-24-0024.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class RedPackageDialog : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.DialogStyle)
    }

    companion object {
        /**
         * 获取实例
         * @param msg 提示消息
         */
        fun create(msg: String): RedPackageDialog {
            val fragment = RedPackageDialog()
            val bundle = Bundle()
            bundle.putString("msg", msg)
            fragment.arguments = bundle
            return fragment
        }
    }

    var takeClickListener: OnTakeClickListener? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.view_red_package, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog_msg.text = arguments.getString("msg","No data")
        dialog_take.setOnClickListener {
            Toast.makeText(context, "已领取", Toast.LENGTH_SHORT).show()
            dismiss()
            takeClickListener?.onTake()
        }
        dialog_close.setOnClickListener { dismiss() }

        object : CountDownTimer(4000, 1000) {
            override fun onFinish() {
                dismiss()
            }

            override fun onTick(millisUntilFinished: Long) {
                try {
                    dialog_miss.text = "${millisUntilFinished / 1000}秒后自动收起"
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }.start()
    }

    interface OnTakeClickListener {

        fun onTake()
    }
}