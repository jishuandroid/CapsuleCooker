package cn.neocross.supor.capsulecooker.ext

import android.content.Context

/**
 * Created by shenhua on 2018/1/13.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
object SaveModel {

    private val SP = "last"

    fun putLastRice(context: Context, strs: Array<String>) {
        val sp = context.getSharedPreferences(SP, Context.MODE_PRIVATE)
        val ed = sp.edit()
        ed.putString("type0", strs[0])
        ed.putString("size0", strs[1])
        ed.putString("fen0", strs[2])
        ed.apply()
    }

    fun getLastRice(context: Context): Array<String> {
        val sp = context.getSharedPreferences(SP, Context.MODE_PRIVATE)
        return arrayOf(sp.getString("type0", "0"),
                sp.getString("size0", "0"),
                sp.getString("fen0", "0"))
    }

    fun putLastSlop(context: Context, strs: Array<String>) {
        val sp = context.getSharedPreferences(SP, Context.MODE_PRIVATE)
        val ed = sp.edit()
        ed.putString("type1", strs[0])
        ed.putString("size1", strs[1])
        ed.putString("fen1", strs[2])
        ed.apply()
    }

    fun getLastSlop(context: Context): Array<String> {
        val sp = context.getSharedPreferences(SP, Context.MODE_PRIVATE)
        return arrayOf(sp.getString("type1", "0"),
                sp.getString("size1", "0"),
                sp.getString("fen1", "0")
        )
    }

    fun clean(context: Context) {
        context.getSharedPreferences(SP, Context.MODE_PRIVATE).edit().clear().apply()
    }

}