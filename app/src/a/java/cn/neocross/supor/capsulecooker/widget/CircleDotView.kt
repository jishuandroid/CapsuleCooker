package cn.neocross.supor.capsulecooker.widget

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.animation.Animation
import android.view.animation.Transformation

/**
 * Created by shenhua on 2018-01-12-0012.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class CircleDotView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : View(context, attrs, defStyleAttr) {

    private val circlePaint: Paint = Paint()
    private val dotPaint: Paint
    private val textPaint: Paint
    private var circleRadius: Int = 50
    private val dotLong: Float = 20f
    private var circleRect: RectF = RectF()
    private val textRect: Rect = Rect()
    private val text: String = "1小时25分钟之后"
    private val text1: String = "开始烹饪"
    private val text2: String = "已保温"
    private val text3: String = "1小时45分钟"
    private var mSweepAngle: Float = 0f
    var progress: Int = 80
        set(value) {
            field = value
            invalidate()
        }
    var isDot: Boolean = true
        set(value) {
            field = value
            invalidate()
        }

    init {
        circlePaint.isAntiAlias = true
        circlePaint.style = Paint.Style.FILL
        circlePaint.color = Color.parseColor("#D9D9D9")

        dotPaint = Paint(circlePaint)
        dotPaint.color = Color.parseColor("#666666")
        dotPaint.strokeCap = Paint.Cap.ROUND
        dotPaint.strokeWidth = 10f

        textPaint = Paint(circlePaint)
        textPaint.color = Color.parseColor("#666666")
        textPaint.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 18f, resources.displayMetrics)
        textPaint.textAlign = Paint.Align.CENTER

        textPaint.getTextBounds(text, 0, text.length, textRect)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (isDot) {
            circlePaint.color = Color.BLUE
        } else {
            progress = progress * 360 / 100
            val anim = Anim()
            anim.duration = 2000
            startAnimation(anim)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val w = if (MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.EXACTLY) {
            MeasureSpec.getSize(measuredWidth)
        } else circleRadius * 4
        setMeasuredDimension(w, w)
        circleRadius = (measuredWidth - paddingLeft - paddingRight) / 2
        circleRect.set(paddingLeft * 1f, paddingTop * 1f, circleRadius * 2f, circleRadius * 2f)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (isDot) {
            dotPaint.strokeWidth = 15f
            for (i in 0 until 60) {
                canvas!!.drawPoint(measuredWidth / 2f, paddingTop + 5f, dotPaint)
                canvas.rotate(360f / 60, measuredWidth / 2f, measuredWidth / 2f)
            }
            canvas!!.save()
            canvas.drawText(text2, measuredWidth / 2f, measuredHeight / 2f, textPaint)
            canvas.drawText(text3, measuredWidth / 2f, measuredHeight / 2f + textRect.height(), textPaint)
            canvas.save()
            // progress
//            for (i in 0 until 60) {
//                canvas.drawPoint(measuredWidth / 2f, paddingTop + 5f, circlePaint)
//                canvas.rotate(360f / 60, measuredWidth / 2f, measuredWidth / 2f)
//            }
        } else {
            canvas!!.drawArc(circleRect, -90f, mSweepAngle, true, circlePaint)
            canvas.save()
            for (i in 0 until 60) {
                canvas.drawLine(measuredWidth / 2f, paddingTop + 5f, measuredWidth / 2f, paddingTop + if (i % 15 == 0) dotLong * 3 else dotLong, dotPaint)
                canvas.rotate(360f / 12, measuredWidth / 2f, measuredWidth / 2f)
            }
            canvas.drawText(text, measuredWidth / 2f, measuredHeight / 2f, textPaint)
            canvas.drawText(text1, measuredWidth / 2f, measuredHeight / 2f + textRect.height(), textPaint)
        }
    }

    inner class Anim : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
            super.applyTransformation(interpolatedTime, t)
            mSweepAngle = if (interpolatedTime < 1.0f)
                progress * interpolatedTime
            else progress * 1f
            postInvalidate()
        }
    }

}