package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.SwipeItemLayout
import cn.neorcoss.supor.capsulecooker.library.bean.Reserve
import cn.neorcoss.supor.capsulecooker.library.helper.WheelPickHelper
import kotlinx.android.synthetic.a.fragment_reserve.*
import kotlinx.android.synthetic.main.view_wheel_picker.*

/**
 * Created by shenhua on 2018-01-12-0012.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ReserveFragment : BaseFragment() {

    companion object {
        fun get(): ReserveFragment {
            return ReserveFragment()
        }
    }

    private lateinit var wheelHelper: WheelPickHelper
    private var adapter: ReserveTimeAdapter? = null
    private var mTimesData: ArrayList<Reserve>? = null
    private var last: Int = -1

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_reserve, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = (activity.application as App)
        val act = (activity as ContentActivity)

        act.setTitle("预约")
        tvChoice.text = "已选 ${app.workType.rice} ${app.workType.size}"
        setViews()
        setRecycler()
        setDatas()
        btnNext.setOnClickListener {
            var time = adapter!!.getOften()
            if (TextUtils.isEmpty(time)) {
                time = wheelHelper.toString()
            }
            app.workType.time = time
            app.workType.type =
                    if (app.workType.type == Constant.RICE) Constant.RESERVE_RICE else Constant.RESERVE_SLOP
            act.setFragment(ReserveDoneFragment.get(), false)
        }
    }

    private fun setDatas() {
        mTimesData = getData()
        adapter!!.datas = mTimesData
    }

    private fun setRecycler() {
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addOnItemTouchListener(SwipeItemLayout.OnSwipeItemTouchListener(context))
        adapter = ReserveTimeAdapter(context, mTimesData)
        recyclerView.adapter = adapter
        adapter!!.onSelectedListener = object : ReserveTimeAdapter.OnSelectedListener {
            override fun onSelect(position: Int) {
                wheelHelper.jumpTo(mTimesData!![position].time)
            }
        }
    }

    private fun getData(): ArrayList<Reserve>? {
        val datas = ArrayList<Reserve>()
        datas.add(Reserve("0", "今天,09:00", false))
        datas.add(Reserve("0", "今天,18:00", false))
        datas.add(Reserve("0", "明天,09:00", false))
        datas.add(Reserve("0", "明天,20:15", false))
        return datas
    }

    private fun setViews() {
        wheelHelper = WheelPickHelper()
        wheelHelper.setPicker(pickerData, pickerHour, pickerMinute)
        btnSaveTime.setOnClickListener {
            val ad = Reserve("0", wheelHelper.toString(), true)
            var isSame = false
            mTimesData!!.forEach {
                if (it == ad) {
                    isSame = true
                    return@forEach
                }
            }
            if (isSame) {
                Toast.makeText(context, "请勿重复添加", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            adapter!!.add(ad)
            recyclerView.scrollToPosition(0)
        }
    }
}