package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.WorkType
import cn.neorcoss.supor.capsulecooker.library.helper.WheelPickHelper
import kotlinx.android.synthetic.a.fragment_choice.*
import kotlinx.android.synthetic.a.view_wheel.*

/**
 * 米饭米量选择
 * Created by shenhua on 2018-01-11-0011.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ChoiceFragment : BaseFragment() {

    companion object {

        fun get(): ChoiceFragment {
            return ChoiceFragment()
        }
    }

    private val data = arrayOf("寒地黑土", "泰国香米", "长白珍珠", "江南贡米", "紫鹊贡米")

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_choice, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = (activity.application as App)
        val act = (activity as ContentActivity)
        val t = WorkType.getTypeString(app.workType.type)
        act.setTitle(t)
        btnReserve.text = "预约$t"
        btnStart.text = "开始$t"

        var rice = "长白珍珠"
        var size = "2人份"
        npRice.displayedValues = data
        npRice.minValue = 0
        npRice.maxValue = data.size - 1
        npRiceSize.minValue = 1
        npRiceSize.maxValue = 6
        npRice.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        npRiceSize.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        npRice.value = 2
        npRiceSize.value = 2
        npRice.setOnValueChangedListener { _, _, newVal -> rice = data[newVal] }
        npRiceSize.setOnValueChangedListener { _, _, newVal -> size = "${newVal}人份" }
        app.workType.rice = rice
        app.workType.size = size

        btnReserve.setOnClickListener {
            app.workType.rice = rice
            app.workType.size = size
            act.setFragment(ReserveFragment.get(), true)
        }
        btnStart.setOnClickListener {
            app.workType.rice = rice
            app.workType.size = size
            act.setFragment(ProcessFragment.get(), false)
        }
        WheelPickHelper.setDividerHeight(npRice, npRiceSize, height = 1)
    }
}