package cn.neocross.supor.capsulecooker.widget

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.view.animation.Animation
import android.view.animation.Transformation

/**
 * A方案水量进度条
 * Created by shenhua on 2018-01-11-0011.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class WaterProgress @JvmOverloads constructor(context: Context, attr: AttributeSet? = null, defStyleAttr: Int = 0)
    : View(context, attr, defStyleAttr) {

    private var backPaint: Paint = Paint()
    private var progressPaint: Paint
    private var linePaint: Paint
    private var warnTextPaint: Paint
    private var normalTextPaint: Paint
    private val radius = 15f
    private val rectf: RectF = RectF()
    private val progressRectf: RectF = RectF()
    private var mMaxRight: Float = 0f
    private val warnText: String = "最低水位"
    private val warnTextRect: Rect = Rect()
    private val normalTextRect: Rect = Rect()
    private val normalText = "12.31加水"
    private var mRight: Float = 0.0f;
    var progress: Int = 50
        set(value) {
            field = value
            invalidate()
        }
    var isExpand: Boolean = false
        set(value) {
            field = value
            invalidate()
        }

    init {
        backPaint.style = Paint.Style.FILL
        backPaint.isAntiAlias = true
        backPaint.color = Color.GRAY
        progressPaint = Paint(backPaint)
        progressPaint.color = Color.BLUE
        linePaint = Paint(backPaint)
        linePaint.color = Color.BLACK
        warnTextPaint = Paint(backPaint)
        warnTextPaint.color = Color.RED
        warnTextPaint.textAlign = Paint.Align.CENTER
        warnTextPaint.textSize = 20f
        normalTextPaint = Paint(warnTextPaint)
        normalTextPaint.color = Color.BLUE

        warnTextPaint.getTextBounds(warnText, 0, warnText.length, warnTextRect)
        normalTextPaint.getTextBounds(normalText, 0, normalText.length, normalTextRect)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        val anim = Anim()
        anim.duration = 1000
        startAnimation(anim)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val mWidth = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> widthSize - (paddingLeft + paddingRight)
            MeasureSpec.UNSPECIFIED -> Math.min(widthSize - (paddingLeft + paddingRight), widthSize)
            else -> Math.min(widthSize - (paddingLeft + paddingRight), widthSize)
        }
        val mHeight = when (heightMode) {
            MeasureSpec.EXACTLY -> heightSize - (paddingTop + paddingBottom)
            MeasureSpec.AT_MOST -> getMostHeight()
            MeasureSpec.UNSPECIFIED -> Math.min(heightSize - (paddingTop + paddingBottom), heightSize)
            else -> Math.min(heightSize - (paddingTop + paddingBottom), heightSize)
        }
        setMeasuredDimension(mWidth, mHeight)
        mRight = when (widthMode) {
            MeasureSpec.EXACTLY -> measuredWidth * 1f - paddingRight
            MeasureSpec.AT_MOST -> measuredWidth * 1f - paddingRight
            MeasureSpec.UNSPECIFIED -> measuredWidth * 1f
            else -> measuredWidth * 1f
        }
        rectf.set(paddingLeft * 1f, paddingTop * 1f, mRight, getBarBottom())
        progressRectf.set(paddingLeft * 1f, paddingTop * 1f, 0f, getBarBottom())
        mMaxRight = mRight * progress / 100f
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas!!.drawRoundRect(rectf, radius, radius, backPaint)
        canvas.drawRoundRect(progressRectf, radius, radius, progressPaint)
        if (isExpand) {
            canvas.drawLine(setMin(), getBarBottom(), setMin(), getBarBottom() + 50, linePaint)
            canvas.drawText(warnText, setMin(), getBarBottom() + 60 + warnTextRect.height() / 2, warnTextPaint)

            canvas.drawLine(mRight, getBarBottom(), mRight, getBarBottom() + 50, linePaint)
            canvas.drawText(normalText, mRight, getBarBottom() + 60 + normalTextRect.height() / 2, normalTextPaint)
        }
    }

    /**
     * 设置最低水位线
     */
    private fun setMin(): Float {
        return (mMaxRight * 0.25).toFloat()
    }

    private fun getMostHeight(): Int {
        return if (isExpand) paddingTop + paddingBottom + 2 * radius.toInt() + 100
        else paddingTop + paddingBottom + 2 * radius.toInt()
    }

    private fun getBarBottom(): Float = paddingTop + 2 * radius

    inner class Anim : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
            super.applyTransformation(interpolatedTime, t)
            progressRectf.right = if (interpolatedTime < 1.0f) {
                interpolatedTime * mMaxRight
            } else {
                mMaxRight
            }
            postInvalidate()
        }
    }

}