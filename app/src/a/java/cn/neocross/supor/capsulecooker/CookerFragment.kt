package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import cn.neocross.supor.capsulecooker.ext.SaveModel
import cn.neocross.supor.capsulecooker.ui.activity.MainActivity
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import kotlinx.android.synthetic.a.fragment_cooker.*

/**
 * Created by shenhua on 2017-12-15-0015.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class CookerFragment : BaseFragment() {

    companion object {

        fun get(): CookerFragment {
            return CookerFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return if ((activity.application as App).isWorking()) {
            inflater!!.inflate(R.layout.fragment_cooker_work, container, false)
        } else {
            inflater!!.inflate(R.layout.fragment_cooker, container, false)
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if ((activity.application as App).isWorking()) {
            setChildView(view!!)
            radialIndicator.processes = intArrayOf(3, 5, 2, 3)
        } else {
            setupViews()
            setListener()
        }
    }

    /**
     * 工作状态下的视图
     */
    private fun setChildView(view: View) {
        val tvRiceType = view.findViewById<TextView>(R.id.tvRiceType)
        val tvRice = view.findViewById<TextView>(R.id.tvRice)
        val tvRiceTime = view.findViewById<TextView>(R.id.tvRiceTime)
        val ivRiceType = view.findViewById<ImageView>(R.id.ivRiceType)
        val app = activity.application as App

        when (app.workType.type) {
            Constant.RICE -> {
                tvRiceType.text = "煮饭"
            }
            Constant.SLOP -> {
                tvRiceType.text = "煮粥"
            }
            Constant.INSULATION -> {
                tvRiceType.text = "已保温45分钟"
                ivRiceType.setImageResource(R.drawable.ic_insulation)
                tvRice.visibility = View.GONE
                tvRiceTime.visibility = View.GONE
            }
            Constant.RESERVE_RICE, Constant.RESERVE_SLOP -> {
                tvRiceType.text = "预约中"
                ivRiceType.setImageResource(R.drawable.ic_reserve)
            }
        }
        tvRice.text = app.workType.rice + app.workType.size

        view.findViewById<CardView>(R.id.layoutCard).setOnClickListener {
            when (app.workType.type) {
                Constant.RICE -> {
                    nav(StartFragment.get())
                }
                Constant.SLOP -> {
                    nav(StartFragment.get())
                }
                Constant.INSULATION -> {
                    nav(InsulationFragment.get())
                }
                Constant.RESERVE_RICE, Constant.RESERVE_SLOP -> {
                    nav(ReserveDoneFragment.get())
                }
            }
        }
    }

    /**
     * 非工作状态下的视图
     */
    private fun setupViews() {
        progress.progress = 60
        typeItemInsulation.height = 100
        if (SaveModel.getLastRice(context)[0] != "0") {
            typeItemRice.setTitles(SaveModel.getLastRice(context))
        }
        if (SaveModel.getLastSlop(context)[0] != "0") {
            typeItemSlop.setTitles(SaveModel.getLastSlop(context))
        } else {
            typeItemSlop.setTitles(arrayOf("煮粥"))
        }
        typeItemInsulation.setTitles(arrayOf("保温"))
    }

    /**
     * 非工作状态下item点击或快捷煮饭
     */
    private fun setListener() {
        radialIndicator.setOnClickListener { nav(WaterStatusFragment.get()) }
        progress.setOnClickListener { nav(WaterStatusFragment.get()) }
        // item
        typeItemRice.setOnClickCallback(View.OnClickListener {
            (activity.application as App).workType.type = Constant.RICE
            nav(ChoiceFragment.get())
        })
        typeItemSlop.setOnClickCallback(View.OnClickListener {
            (activity.application as App).workType.type = Constant.SLOP
            nav(ChoiceFragment.get())
        })
        typeItemInsulation.setOnClickCallback(View.OnClickListener { nav(InsulationFragment.get()) })
        // fast start
        if (typeItemRice.fastModel) {
            typeItemRice.setFastStartCallback(View.OnClickListener {
                val app = (activity.application as App)
                app.workType.type = Constant.RICE
                val str = SaveModel.getLastRice(context)
                app.workType.rice = str[1]
                app.workType.size = str[2]
                nav(ProcessFragment.get())
            })
        }
        if (typeItemSlop.fastModel) {
            typeItemSlop.setFastStartCallback(View.OnClickListener {
                val app = (activity.application as App)
                app.workType.type = Constant.SLOP
                val str = SaveModel.getLastSlop(context)
                app.workType.rice = str[1]
                app.workType.size = str[2]
                nav(ProcessFragment.get())
            })
        }
    }

    private fun nav(fragment: Fragment) {
        (activity as MainActivity).navTo(fragment)
    }

}