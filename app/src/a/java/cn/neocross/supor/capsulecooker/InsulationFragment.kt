package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cn.neocross.libs.neosocket.bean.InstantMessage
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.MessageDialogFragment
import kotlinx.android.synthetic.a.fragment_insulation.*

/**
 * 保温
 * Created by shenhua on 2017-12-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class InsulationFragment : BaseFragment() {

    companion object {
        fun get(): InsulationFragment {
            return InsulationFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_insulation, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = (activity.application as App)
        val act = (activity as ContentActivity)

        act.setTitle("保温中")
        tvTimer.base = SystemClock.elapsedRealtime()
        val hour = ((SystemClock.elapsedRealtime() - tvTimer.base) / 1000 / 60).toInt()
        tvTimer.format = "0" + hour.toString() + ":%s"
        tvTimer.start()
        app.workType.type = Constant.INSULATION
        if (!app.isPassive()) {
            app.mClient?.send(InstantMessage(StatusType.TYPE_MSG, app.workType))
        }
        btnCancel.setOnClickListener {
            val dialog = MessageDialogFragment.create("是否确定取消保温？", "是", "再想想")
            dialog.show(childFragmentManager, "")
            dialog.listener = object : MessageDialogFragment.OnClickListener {
                override fun onPositiveListener() {
                    app.mClient?.send(InstantMessage(StatusType.TYPE_MSG, Constant.INSULATION_TEXT_CANCEL))
                    app.resetWorkType()
                    Toast.makeText(context, "已取消保温", Toast.LENGTH_SHORT).show()
                    activity.finish()
                }
            }
        }
    }
}