package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import cn.neorcoss.supor.capsulecooker.library.ViewUtils
import kotlinx.android.synthetic.main.activity_content.*

/**
 * 内容界面
 * Created by shenhua on 2017/12/16.
 * Email shenhuanet@126.com
 */
class ContentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ViewUtils.full(window)
        setContentView(R.layout.activity_content)
        setupViews()
        setupFragment()
    }

    /**
     * 初始化界面
     */
    private fun setupViews() {
        setSupportActionBar(toolbar)
        supportActionBar!!.title = null
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    /**
     * 自主填充Fragment
     */
    private fun setupFragment() {
        val fragment = intent.getStringExtra("fragment")
        when (fragment) {
            WaterStatusFragment.get().toString() -> setFragment(WaterStatusFragment.get())
            InsulationFragment.get().toString() -> setFragment(InsulationFragment.get())
            ChoiceFragment.get().toString() -> setFragment(ChoiceFragment.get())
            ProcessFragment.get().toString() -> setFragment(ProcessFragment.get())
            StartFragment.get().toString() -> setFragment(StartFragment.get())
            ReserveDoneFragment.get().toString() -> setFragment(ReserveDoneFragment.get())
        }
    }

    private fun setFragment(fragment: Fragment) {
        setFragment(fragment, false)
    }

    /**
     * 本activity设置fragment
     */
    fun setFragment(fragment: Fragment, needBackStack: Boolean) {
        if (isFinishing) {
            return
        }
        if (needBackStack) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .addToBackStack(fragment.toString())
                    .commit()
        } else {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit()
        }
        invalidateOptionsMenu()
    }

    /**
     * 更新标题
     */
    fun setTitle(title: String) {
        toolbar_title.text = title
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        if (fragment is StartFragment || fragment is ReserveDoneFragment
                || fragment is InsulationFragment) {
            menuInflater.inflate(R.menu.share, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }
        if (item.itemId == R.id.id_menu_share) {
            Toast.makeText(this, "分享", Toast.LENGTH_SHORT).show()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        if (fragment is ProcessFragment
                || fragment is StartFragment
                || fragment is ReserveDoneFragment
                || fragment is InsulationFragment) {
            finish()
        } else if (fragment is ChoiceFragment) {
            (application as App).resetWorkType()
            super.onBackPressed()
        } else {
            super.onBackPressed()
        }
    }

}