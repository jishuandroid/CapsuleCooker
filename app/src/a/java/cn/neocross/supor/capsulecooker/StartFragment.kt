package cn.neocross.supor.capsulecooker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cn.neocross.libs.neosocket.bean.InstantMessage
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.MessageDialogFragment
import cn.neorcoss.supor.capsulecooker.library.WorkType
import kotlinx.android.synthetic.a.fragment_start.*

/**
 * Created by shenhua on 2018-01-12-0012.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class StartFragment : BaseFragment() {

    companion object {

        fun get(): StartFragment {
            return StartFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_start, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = (activity.application as App)
        val act = (activity as ContentActivity)
        val t = WorkType.getTypeString(app.workType.type)
        act.setTitle("${t}中")
        tvRiceSize.text = "${app.workType.rice} ${app.workType.size}"

        btnCancel.setOnClickListener {
            val dialog = MessageDialogFragment.create("米已经加了\n确定${t}吗？", "是", "再想想")
            dialog.show(childFragmentManager, "")
            dialog.listener = object : MessageDialogFragment.OnClickListener {
                override fun onPositiveListener() {
                    app.mClient?.send(InstantMessage(StatusType.TYPE_MSG,
                            if (app.workType.type == Constant.RICE) Constant.RICE_TEXT_CANCEL
                            else Constant.SLOP_TEXT_CANCEL))
                    app.resetWorkType()
                    Toast.makeText(context, "已取消", Toast.LENGTH_SHORT).show()
                    activity.finish()
                }
            }
        }
    }
}