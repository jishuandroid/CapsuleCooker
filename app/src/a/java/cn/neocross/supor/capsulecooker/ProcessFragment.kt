package cn.neocross.supor.capsulecooker

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.neocross.libs.neosocket.bean.InstantMessage
import cn.neocross.libs.neosocket.callback.StatusType
import cn.neocross.supor.capsulecooker.ext.SaveModel
import cn.neorcoss.supor.capsulecooker.library.BaseFragment
import cn.neorcoss.supor.capsulecooker.library.Constant
import cn.neorcoss.supor.capsulecooker.library.WorkType
import kotlinx.android.synthetic.a.fragment_process.*
import java.util.*

/**
 * 煮饭进程
 * Created by shenhua on 2018-01-12-0012.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class ProcessFragment : BaseFragment() {

    companion object {
        fun get(): ProcessFragment {
            return ProcessFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_process, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = (activity.application as App)
        val act = (activity as ContentActivity)
        val t = WorkType.getTypeString(app.workType.type)
        act.setTitle("${t}中")
        tvTag1.text = "${app.workType.rice} ${app.workType.size}"
        if (app.workType.type == Constant.RICE) {
            SaveModel.putLastRice(context, arrayOf(t, app.workType.rice!!, app.workType.size!!))
        } else {
            SaveModel.putLastSlop(context, arrayOf(t, app.workType.rice!!, app.workType.size!!))
        }
        if (!app.isPassive()) {
            app.mClient?.send(InstantMessage(StatusType.TYPE_MSG, app.workType))
        }
        val r = Random().nextInt(3)
        if (r == 1) {
            Thread({
                val a = (2000 * Math.random() + 500).toLong()
                Thread.sleep(a)
                mHandler.obtainMessage(100).sendToTarget()
            }).start()
        }
        // total time = 7700
        Thread({
            try {
                for (i in 0..50) {
                    Thread.sleep(50)
                    mHandler.obtainMessage(2, i).sendToTarget()
                }
                mHandler.obtainMessage(1).sendToTarget()
                for (i in 51..100) {
                    Thread.sleep(100)
                    mHandler.obtainMessage(2, i).sendToTarget()
                }
                Thread.sleep(200)
                mHandler.obtainMessage(3).sendToTarget()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }).start()
    }

    private val mHandler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            if (activity == null || activity.isFinishing || activity.isDestroyed) {
                return
            }
            try {
                if (msg!!.what == 1) {
                    tvTag2.text = "正在注水"
                    ivAnim.setImageResource(R.drawable.img_zhushui)
                }
                if (msg.what == 2) {
                    stepView.progress = msg.obj as Int
                }
                if (msg.what == 3) {
                    (activity as ContentActivity)
                            .setFragment(StartFragment.get(), false)
                }
                if (msg.what == 100) {
                    RedPackageDialog.create("打开米胶囊时发现\n优惠券一枚！").show(childFragmentManager, "dialog")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}