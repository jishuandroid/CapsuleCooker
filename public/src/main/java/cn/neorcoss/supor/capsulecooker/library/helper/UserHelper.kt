package cn.neorcoss.supor.capsulecooker.library.helper

import android.content.Context
import android.text.TextUtils

/**
 * Created by shenhua on 2017-12-15-0015.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
object UserHelper {

    fun isBinding(context: Context): Boolean = !TextUtils.isEmpty(InputLocalHelper.readIp(context))


    fun isLogin(context: Context): Boolean = true
}