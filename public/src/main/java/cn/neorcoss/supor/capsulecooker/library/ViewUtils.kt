package cn.neorcoss.supor.capsulecooker.library

import android.os.Build
import android.view.View
import android.view.Window

/**
 * Created by shenhua on 2018-01-18-0018.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
object ViewUtils {

    fun full(window: Window?) {
        if (window == null) {
            return
        }
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE)
        window.decorView.setOnSystemUiVisibilityChangeListener {
            var uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    //布局位于状态栏下方
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    //全屏
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    //隐藏导航栏
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            uiOptions = if (Build.VERSION.SDK_INT >= 19) {
                uiOptions or 0x00001000
            } else {
                uiOptions or View.SYSTEM_UI_FLAG_LOW_PROFILE
            }
            window.decorView.systemUiVisibility = uiOptions
        }
    }
}