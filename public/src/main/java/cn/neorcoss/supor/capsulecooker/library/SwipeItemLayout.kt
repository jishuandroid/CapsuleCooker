package cn.neorcoss.supor.capsulecooker.library

import android.content.Context
import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.util.Log
import android.view.*
import android.view.animation.Interpolator
import android.widget.Scroller
import java.lang.IllegalStateException

/**
 * @author shenhua
 */
class SwipeItemLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : ViewGroup(context, attrs) {

    private var touchMode: Mode? = null
    private var mainItemView: View? = null
    private var mInLayout = false
    var scrollOffset: Int = 0
        private set
    private var maxScrollOffset: Int = 0
    private val scrollRunnable: ScrollRunnable
    private var isOpen: Boolean = false

    enum class Mode {
        RESET, DRAG, FLING, CLICK
    }

    init {
        touchMode = Mode.RESET
        scrollOffset = 0
        scrollRunnable = ScrollRunnable(context)
    }

    fun open() {
        if (scrollOffset != -maxScrollOffset) {
            if (touchMode == Mode.FLING) {
                scrollRunnable.abort()
            }
            scrollRunnable.startScroll(scrollOffset, -maxScrollOffset)
        }
    }

    fun close() {
        if (scrollOffset != 0) {
            if (touchMode == Mode.FLING) {
                scrollRunnable.abort()
            }
            scrollRunnable.startScroll(scrollOffset, 0)
        }
    }

    internal fun fling(xVel: Int) {
        scrollRunnable.startFling(scrollOffset, xVel)
    }

    internal fun revise() {
        if (scrollOffset < -maxScrollOffset / 2) {
            open()
        } else {
            close()
        }
    }

    private fun ensureChildren() {
        val childCount = childCount

        for (i in 0 until childCount) {
            val childView = getChildAt(i)
            val tempLp = childView.layoutParams

            if (tempLp == null || tempLp !is LayoutParams) {
                throw IllegalStateException("缺少layout参数")
            }

            if (tempLp.itemType == 0x01) {
                mainItemView = childView
            }
        }

        if (mainItemView == null) {
            throw IllegalStateException("main item不能为空")
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        ensureChildren()
        var lp = mainItemView!!.layoutParams as LayoutParams
        measureChildWithMargins(
                mainItemView,
                widthMeasureSpec, paddingLeft + paddingRight,
                heightMeasureSpec, paddingTop + paddingBottom)
        setMeasuredDimension(
                mainItemView!!.measuredWidth + paddingLeft + paddingRight + lp.leftMargin + lp.rightMargin, mainItemView!!.measuredHeight + paddingTop + paddingBottom + lp.topMargin + lp.bottomMargin)
        val menuWidthSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
        val menuHeightSpec = MeasureSpec.makeMeasureSpec(mainItemView!!.measuredHeight, MeasureSpec.EXACTLY)
        for (i in 0 until childCount) {
            val menuView = getChildAt(i)
            lp = menuView.layoutParams as LayoutParams
            if (lp.itemType == 0x01) {
                continue
            }
            measureChildWithMargins(menuView, menuWidthSpec, 0, menuHeightSpec, 0)
        }
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        mInLayout = true
        ensureChildren()
        val pl = paddingLeft
        val pt = paddingTop
        val pr = paddingRight
        val pb = paddingBottom
        var lp: LayoutParams
        //layout main
        lp = mainItemView!!.layoutParams as LayoutParams
        mainItemView!!.layout(
                pl + lp.leftMargin,
                pt + lp.topMargin,
                width - pr - lp.rightMargin,
                height - pb - lp.bottomMargin)
        //layout menu
        var totalLength = 0
        var menuLeft = mainItemView!!.right + lp.rightMargin
        for (i in 0 until childCount) {
            val menuView = getChildAt(i)
            lp = menuView.layoutParams as LayoutParams
            if (lp.itemType == 0x01) {
                continue
            }
            val tempLeft = menuLeft + lp.leftMargin
            val tempTop = pt + lp.topMargin
            menuView.layout(
                    tempLeft,
                    tempTop,
                    tempLeft + menuView.measuredWidth + lp.rightMargin,
                    tempTop + menuView.measuredHeight + lp.bottomMargin)

            menuLeft = menuView.right + lp.rightMargin
            totalLength += lp.leftMargin + lp.rightMargin + menuView.measuredWidth
        }

        maxScrollOffset = totalLength
        scrollOffset = if (scrollOffset < -maxScrollOffset / 2) -maxScrollOffset else 0
        offsetChildrenLeftAndRight(scrollOffset)
        mInLayout = false
    }

    internal fun offsetChildrenLeftAndRight(delta: Int) {
        for (i in 0 until childCount) {
            val childView = getChildAt(i)
            ViewCompat.offsetLeftAndRight(childView, delta)
        }
    }

    override fun requestLayout() {
        if (!mInLayout) {
            super.requestLayout()
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        removeCallbacks(scrollRunnable)
        touchMode = Mode.RESET
        scrollOffset = 0
    }

    // 展开的情况下，拦截down event，避免触发点击main事件
    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
//        if (!isOpen) {
//            return super.onInterceptTouchEvent(ev)
//        }
        val action = ev.actionMasked
        when (action) {
            MotionEvent.ACTION_DOWN -> {
                val x = ev.x.toInt()
                val y = ev.y.toInt()
                val pointView = findTopChildUnder(this, x, y)
                if (pointView != null && pointView == mainItemView && scrollOffset != 0) {
                    return true
                }
            }

            MotionEvent.ACTION_MOVE, MotionEvent.ACTION_CANCEL -> {
            }

            MotionEvent.ACTION_UP -> run {
                val x = ev.x.toInt()
                val y = ev.y.toInt()
                val pointView = findTopChildUnder(this, x, y)
                if (pointView != null && pointView === mainItemView && touchMode == Mode.CLICK && scrollOffset != 0) {
                    return true
                }
            }
        }
        return false
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
//        if (!isOpen) {
//            return super.onTouchEvent(ev)
//        }
        val action = ev.actionMasked
        when (action) {
            MotionEvent.ACTION_DOWN -> {
                val x = ev.x.toInt()
                val y = ev.y.toInt()
                val pointView = findTopChildUnder(this, x, y)
                if (pointView != null && pointView === mainItemView && scrollOffset != 0) {
                    return true
                }
            }

            MotionEvent.ACTION_MOVE, MotionEvent.ACTION_CANCEL -> {
            }

            MotionEvent.ACTION_UP -> run {
                val x = ev.x.toInt()
                val y = ev.y.toInt()
                val pointView = findTopChildUnder(this, x, y)
                if (pointView != null && pointView === mainItemView && touchMode == Mode.CLICK && scrollOffset != 0) {
                    close()
                    return true
                }
            }
        }
        return false
    }

    internal fun setTouchMode(mode: Mode) {
        if (mode == touchMode) {
            return
        }

        if (touchMode == Mode.FLING) {
            removeCallbacks(scrollRunnable)
        }

        touchMode = mode
    }

    fun getTouchMode(): Mode? {
        return touchMode
    }

    internal fun trackMotionScroll(deltaX: Int): Boolean {
        if (deltaX == 0) {
            return true
        }

        var over = false
        var newLeft = scrollOffset + deltaX
        if (deltaX > 0 && newLeft > 0 || deltaX < 0 && newLeft < -maxScrollOffset) {
            over = true
            newLeft = Math.min(newLeft, 0)
            newLeft = Math.max(newLeft, -maxScrollOffset)
        }

        offsetChildrenLeftAndRight(newLeft - scrollOffset)
        scrollOffset = newLeft
        return over
    }

    private inner class ScrollRunnable internal constructor(context: Context) : Runnable {
        private val scroller: Scroller
        private var abort: Boolean = false
        private val minVelocity: Int

        init {
            scroller = Scroller(context, sInterpolator)
            abort = false

            val configuration = ViewConfiguration.get(context)
            minVelocity = configuration.scaledMinimumFlingVelocity
        }

        internal fun startScroll(startX: Int, endX: Int) {
            if (startX != endX) {
                Log.e("scroll - startX - endX", "" + startX + " " + endX)
                setTouchMode(Mode.FLING)
                abort = false

                scroller.startScroll(startX, 0, endX - startX, 0, 400)
                ViewCompat.postOnAnimation(this@SwipeItemLayout, this)
            }
        }

        internal fun startFling(startX: Int, xVel: Int) {
            Log.e("fling - startX", "" + startX)

            if (xVel > minVelocity && startX != 0) {
                startScroll(startX, 0)
                return
            }

            if (xVel < -minVelocity && startX != -maxScrollOffset) {
                startScroll(startX, -maxScrollOffset)
                return
            }

            startScroll(startX, if (startX > -maxScrollOffset / 2) 0 else -maxScrollOffset)
        }

        internal fun abort() {
            if (!abort) {
                abort = true
                if (!scroller.isFinished) {
                    scroller.abortAnimation()
                    removeCallbacks(this)
                }
            }
        }

        override fun run() {
            Log.e("abort", java.lang.Boolean.toString(abort))
            if (!abort) {
                val more = scroller.computeScrollOffset()
                val curX = scroller.currX
                Log.e("curX", "" + curX)

                var atEdge = false
                if (curX != scrollOffset) {
                    atEdge = trackMotionScroll(curX - scrollOffset)
                }

                if (more && !atEdge) {
                    ViewCompat.postOnAnimation(this@SwipeItemLayout, this)
                    return
                } else {
                    removeCallbacks(this)
                    if (!scroller.isFinished) {
                        scroller.abortAnimation()
                    }
                    setTouchMode(Mode.RESET)
                }
            }
        }
    }

    override fun generateDefaultLayoutParams(): LayoutParams {
        return LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
    }

    override fun generateLayoutParams(p: ViewGroup.LayoutParams): LayoutParams {
        return p as? LayoutParams ?: LayoutParams(p)
    }

    override fun checkLayoutParams(p: ViewGroup.LayoutParams): Boolean {
        return p is LayoutParams && super.checkLayoutParams(p)
    }

    override fun generateLayoutParams(attrs: AttributeSet): LayoutParams {
        return LayoutParams(context, attrs)
    }

    class LayoutParams : MarginLayoutParams {
        var itemType = -1

        constructor(c: Context, attrs: AttributeSet) : super(c, attrs) {

            val a = c.obtainStyledAttributes(attrs, R.styleable.SwipeItemLayout_Layout)
            itemType = a.getInt(R.styleable.SwipeItemLayout_Layout_layout_itemType, -1)
            a.recycle()
        }

        constructor(source: ViewGroup.LayoutParams) : super(source) {}

        constructor(source: LayoutParams) : super(source) {
            itemType = source.itemType
        }

        constructor(width: Int, height: Int) : super(width, height) {}
    }

    class OnSwipeItemTouchListener(context: Context) : RecyclerView.OnItemTouchListener {
        private var captureItem: SwipeItemLayout? = null
        private var lastMotionX: Float = 0.toFloat()
        private var lastMotionY: Float = 0.toFloat()
        private var velocityTracker: VelocityTracker? = null

        private var activePointerId: Int = 0

        private val touchSlop: Int
        private val maximumVelocity: Int

        private var parentHandled: Boolean = false
        private var probingParentProcess: Boolean = false

        private var ignoreActions = false

        init {
            val configuration = ViewConfiguration.get(context)
            touchSlop = configuration.scaledTouchSlop
            maximumVelocity = configuration.scaledMaximumFlingVelocity
            activePointerId = -1
            parentHandled = false
            probingParentProcess = false
        }

        override fun onInterceptTouchEvent(rv: RecyclerView, ev: MotionEvent): Boolean {
            if (probingParentProcess) {
                return false
            }

            var intercept = false
            val action = ev.actionMasked

            if (action != MotionEvent.ACTION_DOWN && ignoreActions) {
                return true
            }

            if (action != MotionEvent.ACTION_DOWN && (captureItem == null || parentHandled)) {
                return false
            }

            if (velocityTracker == null) {
                velocityTracker = VelocityTracker.obtain()
            }
            velocityTracker!!.addMovement(ev)

            when (action) {
                MotionEvent.ACTION_DOWN -> {
                    ignoreActions = false
                    parentHandled = false
                    activePointerId = ev.getPointerId(0)
                    val x = ev.x
                    val y = ev.y
                    lastMotionX = x
                    lastMotionY = y

                    var pointOther = false
                    var pointItem: SwipeItemLayout? = null
                    //首先知道ev针对的是哪个item
                    val pointView = findTopChildUnder(rv, x.toInt(), y.toInt())
                    if (pointView == null || pointView !is SwipeItemLayout) {
                        //可能是head view或bottom view
                        pointOther = true
                    } else {
                        pointItem = pointView
                    }

                    //此时的pointOther=true，意味着点击的view为空或者点击的不是item
                    //还没有把点击的是item但是不是capture item给过滤出来
                    if (!pointOther && (captureItem == null || captureItem !== pointItem)) {
                        pointOther = true
                    }

                    //点击的是capture item
                    if (!pointOther) {
                        val mode = captureItem!!.getTouchMode()

                        //如果它在fling，就转为drag
                        //需要拦截，并且requestDisallowInterceptTouchEvent
                        var disallowIntercept = false
                        if (mode == Mode.FLING) {
                            captureItem!!.setTouchMode(Mode.DRAG)
                            disallowIntercept = true
                            intercept = true
                        } else {//如果是expand的，就不允许parent拦截
                            captureItem!!.setTouchMode(Mode.CLICK)
                            if (captureItem!!.scrollOffset != 0) {
                                disallowIntercept = true
                            }
                        }

                        if (disallowIntercept) {
                            val parent = rv.parent
                            parent?.requestDisallowInterceptTouchEvent(true)
                        }
                    } else {//capture item为null或者与point item不一样
                        //直接将其close掉
                        if (captureItem != null && captureItem!!.scrollOffset != 0) {
                            captureItem!!.close()
                            ignoreActions = true
                            return true
                        }

                        captureItem = null

                        if (pointItem != null) {
                            captureItem = pointItem
                            captureItem!!.setTouchMode(Mode.CLICK)
                        }
                    }

                    //如果parent处于fling状态，此时，parent就会转为drag。应该将后续move都交给parent处理
                    probingParentProcess = true
                    parentHandled = rv.onInterceptTouchEvent(ev)
                    probingParentProcess = false
                    if (parentHandled) {
                        intercept = false
                        //在down时，就被认定为parent的drag，所以，直接交给parent处理即可
                        if (captureItem != null && captureItem!!.scrollOffset != 0) {
                            captureItem!!.close()
                        }
                    }
                }

                MotionEvent.ACTION_POINTER_DOWN -> {
                    val actionIndex = ev.actionIndex
                    activePointerId = ev.getPointerId(actionIndex)

                    lastMotionX = ev.getX(actionIndex)
                    lastMotionY = ev.getY(actionIndex)
                }

                MotionEvent.ACTION_POINTER_UP -> {
                    val actionIndex = ev.actionIndex
                    val pointerId = ev.getPointerId(actionIndex)
                    if (pointerId == activePointerId) {
                        val newIndex = if (actionIndex == 0) 1 else 0
                        activePointerId = ev.getPointerId(newIndex)

                        lastMotionX = ev.getX(newIndex)
                        lastMotionY = ev.getY(newIndex)
                    }
                }

            //down时，已经将capture item定下来了。所以，后面可以安心考虑event处理
                MotionEvent.ACTION_MOVE -> {
                    val activePointerIndex = ev.findPointerIndex(activePointerId)

                    val x = (ev.getX(activePointerIndex) + .5f).toInt()
                    val y = (ev.getY(activePointerIndex).toInt() + .5f).toInt()

                    var deltaX = (x - lastMotionX).toInt()
                    val deltaY = (y - lastMotionY).toInt()
                    val xDiff = Math.abs(deltaX)
                    val yDiff = Math.abs(deltaY)

                    var mode = captureItem!!.getTouchMode()

                    if (mode == Mode.CLICK) {
                        //如果capture item是open的，下拉有两种处理方式：
                        //  1、下拉后，直接close item
                        //  2、只要是open的，就拦截所有它的消息，这样如果点击open的，就只能滑动该capture item
                        if (xDiff > touchSlop && xDiff > yDiff) {
                            captureItem!!.setTouchMode(Mode.DRAG)
                            val parent = rv.parent
                            parent.requestDisallowInterceptTouchEvent(true)

                            deltaX = if (deltaX > 0) deltaX - touchSlop else deltaX + touchSlop
                        } else
                        /* if(yDiff>touchSlop)*/ {
                            probingParentProcess = true
                            parentHandled = rv.onInterceptTouchEvent(ev)
                            probingParentProcess = false

                            if (parentHandled && captureItem!!.scrollOffset != 0) {
                                captureItem!!.close()
                            }
                        }
                    }

                    mode = captureItem!!.getTouchMode()
                    if (mode == Mode.DRAG) {
                        intercept = true
                        lastMotionX = x.toFloat()
                        lastMotionY = y.toFloat()

                        //对capture item进行拖拽
                        captureItem!!.trackMotionScroll(deltaX)
                    }
                }

                MotionEvent.ACTION_UP -> {
                    val mode = captureItem!!.getTouchMode()
                    if (mode == Mode.DRAG) {
                        val velocityTracker = this.velocityTracker
                        velocityTracker!!.computeCurrentVelocity(1000, maximumVelocity.toFloat())
                        val xVel = velocityTracker.getXVelocity(activePointerId).toInt()
                        captureItem!!.fling(xVel)

                        intercept = true
                    }
                    cancel()
                }

                MotionEvent.ACTION_CANCEL -> {
                    captureItem!!.revise()
                    cancel()
                }
                else -> {
                }
            }

            return intercept
        }

        override fun onTouchEvent(rv: RecyclerView, ev: MotionEvent) {
            if (ignoreActions) {
                return
            }

            val action = ev.actionMasked
            val actionIndex = ev.actionIndex

            if (velocityTracker == null) {
                velocityTracker = VelocityTracker.obtain()
            }
            velocityTracker!!.addMovement(ev)

            when (action) {
                MotionEvent.ACTION_POINTER_DOWN -> {
                    activePointerId = ev.getPointerId(actionIndex)

                    lastMotionX = ev.getX(actionIndex)
                    lastMotionY = ev.getY(actionIndex)
                }

                MotionEvent.ACTION_POINTER_UP -> {
                    val pointerId = ev.getPointerId(actionIndex)
                    if (pointerId == activePointerId) {
                        val newIndex = if (actionIndex == 0) 1 else 0
                        activePointerId = ev.getPointerId(newIndex)

                        lastMotionX = ev.getX(newIndex)
                        lastMotionY = ev.getY(newIndex)
                    }
                }

            //down时，已经将capture item定下来了。所以，后面可以安心考虑event处理
                MotionEvent.ACTION_MOVE -> {
                    val activePointerIndex = ev.findPointerIndex(activePointerId)

                    val x = ev.getX(activePointerIndex)
                    val y = ev.getY(activePointerIndex).toInt().toFloat()

                    val deltaX = (x - lastMotionX).toInt()

                    if (captureItem != null && captureItem!!.getTouchMode() == Mode.DRAG) {
                        lastMotionX = x
                        lastMotionY = y

                        //对capture item进行拖拽
                        captureItem!!.trackMotionScroll(deltaX)
                    }
                }

                MotionEvent.ACTION_UP -> {
                    if (captureItem != null) {
                        val mode = captureItem!!.getTouchMode()
                        if (mode == Mode.DRAG) {
                            val velocityTracker = this.velocityTracker
                            velocityTracker!!.computeCurrentVelocity(1000, maximumVelocity.toFloat())
                            val xVel = velocityTracker.getXVelocity(activePointerId).toInt()
                            captureItem!!.fling(xVel)
                        }
                    }
                    cancel()
                }

                MotionEvent.ACTION_CANCEL -> {
                    if (captureItem != null) {
                        captureItem!!.revise()
                    }

                    cancel()
                }
                else -> {
                }
            }
        }

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}

        internal fun cancel() {
            parentHandled = false
            activePointerId = -1
            if (velocityTracker != null) {
                velocityTracker!!.recycle()
                velocityTracker = null
            }
        }
    }

    companion object {

        private val sInterpolator = Interpolator { t ->
            var t = t
            t -= 1.0f
            t * t * t * t * t + 1.0f
        }

        internal fun findTopChildUnder(parent: ViewGroup, x: Int, y: Int): View? {
            val childCount = parent.childCount
            for (i in childCount - 1 downTo 0) {
                val child = parent.getChildAt(i)
                if (x >= child.left && x < child.right
                        && y >= child.top && y < child.bottom) {
                    return child
                }
            }
            return null
        }
    }
}