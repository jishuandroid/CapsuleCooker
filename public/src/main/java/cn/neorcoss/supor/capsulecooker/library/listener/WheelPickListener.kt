package cn.neorcoss.supor.capsulecooker.library.listener

/**
 * Created by shenhua on 2017-12-29-0029.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
interface WheelPickListener {
    /**
     * 时间选择器选择监听
     * @param value 时间
     */
    fun onSelect(value: String)
}