package cn.neorcoss.supor.capsulecooker.library.bean

import java.util.*

/**
 * 饭煲状态
 * Created by shenhua on 2017/12/18.
 * Email shenhuanet@126.com
 */
/**
 * 参数
 * water:水量
 * waterDesc:水量描述
 * rice:米量数组
 * riceDesc:米量描述
 * content:内容
 */
data class CookerStatus(var water: Int, var waterDesc: String, var rice: IntArray, var riceDesc: String, var content: String) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CookerStatus

        if (water != other.water) return false
        if (waterDesc != other.waterDesc) return false
        if (!Arrays.equals(rice, other.rice)) return false
        if (riceDesc != other.riceDesc) return false
        if (content != other.content) return false

        return true
    }

    override fun hashCode(): Int {
        var result = water
        result = 31 * result + waterDesc.hashCode()
        result = 31 * result + Arrays.hashCode(rice)
        result = 31 * result + riceDesc.hashCode()
        result = 31 * result + content.hashCode()
        return result
    }
}