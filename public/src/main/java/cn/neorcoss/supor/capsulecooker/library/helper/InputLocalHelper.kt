package cn.neorcoss.supor.capsulecooker.library.helper

import android.content.Context
import cn.neorcoss.supor.capsulecooker.library.bean.Reserve
import java.util.regex.Pattern

/**
 * 处理用户输入的数据保存
 * Created by shenhua on 2017-12-15-0015.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
object InputLocalHelper {
    private val SP_NAME = "config"

    /**
     * 存储服务端ip
     * @param context context
     * @param ip ip
     */
    fun saveIp(context: Context, ip: String) {
        val sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        sp.edit().putString("ip", ip).apply()
    }

    /**
     * 读取服务端ip
     * @param context context
     */
    fun readIp(context: Context): String {
        val sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        return sp.getString("ip", "")
    }

    /**
     * 清除服务端ip
     * @param context context
     */
    fun cleanIp(context: Context): String {
        val sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        return sp.getString("ip", "")
    }

    /**
     * 保存预约数据
     */
    fun saveReserve(context: Context, reserve: Reserve) {
        val sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        sp.edit()
                .putString("reserve_type", reserve.type)
                .putString("reserve_time", reserve.time).apply()
    }

    /**
     * 读取预约数据
     */
    fun readReserve(context: Context): Reserve {
        val sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        return Reserve(sp.getString("reserve_type", ""), sp.getString("reserve_time", ""), false)
    }

    /**
     * 清除预约数据
     */
    fun cleanReserve(context: Context) {
        val sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        sp.edit()
                .putString("reserve_type", "")
                .putString("reserve_time", "").apply()
    }

    /**
     * 保存工作状态
     */
    fun saveWork(context: Context, type: String) {
        val sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        sp.edit().putString("work_type", type).apply()
    }

    /**
     * 读取工作状态
     */
    fun readWork(context: Context): String {
        val sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        return sp.getString("work_type", "")
    }

    /**
     * 清除工作状态
     */
    fun cleanWork(context: Context) {
        val sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        sp.edit().putString("work_type", "").apply()
    }

    fun isIp(addr: String): Boolean {
        if (addr.length < 7 || addr.length > 15 || "" == addr) {
            return false
        }
        val rexp = "([1-9]|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])(\\.(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])){3}"
        val pat = Pattern.compile(rexp)
        val mat = pat.matcher(addr)
        return mat.find()
    }
}