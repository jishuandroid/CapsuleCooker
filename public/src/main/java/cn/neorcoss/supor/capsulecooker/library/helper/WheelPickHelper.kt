package cn.neorcoss.supor.capsulecooker.library.helper

import android.support.design.widget.BottomSheetDialogFragment
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.NumberPicker
import android.widget.TextView
import cn.neorcoss.supor.capsulecooker.library.listener.WheelPickListener

/**
 * Created by shenhua on 2017-12-29-0029.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class WheelPickHelper {

    companion object {
        /**
         * 设置分割线高度
         */
        fun setDividerHeight(vararg picker: NumberPicker, height: Int) {
            val field = NumberPicker::class.java.getDeclaredField("mSelectionDividerHeight")
            field.isAccessible = true
            picker.forEach {
                field.setInt(it, height)
            }
        }
    }

    private val data = arrayOf("今天", "明天", "后天")
    private var mData = data[1]
    private var mHour = "12"
    private var mMinu = "00"
    private lateinit var pickerData: NumberPicker
    private lateinit var pickerHour: NumberPicker
    private lateinit var pickerMinute: NumberPicker

    fun setPicker(pickerData: NumberPicker, pickerHour: NumberPicker, pickerMinute: NumberPicker) {
        this.pickerData = pickerData
        this.pickerHour = pickerHour
        this.pickerMinute = pickerMinute
        setViews()
        setDividerHeight(pickerData, pickerHour, pickerMinute, height = 1)
    }

    private fun setViews() {
        pickerData.displayedValues = data
        pickerData.minValue = 0
        pickerData.maxValue = data.size - 1
        pickerHour.minValue = 0
        pickerHour.maxValue = 23
        pickerMinute.minValue = 0
        pickerMinute.maxValue = 59
        pickerData.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        pickerHour.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        pickerMinute.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS
        pickerHour.setFormatter {
            formatTime(it)
        }
        pickerMinute.setFormatter {
            formatTime(it)
        }

        pickerData.setOnValueChangedListener { _, _, newVal -> mData = data[newVal] }
        pickerHour.setOnValueChangedListener { _, _, newVal -> mHour = formatTime(newVal) }
        pickerMinute.setOnValueChangedListener { _, _, newVal -> mMinu = formatTime(newVal) }

        pickerData.value = 1
        pickerHour.value = 12
        pickerMinute.value = 0
    }

    fun forDialog(dialog: AlertDialog, cancel: TextView, done: TextView, listener: WheelPickListener?) {
        done.visibility = View.VISIBLE
        done.text = "完成"
        cancel.visibility = View.VISIBLE
        done.setOnClickListener {
            dialog.dismiss()
            listener?.onSelect(toString())
        }
        cancel.setOnClickListener { dialog.dismiss() }
    }

    fun forDialogFragment(dialog: BottomSheetDialogFragment, cancel: TextView, done: TextView, listener: WheelPickListener?) {
        done.visibility = View.VISIBLE
        done.text = "完成"
        cancel.visibility = View.VISIBLE
        done.setOnClickListener {
            dialog.dismiss()
            listener?.onSelect(toString())
        }
        cancel.setOnClickListener { dialog.dismiss() }
    }

    private fun formatTime(it: Int) = if (it < 10) "0$it" else "$it"

    fun jumpTo(time: String) {
        try {
            val s1 = time.split(",")
            pickerData.value = getDateIndex(s1[0])
            val s2 = s1[1].split(":")
            pickerHour.value = s2[0].toInt()
            pickerMinute.value = s2[1].toInt()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getDateIndex(string: String): Int = data.indexOf(string)

    override fun toString(): String = "$mData,$mHour:$mMinu"

}