package cn.neorcoss.supor.capsulecooker.library

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.view_cu_dialog.*

/**
 *
val dialog = MessageDialogFragment.create("消息消息", "确定", "")
dialog.show(childFragmentManager, "message")
dialog.listener = object : MessageDialogFragment.OnClickListener {
override fun onPositiveListener() {
Toast.makeText(context, "确定", Toast.LENGTH_SHORT).show()
}

override fun onNegativeListener() {
Toast.makeText(context, "取消", Toast.LENGTH_SHORT).show()
}
}
 * Created by shenhua on 2018-01-16-0016.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class MessageDialogFragment : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.DialogStyle)
    }

    companion object {
        /**
         * 获取实例
         * @param msg 提示消息
         * @param positive 确定按钮的文本,为空则不显示
         * @param negative 取消按钮的文本,为空则不显示
         */
        fun create(msg: String, positive: String, negative: String): MessageDialogFragment {
            val fragment = MessageDialogFragment()
            val bundle = Bundle()
            bundle.putString("msg", msg)
            bundle.putString("positive", positive)
            bundle.putString("negative", negative)
            fragment.arguments = bundle
            return fragment
        }
    }

    var listener: OnClickListener? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.view_cu_dialog, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog_title.text = tag
        dialog_title.visibility = if (TextUtils.isEmpty(tag)) View.GONE else View.VISIBLE
        dialog_msg.text = arguments.getString("msg")
        // 确定
        if (TextUtils.isEmpty(arguments.getString("positive"))) {
            dialog_positive.visibility = View.GONE
        } else {
            dialog_positive.text = arguments.getString("positive")
            dialog_positive.setOnClickListener {
                dismiss()
                listener?.onPositiveListener()
            }
        }
        // 取消
        if (TextUtils.isEmpty(arguments.getString("negative"))) {
            dialog_negative.visibility = View.GONE
        } else {
            dialog_negative.text = arguments.getString("negative")
            dialog_negative.setOnClickListener {
                dismiss()
                listener?.onNegativeListener()
            }
        }
    }

    interface OnClickListener {

        fun onPositiveListener(){}

        fun onNegativeListener(){}
    }
}