package cn.neorcoss.supor.capsulecooker.library.bean

/**
 * 预约时间
 * Created by shenhua on 2017/12/18.
 * Email shenhuanet@126.com
 */
data class Reserve(var type: String, var time: String, var selected: Boolean) {

    override fun hashCode(): Int {
        var result = type.hashCode()
        result = 31 * result + time.hashCode()
        result = 31 * result + selected.hashCode()
        return result
    }

    override fun equals(other: Any?): Boolean {
        val o = other as Reserve
        return o.type == this.type && o.time == this.time
    }
}