package cn.neorcoss.supor.capsulecooker.library

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View

/**
 * Created by shenhua on 2018/1/18.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
open class BaseFragment : Fragment() {

    var isDestroy = false
//    var workType: WorkType? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        workType = arguments.getSerializable("work") as WorkType
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isDestroy = false
//        setupViews()
//        setListener()
//        sendMsg()
    }

    override fun onDestroy() {
        super.onDestroy()
        isDestroy = true
    }

    override fun toString(): String = this::class.java.name
}