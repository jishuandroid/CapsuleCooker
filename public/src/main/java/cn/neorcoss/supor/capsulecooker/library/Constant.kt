package cn.neorcoss.supor.capsulecooker.library

/**
 * 常量类
 * Created by shenhua on 2017/12/19.
 * Email shenhuanet@126.com
 */
object Constant {

    /**
     * 缺省
     */
    val DEFAULT: Int = 0
//    val DEFAULT_TEXT: String = "default"
    /**
     * 饭
     */
    val RICE: Int = 1
//    val RICE_TEXT: String = "rice"
    val RICE_STRING: String = "煮饭"
    val RICE_TEXT_CANCEL: String = "rice_cancel"
    /**
     * 粥
     */
    val SLOP: Int = 2
//    val SLOP_TEXT: String = "slop"
    val SLOP_STRING: String = "煮粥"
    val SLOP_TEXT_CANCEL: String = "slop_cancel"
    /**
     * 预约饭
     */
    val RESERVE_RICE: Int = 3
//    val RESERVE_RICE_TEXT: String = "reserve_rice"
    val RESERVE_RICE_STRING: String = RICE_STRING
    val RESERVE_RICE_TEXT_CANCEL: String = "reserve_rice_cancel"
    /**
     * 预约粥
     */
    val RESERVE_SLOP: Int = 4
//    val RESERVE_SLOP_TEXT: String = "reserve_slop"
    val RESERVE_SLOP_STRING: String = SLOP_STRING
    val RESERVE_SLOP_TEXT_CANCEL: String = "reserve_slop_cancel"
    /**
     * 保温
     */
    val INSULATION: Int = 5
//    val INSULATION_TEXT: String = "insulation"
    val INSULATION_STRING: String = "保温"
    val INSULATION_TEXT_CANCEL: String = "insulation_cancel"

    /**
     * 胶囊模式
     */
    val MODE_CAPSULE: Int = 0
    /**
     * 非胶囊模式
     */
    val MODE_NONCAPSULE: Int = 1
}