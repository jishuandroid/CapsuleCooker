package cn.neorcoss.supor.capsulecooker.library.bean

import java.io.Serializable

/**
 * 米饭
 * Created by shenhua on 2018/1/19.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
data class Rice(var name: String, var desc: String, var size: Int) : Serializable