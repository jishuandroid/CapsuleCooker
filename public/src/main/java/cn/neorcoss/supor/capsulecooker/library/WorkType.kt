package cn.neorcoss.supor.capsulecooker.library

import com.google.gson.Gson
import java.io.Serializable

/**
 * Created by shenhua on 2018/1/17.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class WorkType : Serializable {

    /**
     * 工作类型：默认、煮饭、煮粥、预约煮饭、预约煮粥、保温
     */
    var type: Int = Constant.DEFAULT
    /**
     * 工作模式：胶囊模式、散米模式
     */
    var mode: Int = Constant.MODE_CAPSULE
    /**
     * 米种：东北大米、紫米等
     */
    var rice: String? = null
    /**
     * 时间：预约时用，格式：今天,12:00
     */
    var time: String? = null
    /**
     * 米量：2个胶囊
     */
    var size: String? = null
    /**
     * 是否被动发起，用于双向联动，被动发起时，被控端不再发送工作状态信息，默认：true
     */
    var passive: Boolean = false

    constructor(type: Int, rice: String, size: String) {
        this.type = type
        this.rice = rice
        this.size = size
    }

    constructor(type: Int, mode: Int, rice: String, size: String) {
        this.type = type
        this.mode = mode
        this.rice = rice
        this.size = size
    }

    constructor(type: Int, rice: String, size: String, time: String) {
        this.type = type
        this.rice = rice
        this.size = size
        this.time = time
    }

    constructor(type: Int, mode: Int, rice: String, size: String, time: String) {
        this.type = type
        this.mode = mode
        this.rice = rice
        this.size = size
        this.time = time
    }

    constructor(type: Int, mode: Int, rice: String, size: String, time: String, passive: Boolean) {
        this.type = type
        this.mode = mode
        this.rice = rice
        this.size = size
        this.time = time
        this.passive = passive
    }

    override fun toString(): String {
        return Gson().toJson(this)
    }

    companion object {

        private const val serialVersionUID = 8439078341846787316L

        /**
         * 根据类型获得文本
         */
        fun getTypeString(type: Int) = when (type) {
            Constant.RICE -> Constant.RICE_STRING
            Constant.SLOP -> Constant.SLOP_STRING
            Constant.RESERVE_RICE -> Constant.RESERVE_RICE_STRING
            Constant.RESERVE_SLOP -> Constant.RESERVE_SLOP_STRING
            Constant.INSULATION -> Constant.INSULATION_STRING
            else -> ""
        }

        /**
         * @param time 今天,12:00
         */
        fun getTimeSplit(time: String): List<String> {
            return try {
                time.split(",")
            } catch (e: Exception) {
                e.printStackTrace()
                List<String>(2, { "error";time })
            }
        }
    }
}