package cn.neorcoss.supor.capsulecooker.library

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup


/**
 * 流式布局
 * Created by shenhua on 2018-01-25-0025.
 * @author shenhua
 *         Email shenhuanet@126.com
 */
class FlowLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : ViewGroup(context, attrs) {

    private val mLines = ArrayList<Line>()
    private var mVerticalSpace: Float = 10f
    private var mHorizontalSpace: Float = 10f
    private var mCurrentLine: Line? = null
    private var mMaxWidth: Int = 0

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        var l = l
        var t = t
        l = paddingLeft
        t = paddingTop
        for (i in mLines.indices) {
            val line = mLines[i]
            line.layout(t, l)
            t += line.height
            if (i != mLines.size - 1) {
                t += mVerticalSpace.toInt()
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        mLines.clear()
        mCurrentLine = null
        val width = MeasureSpec.getSize(widthMeasureSpec)
        mMaxWidth = width - paddingLeft - paddingRight
        val childCount = this.childCount
        for (i in 0 until childCount) {
            val childView = getChildAt(i)
            measureChild(childView, widthMeasureSpec, heightMeasureSpec)
            if (mCurrentLine == null) {
                mCurrentLine = Line(mMaxWidth, mVerticalSpace)
                mCurrentLine!!.addView(childView)
                mLines.add(mCurrentLine!!)
            } else {
                if (mCurrentLine!!.canAddView(childView)) {
                    mCurrentLine!!.addView(childView)
                } else {
                    mCurrentLine = Line(mMaxWidth, mVerticalSpace)
                    mCurrentLine!!.addView(childView)
                    mLines.add(mCurrentLine!!)
                }
            }
        }
        var height = paddingTop + paddingBottom + mLines.indices.sumBy { mLines[it].height }
        height += ((mLines.size - 1) * mVerticalSpace).toInt()
        setMeasuredDimension(width, height)
    }

    inner class Line(private var maxWidth: Int, private var space: Float) {
        private val views: ArrayList<View> = ArrayList()
        private var usedWidth: Int = 0
        var height: Int = 0

        fun addView(view: View) {
            val cw = view.measuredWidth
            val ch = view.measuredHeight
            if (views.size == 0) {
                if (cw > maxWidth) {
                    usedWidth = maxWidth
                    height = ch
                } else {
                    usedWidth = cw
                    height = ch
                }
            } else {
                usedWidth += cw + Math.ceil(space.toDouble()).toInt()
                height = if (ch > height) ch else height
            }
            views.add(view)
        }

        fun canAddView(view: View): Boolean {
            if (views.size == 0) return true
            if (view.measuredWidth > (maxWidth - usedWidth - space)) return false
            return true
        }

        fun layout(t: Int, l: Int) {
            var l = l
            val avg = (maxWidth - usedWidth) / views.size
            for (view in views) {
                var measuredWidth = view.measuredWidth
                val measuredHeight = view.measuredHeight
                view.measure(View.MeasureSpec.makeMeasureSpec(measuredWidth + avg, View.MeasureSpec.EXACTLY),
                        View.MeasureSpec.makeMeasureSpec(measuredHeight, View.MeasureSpec.EXACTLY))
                measuredWidth = view.measuredWidth
                val left = l
                val right = measuredWidth + left
                val bottom = measuredHeight + t
                view.layout(left, t, right, bottom)
                l += (measuredWidth + space).toInt()
            }
        }

    }
}